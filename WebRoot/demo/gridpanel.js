Ext.onReady(function() {
	var store = new Ext.data.JsonStore({
	    storeId: 'revision',
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/demo/gridpanel.js.json',
	        reader: {
	            type: 'json',
	            root: 'images',
	            idProperty: 'name'
	        }
	    },
	    fields: ['revision', 'author','import_date','message','date','import_stat','import_err'],
	    autoLoad: true
	});
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        {header : "修订号",width: 80,autoHeight : true,align : 'left',dataIndex : 'revision',sortable:true},
    		{header : "提交作者",width: 150,autoHeight : true,align : 'left',dataIndex : 'author',sortable:true},
    		{header : "提交时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'date',sortable:true},
    		{header : "备注",width: 250,autoHeight : true,align : 'left',dataIndex : 'message',sortable:true},
    		{header : "导入时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'import_date',sortable:true},
    		{header : "导入结果",width: 150,autoHeight : true,align : 'left',dataIndex : 'import_stat',sortable:true},
    		{header : "导入错误",width: 250,autoHeight : true,align : 'left',dataIndex : 'import_err',sortable:true}
	    ],
	    tbar: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
					//console.info(grid.getSelectionModel().getSelection());
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: Ext.get('center-panel').getHeight() - 40,
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
});