<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>ADTS</title>
	<link rel="stylesheet" type="text/css" href="/adts/css/base.css" />
    <link rel="stylesheet" type="text/css" href="/adts/css/table.css" />
    <link rel="stylesheet" type="text/css" href="/adts/css/superfish.css" media="screen"/>
    
    
    <script src="/adts/js/jquery.js"></script>
    <script src="/adts/js/hoverIntent.js"></script>
    <script src="/adts/js/superfish.min.js"></script>
    
    <script>
		(function($){ //create closure so we can safely use $ as alias for jQuery
			$(document).ready(function(){
				// initialise plugin
				var example = $('#example').superfish({
					//add options here if required
				});
				// buttons to demonstrate Superfish's public methods
				$('.destroy').on('click', function(){
					example.superfish('destroy');
				});

				$('.init').on('click', function(){
					example.superfish();
				});

				$('.open').on('click', function(){
					example.children('li:first').superfish('show');
				});

				$('.close').on('click', function(){
					example.children('li:first').superfish('hide');
				});
			});
		})(jQuery);
	</script>
</head>
<body>
	<!-- Header -->
    <div id="header">
        <div id="branding">
            <h1 id="site-name">ECIF ADTS</h1>
        </div>

        <div id="user-tools">
            Welcome,
            <strong>guest</strong>.
                <!-- <a href="#">Change password</a> /
                <a href="#">Log out</a> -->
        </div>
    </div>
    <!-- END Header -->
    <%@ include file="index_menu.jsp"%>
    <hr/><div style="margin-bottom: 5px;margin-left: 8px;margin-top: 2px;"> Home > 版本管理 > 自动打包</div>
    <hr/>
	<div style="margin-left: 12px;margin-top: 12px;">
