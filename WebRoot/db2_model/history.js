Ext.onReady(function() {
	var tag = 'DB2_ECIF';
	var store = new Ext.data.JsonStore({
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/model/getSvnHistory.do',
	        reader: {
	            type: 'json',
	            root: 'images',
	            idProperty: 'name'
	        },
	        extraParams:{tag:tag}
	    },
	    fields: ['revision','author','import_date','message','date','import_stat','import_err'],
	    autoLoad: true,
	    sortRoot: 'revision',
	    sortOnLoad: true
	});
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        {header : "修订号",width: 80,autoHeight : true,align : 'left',dataIndex : 'revision',sortable:true},
    		{header : "提交作者",width: 150,autoHeight : true,align : 'left',dataIndex : 'author',sortable:true},
    		{header : "提交时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'date',sortable:true},
    		{header : "备注",width: 250,autoHeight : true,align : 'left',dataIndex : 'message',sortable:true},
    		{header : "导入时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'import_date',sortable:true},
    		{header : "导入结果",width: 150,autoHeight : true,align : 'left',dataIndex : 'import_stat',sortable:true},
    		{header : "导入错误",width: 250,autoHeight : true,align : 'left',dataIndex : 'import_err',sortable:true}
	    ],
	    tbar: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			},{
				xtype:'button',
				text:'导入',
				iconCls:'zj',
				handler:function(){
					importSvnModel();
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: getNewTabHeight(),
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
	function importSvnModel(){
		var sm = grid.getSelectionModel();
		if(!sm.hasSelection()){
			Ext.example.msg('','请选择要导入的修订号');
			return;
		}
		var record = sm.getSelection()[0];
		var revision = record.get('revision');
		Ext.getBody().mask("正在解析模型Excel,请稍等!");
		Ext.Ajax.request({
    		url: APP_ROOT+'/model/importSvnModel.do',
    		timeout:999999999,
    		success: function(response, opts){
    			var obj = Ext.decode(response.responseText);
    			var result = obj.result;
    			if(result == 0){
    				Ext.Msg.alert('失败', obj.msg);
    				Ext.getBody().unmask();
    			}else if(result == 2){
    				Ext.Msg.alert('警告', obj.msg);
    				Ext.getBody().unmask();
    			}else{
    				Ext.getBody().unmask();
    				Ext.Msg.alert('提示', "导入成功");
    			}
    			store.load();
    		},
    		failure: function(){
    			Ext.Msg.alert('提示', '解析失败');
    			Ext.getBody().unmask();
    		},
    		params : {revision:revision,tag:tag}
		});
	}
});