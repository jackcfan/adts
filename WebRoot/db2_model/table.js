Ext.onReady(function() {
	var pageSize = 150;
	var tag = 'DB2_ECIF';
	var table_store = new Ext.data.JsonStore({
		pageSize:pageSize,
	    proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/model/getTableList.do',
	        reader: {
	            type: 'json',
	            idProperty: 'name',
	            totalProperty : "count",
				root : "rows"
	        },
	        extraParams:{tag:tag}
	    },
	    fields: ['schema_name','idx_space','tab_name','tab_space','tab_name_cn','remark'],
	    sortOnLoad: true
	});
	
	var revision_store = new Ext.data.JsonStore({
	    proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/model/getRevisionList.do',
	        reader: {
	            type: 'json'
	        },
	        extraParams:{tag:tag}
	    },
	    fields: ['revision']
	});
	
	var env_store = new Ext.data.JsonStore({
	    proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/model/getEnvListByRevision.do',
	        reader: {
	            type: 'json'
	        },
	        extraParams:{tag:tag}
	    },
	    fields: ['env_name']
	});
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: table_store,
	    selModel:selModel,
	    columns : [
	        new Ext.grid.RowNumberer({width:30}),
	        {header : "模式名",width: 180,autoHeight : true,align : 'left',dataIndex : 'schema_name',sortable:true},
    		{header : "表名",width: 300,autoHeight : true,align : 'left',dataIndex : 'tab_name',sortable:true},
    		{header : "表中文名",width: 300,autoHeight : true,align : 'left',dataIndex : 'tab_name_cn',sortable:true},
    		{header : "表空间",width: 200,autoHeight : true,align : 'left',dataIndex : 'tab_space',sortable:true},
    		{header : "索引空间",width: 200,autoHeight : true,align : 'left',dataIndex : 'idx_space',sortable:true},
    		{header : "备注",width: 150,autoHeight : true,align : 'left',dataIndex : 'remark',sortable:true}
	    ],
	    tbar: ['修订号',
            {
				xtype:'combo',
				id:'adts_model_table_revision_db2',
				store:revision_store,
				width:80,
				valueField : 'revision',
				displayField : 'revision',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local'
			},'&nbsp;环境名',
            {
				xtype:'combo',
				id:'adts_model_table_env_name_db2',
				store:env_store,
				width:80,
				valueField : 'env_name',
				displayField : 'env_name',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local',
				value:''
			},'&nbsp;模式名',
            {
				xtype:'textfield',
				id:'adts_model_table_schema_name_db2',
				width:150
			},' &nbsp;&nbsp;表名&nbsp;',{
 				xtype:'textfield',
 				id:'adts_model_table_tab_name_db2',
 				name:'adts_model_table_tab_name_db2',
 				width:150
 			},'-',{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					table_store.load();
				}
			},{
				xtype:'button',
				text:'字段',
				iconCls:'Tablecolumn',
				handler:function(){
					checkCol();
				}
			}
		],
		bbar:new Ext.PagingToolbar({
			pageSize : pageSize,
			store : table_store,
			emptyMsg  : '没有查到数据',
			displayInfo : true,
			beforePageText : '第',
			afterPageText : '页 共 {0} 页',
			displayMsg : '显示{0} - {1}，共 {2} 条',
			refreshText : '刷新'
		}),
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: getNewTabHeight(),
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
	table_store.on('beforeload', function(store, options) {
		table_store.removeAll();
		Ext.apply(store.proxy.extraParams, {
			schema_name:getCmpValue("adts_model_table_schema_name_db2").toUpperCase(),
			tab_name:getCmpValue("adts_model_table_tab_name_db2").toUpperCase(),
			revision:getCmpValue("adts_model_table_revision_db2"),
			env_name:getCmpValue("adts_model_table_env_name_db2"),
			limit:pageSize
		});
	});
	
	env_store.on('beforeload', function(store, options) {
		env_store.removeAll();
		Ext.apply(store.proxy.extraParams, {revision:getCmpValue("adts_model_table_revision_db2")});
	});
	
	Ext.getCmp('adts_model_table_revision_db2').on('select',function(){
		env_store.load();
	});
	
	revision_store.load({callback:function(){
		if(revision_store.getCount() > 0){
			Ext.getCmp('adts_model_table_revision_db2').setValue(revision_store.getAt(0).get('revision'));
			env_store.load({callback:function(){
				if(env_store.getCount() > 0){
					Ext.getCmp('adts_model_table_env_name_db2').setValue(env_store.getAt(0).get('env_name'));
					table_store.load();
				}
			}});
		}
	}});
	
	var tab_col_id = new Date().getTime();
	
	function checkCol(){
		var sm = grid.getSelectionModel();
		if(!sm.hasSelection()){
			Ext.example.msg('','请先选中要查看的表');
			return;
		}
		var record = sm.getSelection()[0];
		var schema_name = record.get('schema_name');
		var tab_name    = record.get('tab_name');
		removeTabPanel({id:tab_col_id});
		addTabPanel({title:'字段信息',url:APP_ROOT+'/db2_model/column.jsp?tab_name_full='+schema_name+'.'+tab_name+"&tag="+tag,id:tab_col_id});
	}
	
});