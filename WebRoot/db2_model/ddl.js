Ext.onReady(function() {
	var pageSize = 150;
	var tag = 'DB2_ECIF';
	var revision_store = new Ext.data.JsonStore({
	    proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/model/getRevisionList.do',
	        reader: {
	            type: 'json'
	        },
	        extraParams:{tag:tag}
	    },
	    fields: ['revision']
	});
	
	var env_store = new Ext.data.JsonStore({
	    proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/model/getEnvListByRevision.do',
	        reader: {
	            type: 'json'
	        },
	        extraParams:{tag:tag}
	    },
	    fields: ['env_name']
	});
	
	var h1 = 150;
	var h2 = getNewTabHeight() - h1 - 60;
	var id1 = Ext.id();
	var id2 = Ext.id();
	var grid = Ext.create('Ext.Panel', {
	    html: "<div style='top:5px'>全表名：(多个表名以逗号或回车符分隔)</div><textarea style='width: 99.5%; height: "+h1+"px;' id='"+id1+"'></textarea>" +
				"<textarea style='width: 99.5%; height: "+h2+"px;' id='"+id2+"' readonly=true></textarea>",
	    tbar: ['修订号',
            {
				xtype:'combo',
				id:'adts_model_ddl_revision_db2',
				store:revision_store,
				width:80,
				valueField : 'revision',
				displayField : 'revision',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local'
			},'&nbsp;环境名',
            {
				xtype:'combo',
				id:'adts_model_ddl_env_name_db2',
				store:env_store,
				width:80,
				valueField : 'env_name',
				displayField : 'env_name',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local',
				value:''
			},'-',{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					ddllook();
				}
			},{
				xtype:'button',
				text:'全选',
				iconCls:'Shapesquareselect',
				handler:function(){
					document.getElementById(id2).select();
				}
			}
		],
	    renderTo: random_id,
	    height: getNewTabHeight(),
	    autoWidth : true,
	    layout:'fit',
		autoWidth : true,
		autoScroll:true,
        stateful: true,
        border : false,
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});

	env_store.on('beforeload', function(store, options) {
		env_store.removeAll();
		Ext.apply(store.proxy.extraParams, {revision:getCmpValue("adts_model_ddl_revision_db2")});
	});
	
	Ext.getCmp('adts_model_ddl_revision_db2').on('select',function(){
		env_store.load();
	});
	
	revision_store.load({callback:function(){
		if(revision_store.getCount() > 0){
			Ext.getCmp('adts_model_ddl_revision_db2').setValue(revision_store.getAt(0).get('revision'));
			env_store.load({callback:function(){
				if(env_store.getCount() > 0){
					Ext.getCmp('adts_model_ddl_env_name_db2').setValue(env_store.getAt(0).get('env_name'));
				}
			}});
		}
	}});
	
	function ddllook(){
		var table_list = Ext.get(id1).getValue();
		if(table_list == ""){
			Ext.example.msg('提示', '未输入全表名，请先输入全表名再做查询');
			return;
		}
		grid.mask('正在查询...');
		Ext.Ajax.request({
    		url: APP_ROOT+'/model/getDDLList.do',
    		success: function(response, opts){
    			var obj = Ext.decode(response.responseText);
    			document.getElementById(id2).value = obj.ddl;
    			grid.unmask();
    		},
    		failure: function(){
    			Ext.Msg.alert('提示', '查询失败');
    			grid.unmask();
    		},
    		params : {
    			table_list:table_list,
    			revision:getCmpValue('adts_model_ddl_revision_db2'),
    			env_name:getCmpValue('adts_model_ddl_env_name_db2'),
    			tag:tag
    	    }
		});
	}
});