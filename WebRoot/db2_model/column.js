Ext.onReady(function() {
	var tag = 'DB2_ECIF';
	var store = new Ext.data.JsonStore({
	    proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/model/getColumnList.do',
	        reader: {
	            type: 'json',
	            idProperty: 'name'
	        },
	        extraParams:{tag:tag}
	    },
	    fields: ['col_name','col_name_cn','col_type','col_len','col_scale','col_no','nullable'
	              ,'is_pk','is_dk','is_ptk','remark','idx_1','idx_2','idx_3','idx_4','idx_5','idx_6','case_no_sens','col_type_str','mask']
	});
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        new Ext.grid.RowNumberer({width:30}),
	        {header : "字段名",width: 200,autoHeight : true,align : 'left',dataIndex : 'col_name',sortable:true},
    		{header : "字段中文名",width: 240,autoHeight : true,align : 'left',dataIndex : 'col_name_cn',sortable:true},
    		{header : "字段类型",width: 140,autoHeight : true,align : 'left',dataIndex : 'col_type_str',sortable:true},
    		{header : "序号",width: 60,autoHeight : true,align : 'left',dataIndex : 'col_no',sortable:true},
    		{header : "允许为空",width: 70,autoHeight : true,align : 'left',dataIndex : 'nullable',sortable:true},
    		{header : "主键",width: 50,autoHeight : true,align : 'left',dataIndex : 'is_pk',sortable:true},
    		{header : "分布键",width: 60,autoHeight : true,align : 'left',dataIndex : 'is_dk',sortable:true},
    		{header : "分区键",width: 60,autoHeight : true,align : 'left',dataIndex : 'is_ptk',sortable:true},
    		{header : "Mask",width: 50,autoHeight : true,align : 'left',dataIndex : 'mask',sortable:true},
    		{header : "备注",width: 150,autoHeight : true,align : 'left',dataIndex : 'remark',sortable:true},
    		{header : "索引1",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_1',sortable:true},
    		{header : "索引2",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_2',sortable:true},
    		{header : "索引3",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_3',sortable:true},
    		{header : "索引4",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_4',sortable:true},
    		{header : "索引5",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_5',sortable:true},
    		{header : "索引6",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_6',sortable:true},
    		{header : "大小写不敏感",width: 100,autoHeight : true,align : 'left',dataIndex : 'case_no_sens',sortable:true}
	    ],
	    tbar: ['全表名&nbsp;',{
 				xtype:'textfield',
 				id:'adts_model_column_tab_name_full_db2',
 				name:'adts_model_column_tab_name_full_db2',
 				width:450
 			},'-',{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: getNewTabHeight(),
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
	store.on('beforeload', function() {
		store.removeAll();
		var tab_name_full = trim(getCmpValue("adts_model_column_tab_name_full_db2").toUpperCase());
		var schema_name   = tab_name_full.substr(0,tab_name_full.indexOf('.'));
		var tab_name      = tab_name_full.substr(tab_name_full.indexOf('.') + 1);
		Ext.apply(store.proxy.extraParams, {
			schema_name:schema_name,
			tab_name:tab_name
		});
	});
	
	if(adts_model_column_tab_name_full_db2 != ""){
		Ext.getCmp("adts_model_column_tab_name_full_db2").setValue(adts_model_column_tab_name_full_db2);
		store.load();
	}
});