<%@page import="adts.util.Util"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<script type="text/javascript">
		<%
			String tab_name_full = request.getParameter("tab_name_full");
		    if(tab_name_full == null){tab_name_full = "";}
		%>
		var adts_model_column_tab_name_full = '<%=tab_name_full%>';
	</script>
	<script type="text/javascript" src="model/column_info.js?<%=Util.getVersion()%>"></script>
  </head>
	<body>
		<div id="adts_model_column"></div>
	</body>
</html>