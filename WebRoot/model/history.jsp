<%@page import="adts.util.Util"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript" src="model/history.js?<%=Util.getVersion()%>"></script>
  </head>
	<body>
		<div id="adts_model_history"></div>
	</body>
</html>