Ext.onReady(function() {
	Ext.QuickTips.init();
	Ext.form.Field.prototype.msgTarget = "side";
	
	var adts_model_revision_store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getRevisionList.do'}),
		reader : new Ext.data.JsonReader({}, [
		    {name : 'revision'}
		])
	});
	
	var adts_model_env_store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getEnvListByRevision.do'}),
		reader : new Ext.data.JsonReader({}, [
		    {name : 'env_name'}
		])
	});
	
	var adts_model_ddl = new Ext.Panel({
		//title: '',
		preventBodyReset: true,
		renderTo: 'adts_model_ddl',
		html: "全表名：(多个表名以逗号或回车符分隔)<br/><textarea style='width: 99.5%; height: 150px;' id='adts_ddl_tables'></textarea>" +
				"<textarea style='width: 99.5%; height: 300px;' id='adts_ddl_pre' readonly=true></textarea>",
	    tbar: new Ext.Toolbar([
			{
				xtype:'button',
				text:'查询',
				iconCls:'look',
				handler:ddllook
			},{
				xtype:'button',
				text:'全选',
				iconCls:'edit',
				handler:function(){
					document.getElementById('adts_ddl_pre').select();
				}
			},{
				xtype:'button',
				text:'下载',
				iconCls:'edit',
				handler:getDDLFile,
				disabled:true
			},'-','&nbsp;修订号',
			{
				xtype:'combo',
				id:'adts_model_ddl_revision',
				store:adts_model_revision_store,
				width:80,
				valueField : 'revision',
				displayField : 'revision',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local'
			},'&nbsp;环境名',
			{
				xtype:'combo',
				id:'adts_model_ddl_env_name',
				store:adts_model_env_store,
				//name:'adts_model_table_schema_name',
				width:80,
				valueField : 'env_name',
				displayField : 'env_name',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local',
				value:''
			}
	    ]),
		stripeRows: true,
        height: global_grid_height,
        layout:'fit',
		autoWidth : true,
		autoScroll:true,
        stateful: true
	});
	
	function ddllook(){
		var table_list = Ext.get('adts_ddl_tables').getValue();
		if(table_list == ""){
			Ext.example.msg('提示', '未输入全表名，请先输入全表名再做查询');
			return;
		}
		Ext.getBody().mask('正在查询...');
		Ext.Ajax.request({
    		url: APP_ROOT+'/model/getDDLList.do',
    		success: function(response, opts){
    			var obj = Ext.decode(response.responseText);
    			adts_ddl_pre.innerHTML = obj.ddl;
    			Ext.getBody().unmask();
    		},
    		failure: function(){
    			Ext.Msg.alert('提示', '查询失败');
    			Ext.getBody().unmask();
    		},
    		params : {
    			table_list:table_list,
    			revision:getCmpValue('adts_model_ddl_revision'),
    			env_name:getCmpValue('adts_model_ddl_env_name')
    	    }
		});
	}
	
	function getDDLFile(){
		var table_list = getCmpValue("adts_model_ddl_table_list");
		if(getCmpValue("adts_model_ddl_table_list") == ""){
			Ext.example.msg('提示', '未输入全表名，请先输入全表名再做查询');
			return;
		}
		Ext.getBody().mask('正在查询...');
		Ext.Ajax.request({
    		url: APP_ROOT+'/model/getDDLListFile.do',
    		success: function(response, opts){
    			Ext.getBody().unmask();
    		},
    		failure: function(){
    			Ext.Msg.alert('提示', '查询失败');
    			Ext.getBody().unmask();
    		},
    		params : {
    			table_list:table_list,
    			revision:getCmpValue('adts_model_ddl_revision'),
    			env_name:getCmpValue('adts_model_ddl_env_name')
    	    }
		});
	}
	
	adts_model_env_store.on('beforeload', function() {
		adts_model_env_store.removeAll();
		Ext.apply(this.baseParams, {
			revision:getCmpValue("adts_model_ddl_revision")
		});
	});
	
	Ext.getCmp('adts_model_ddl_revision').on('select',function(){
		adts_model_env_store.load();
	});
	
	adts_model_revision_store.load({callback:function(){
		if(adts_model_revision_store.getCount() > 0){
			Ext.getCmp('adts_model_ddl_revision').setValue(adts_model_revision_store.getAt(0).get('revision'));
			adts_model_env_store.load({callback:function(){
				if(adts_model_env_store.getCount() > 0){
					Ext.getCmp('adts_model_ddl_env_name').setValue(adts_model_env_store.getAt(0).get('env_name'));
				}
			}});
		}
	}});
	
	//adts_model_ddl.update("<pre id='adts_ddl_pre'></pre>");
});