Ext.onReady(function() {
	Ext.QuickTips.init();
	Ext.form.Field.prototype.msgTarget = "side";
	
	var adts_model_history_store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getSvnHistory.do'}),
		reader : new Ext.data.JsonReader({}, [
		    {name : 'revision'},
		    {name : 'author'},
			{name : 'import_date'},
			{name : 'message'},
			{name : 'date'},
			{name : 'import_stat'},
			{name : 'import_err'}
		]),
		sortInfo:{field:'revision',direction:'DESC'}
	});
	
	var adts_model_history_sm = new Ext.grid.CheckboxSelectionModel({singleSelect: true,dataIndex: 'id'});
	var adts_model_history_grid = new Ext.grid.GridPanel({
		//id : 'adts_model_history_grid',
		renderTo : 'adts_model_history',
		sm: adts_model_history_sm,
		store : adts_model_history_store,
		colModel: new Ext.grid.ColumnModel([
    		new Ext.grid.RowNumberer(),
    		adts_model_history_sm,
    		{header : "修订号",width: 80,autoHeight : true,align : 'left',dataIndex : 'revision',sortable:true},
    		{header : "提交作者",width: 150,autoHeight : true,align : 'left',dataIndex : 'author',sortable:true},
    		{header : "提交时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'date',sortable:true},
    		{header : "备注",width: 250,autoHeight : true,align : 'left',dataIndex : 'message',sortable:true},
    		{header : "导入时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'import_date',sortable:true},
    		{header : "导入结果",width: 150,autoHeight : true,align : 'left',dataIndex : 'import_stat',sortable:true},
    		{header : "导入错误",width: 250,autoHeight : true,align : 'left',dataIndex : 'import_err',sortable:true}
    	]),
		tbar: new Ext.Toolbar([{
				xtype:'button',
				text:'查询',
				iconCls:'look',
				handler:function(){
					adts_model_history_store.load({});
				}
			},'-',{
				xtype:'button',
				text:'导入',
				iconCls:'zj',
				handler:function(){importSvnModel();}
			}
		]),
		stripeRows: true,
        loadMask : ({msg : '数据正在加载中，请稍等……'}),
        height: global_grid_height,
		autoWidth : true,
		autoScroll:true,
        //title: 'Array Grid',
        // config options for stateful behavior
        stateful: true
        //stateId: 'grid'
	});
	
	function importSvnModel(){
		Ext.getBody().mask();
		var record = adts_model_history_sm.getSelected();
		if(typeof record == "undefined"){
			Ext.example.msg('提示', '请选择要导入的修订号');
			Ext.getBody().unmask();
			return;
		}
		var revision = record.get('revision');
		Ext.getBody().mask("正在解析模型Excel,请稍等!");
		Ext.Ajax.request({
    		url: APP_ROOT+'/model/importSvnModel.do',
    		timeout:999999999,
    		success: function(response, opts){
    			var obj = Ext.decode(response.responseText);
    			var result = obj.result;
    			if(result == 0){
    				Ext.Msg.alert('失败', obj.msg);
    				Ext.getBody().unmask();
    			}else if(result == 2){
    				Ext.Msg.alert('警告', obj.msg);
    				Ext.getBody().unmask();
    			}else{
    				Ext.getBody().unmask();
    				Ext.Msg.alert('提示', "导入成功");
    			}
    			adts_model_history_store.load();
    		},
    		failure: function(){
    			Ext.Msg.alert('提示', '解析失败');
    			Ext.getBody().unmask();
    		},
    		params : {revision:revision}
		});
	}
	adts_model_history_store.load();
	//adts_model_history_grid.addListener('rowdblclick',editSys);
});