Ext.onReady(function() {
	Ext.QuickTips.init();
	Ext.form.Field.prototype.msgTarget = "side";
	
	var adts_model_column_store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getColumnList.do'}),
		reader : new Ext.data.JsonReader({}, [
		    {name : 'col_name'},
		    {name : 'col_name_cn'},
			{name : 'col_type'},
			{name : 'col_len'},
			{name : 'col_scale'},
			{name : 'col_no'},
			{name : 'nullable'},
			{name : 'is_pk'},
			{name : 'is_dk'},
			{name : 'is_ptk'},
			{name : 'remark'},
			{name : 'idx_1'},
			{name : 'idx_2'},
			{name : 'idx_3'},
			{name : 'idx_4'},
			{name : 'idx_5'},
			{name : 'idx_6'},
			{name : 'case_no_sens'},
			{name : 'col_type_str'}
		])
	});
	
	var adts_model_column_sm = new Ext.grid.CheckboxSelectionModel({singleSelect: true,dataIndex: 'id'});
	var adts_model_column_grid = new Ext.grid.GridPanel({
		//id : 'adts_model_column_grid',
		renderTo : 'adts_model_column',
		sm: adts_model_column_sm,
		store : adts_model_column_store,
		viewConfig:{enableTextSelection:true},
		colModel: new Ext.grid.ColumnModel([
    		new Ext.grid.RowNumberer({width:30}),
    		adts_model_column_sm,
    		{header : "字段名",width: 160,autoHeight : true,align : 'left',dataIndex : 'col_name',sortable:true},
    		{header : "字段中文名",width: 180,autoHeight : true,align : 'left',dataIndex : 'col_name_cn',sortable:true},
    		{header : "字段类型",width: 120,autoHeight : true,align : 'left',dataIndex : 'col_type_str',sortable:true},
    		{header : "序号",width: 50,autoHeight : true,align : 'left',dataIndex : 'col_no',sortable:true},
    		{header : "允许为空",width: 70,autoHeight : true,align : 'left',dataIndex : 'nullable',sortable:true},
    		{header : "主键",width: 50,autoHeight : true,align : 'left',dataIndex : 'is_pk',sortable:true},
    		{header : "分布键",width: 60,autoHeight : true,align : 'left',dataIndex : 'is_dk',sortable:true},
    		{header : "分区键",width: 60,autoHeight : true,align : 'left',dataIndex : 'is_ptk',sortable:true},
    		{header : "备注",width: 150,autoHeight : true,align : 'left',dataIndex : 'remark',sortable:true},
    		{header : "索引1",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_1',sortable:true},
    		{header : "索引2",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_2',sortable:true},
    		{header : "索引3",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_3',sortable:true},
    		{header : "索引4",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_4',sortable:true},
    		{header : "索引5",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_5',sortable:true},
    		{header : "索引6",width: 50,autoHeight : true,align : 'left',dataIndex : 'idx_6',sortable:true},
    		{header : "大小写不敏感",width: 100,autoHeight : true,align : 'left',dataIndex : 'case_no_sens',sortable:true}
    	]),
		tbar: new Ext.Toolbar(['&nbsp;全表名&nbsp;',{
 				xtype:'textfield',
 				id:'adts_model_column_tab_name_full',
 				name:'adts_model_column_tab_name_full',
 				width:350
 			},'-',{
				xtype:'button',
				text:'查询',
				iconCls:'look',
				handler:function(){
					var tab_name_full = trim(getCmpValue("adts_model_column_tab_name_full").toUpperCase());
					if(tab_name_full.indexOf('.') == -1){
						Ext.Msg.alert('提示', '请输入全表名');
						return;
					}
					adts_model_column_store.load({});
				}
			}
		]),
		stripeRows: true,
        loadMask : ({msg : '数据正在加载中，请稍等……'}),
        height: global_grid_height,
		autoWidth : true,
		autoScroll:true,
        //title: 'Array Grid',
        // config options for stateful behavior
        stateful: true
        //stateId: 'grid'
	});
	
	adts_model_column_store.on('beforeload', function() {
		adts_model_column_store.removeAll();
		var tab_name_full = trim(getCmpValue("adts_model_column_tab_name_full").toUpperCase());
		var schema_name   = tab_name_full.substr(0,tab_name_full.indexOf('.'));
		var tab_name      = tab_name_full.substr(tab_name_full.indexOf('.') + 1);
		Ext.apply(this.baseParams, {
			schema_name:schema_name,
			tab_name:tab_name
		});
	});
	
	if(adts_model_column_tab_name_full != ""){
		Ext.getCmp("adts_model_column_tab_name_full").setValue(adts_model_column_tab_name_full);
		adts_model_column_store.load();
	}
	//adts_model_column_store.load();
	//adts_model_column_grid.addListener('rowdblclick',editSys);
});