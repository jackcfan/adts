Ext.onReady(function() {
	Ext.QuickTips.init();
	Ext.form.Field.prototype.msgTarget = "side";
	
	var pageSize = 150;
	
	var adts_model_table_store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getTableList.do'}),
		reader : new Ext.data.JsonReader({
			totalProperty : "count",
			root : "rows"
		}, [
		    {name : 'schema_name'},
		    {name : 'idx_space'},
			{name : 'tab_name'},
			{name : 'tab_space'},
			{name : 'tab_name_cn'},
			{name : 'remark'}
		])
	});
	
	var adts_model_revision_store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getRevisionList.do'}),
		reader : new Ext.data.JsonReader({}, [
		    {name : 'revision'}
		])
	});
	
	var adts_model_env_store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getEnvListByRevision.do'}),
		reader : new Ext.data.JsonReader({}, [
		    {name : 'env_name'}
		])
	});
	
	var adts_model_table_sm = new Ext.grid.CheckboxSelectionModel({singleSelect: true,dataIndex: 'id'});
	var adts_model_table_grid = new Ext.grid.GridPanel({
		//id : 'adts_model_table_grid',
		renderTo : 'adts_model_table',
		sm: adts_model_table_sm,
		store : adts_model_table_store,
		colModel: new Ext.grid.ColumnModel([
    		new Ext.grid.RowNumberer({width:30}),
    		adts_model_table_sm,
    		{header : "模式名",width: 120,autoHeight : true,align : 'left',dataIndex : 'schema_name',sortable:true},
    		{header : "表名",width: 300,autoHeight : true,align : 'left',dataIndex : 'tab_name',sortable:true},
    		{header : "表中文名",width: 300,autoHeight : true,align : 'left',dataIndex : 'tab_name_cn',sortable:true},
    		{header : "表空间",width: 120,autoHeight : true,align : 'left',dataIndex : 'tab_space',sortable:true},
    		{header : "索引空间",width: 130,autoHeight : true,align : 'left',dataIndex : 'idx_space',sortable:true},
    		{header : "备注",width: 150,autoHeight : true,align : 'left',dataIndex : 'remark',sortable:true}
    	]),
		tbar: new Ext.Toolbar(['&nbsp;修订号',
            {
				xtype:'combo',
				id:'adts_model_table_revision',
				store:adts_model_revision_store,
				//name:'adts_model_table_schema_name',
				width:80,
				valueField : 'revision',
				displayField : 'revision',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local'
			},'&nbsp;环境名',
            {
				xtype:'combo',
				id:'adts_model_table_env_name',
				store:adts_model_env_store,
				//name:'adts_model_table_schema_name',
				width:80,
				valueField : 'env_name',
				displayField : 'env_name',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local',
				value:''
			},'&nbsp;模式名',
            {
				xtype:'textfield',
				id:'adts_model_table_schema_name',
				width:150
			},' &nbsp;&nbsp;表名&nbsp;',{
 				xtype:'textfield',
 				id:'adts_model_table_tab_name',
 				name:'adts_model_table_tab_name',
 				width:150
 			},'-',{
				xtype:'button',
				text:'查询',
				iconCls:'look',
				handler:function(){
					adts_model_table_store.load({});
				}
			},'&nbsp;',{
				xtype:'button',
				text:'字段',
				iconCls:'column',
				handler:checkCol
			}
		]),
		bbar:new Ext.PagingToolbar({
			pageSize : pageSize,
			store : adts_model_table_store,
			emptyMsg  : '没有查到数据',
			displayInfo : true,
			beforePageText : '第',
			afterPageText : '页 共 {0} 页',
			displayMsg : '显示{0} - {1}，共 {2} 条',
			refreshText : '刷新'
		}),
		stripeRows: true,
        loadMask : ({msg : '数据正在加载中，请稍等……'}),
        height: global_grid_height,
		autoWidth : true,
		autoScroll:true,
        //title: 'Array Grid',
        // config options for stateful behavior
        stateful: true
        //stateId: 'grid'
	});
	
	function checkCol(){
		var record = adts_model_table_sm.getSelected();
		if(typeof record == "undefined"){
			Ext.Msg.alert('提示', '未选择表，请先选择要查看的表');
			return;
		}
		var schema_name = record.get('schema_name');
		var tab_name    = record.get('tab_name');
		removeTapPanel('字段信息');
		addTapPanel('model/column_info.jsp?tab_name_full='+schema_name+'.'+tab_name,'字段信息',true);
	}

	adts_model_table_store.on('beforeload', function() {
		adts_model_table_store.removeAll();
		Ext.apply(this.baseParams, {
			schema_name:getCmpValue("adts_model_table_schema_name").toUpperCase(),
			tab_name:getCmpValue("adts_model_table_tab_name").toUpperCase(),
			revision:getCmpValue("adts_model_table_revision"),
			env_name:getCmpValue("adts_model_table_env_name"),
			start:0,
			limit:pageSize
		});
	});
	
	adts_model_env_store.on('beforeload', function() {
		adts_model_env_store.removeAll();
		Ext.apply(this.baseParams, {
			revision:getCmpValue("adts_model_table_revision")
		});
	});
	
	Ext.getCmp('adts_model_table_revision').on('select',function(){
		adts_model_env_store.load();
	});
	
	adts_model_revision_store.load({callback:function(){
		if(adts_model_revision_store.getCount() > 0){
			Ext.getCmp('adts_model_table_revision').setValue(adts_model_revision_store.getAt(0).get('revision'));
			adts_model_env_store.load({callback:function(){
				if(adts_model_env_store.getCount() > 0){
					Ext.getCmp('adts_model_table_env_name').setValue(adts_model_env_store.getAt(0).get('env_name'));
					adts_model_table_store.load();
				}
			}});
		}
	}});
	
	//adts_model_table_grid.addListener('rowdblclick',editSys);
});