Ext.onReady(function() {
	var store = new Ext.data.JsonStore({
	    storeId: 'revision',
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/demo/gridpanel.js.json',
	        url: APP_ROOT+'/auth/getAllUser.do',
	        reader: {
	            type: 'json',
	            root: 'images',
	            idProperty: 'name'
	        }
	    },
	    fields: ['username', 'realname','state','tel_no','email','create_time','last_login_time'],
	    autoLoad: true
	});
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        {header : "账户名",width: 150,autoHeight : true,align : 'left',dataIndex : 'username',sortable:true},
    		{header : "实名",width: 90,autoHeight : true,align : 'left',dataIndex : 'realname',sortable:true},
    		{header : "状态",width: 100,autoHeight : true,align : 'left',dataIndex : 'state',sortable:true},
    		{header : "手机号",width: 130,autoHeight : true,align : 'left',dataIndex : 'tel_no',sortable:true},
    		{header : "Email",width: 250,autoHeight : true,align : 'left',dataIndex : 'email',sortable:true,filter: {
                    type: 'string'
                }},
    		{header : "创建时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'create_time',sortable:true},
    		{header : "最后登录时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'last_login_time',sortable:true}
	    ],
	    tbar: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			},{
				xtype:'button',
				text:'添加',
				iconCls:'Useradd',
				handler:function(){
					Ext.getBody().mask();
					auth_user_win.setIconCls('Useradd');
					auth_user_form.getForm().reset();
					auth_user_win.setTitle('新增用户');
					auth_user_win.show().center();
				}
			},{
				xtype:'button',
				text:'编辑',
				iconCls:'Useredit',
				disabled:true,
				handler:function(){
					auth_user_win.setIconCls('Useredit');
					
					auth_user_form.getForm().reset();
					auth_user_win.setTitle('编辑用户');
					auth_user_win.show().center();
				}
			},{
				xtype:'button',
				text:'删除',
				iconCls:'Userdelete',
				handler:function(){
					var sm = grid.getSelectionModel();
					if(!sm.hasSelection()){
						Ext.example.msg('','请先选中要删除的用户');
						return;
					}
					var record = sm.getSelection()[0];
					deleteUser(record.get('id'),record.get('username'));
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: Ext.get('center-panel').getHeight() - 40,
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
	var auth_user_form = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            width: 350,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            defaultType: 'textfield',
            items: [{
                fieldLabel: '账户名',
                name: 'username',
                allowBlank: false
            },{
                fieldLabel: '实 名',
                name: 'realname',
                allowBlank: false
            },{
                fieldLabel: '用户状态',
                name: 'state',
                allowBlank: false,
                xtype:'combo',
                store: new Ext.data.SimpleStore({
					fields:['value','text'],
					data:[['0','正常'],['1','禁用'],['2','密码过期']]
				}),
				valueField : 'value',
				displayField : 'text',
				editable:false
            },{
                fieldLabel: '密码',
                name: 'password',
                allowBlank: false
            },{
                fieldLabel: '手机号',
                name: 'tel_no',
                allowBlank: true
            },{
                fieldLabel: 'id',
                name: 'id',
                allowBlank: true,
                xtype: 'hiddenfield'
            }],
            // Reset and Submit buttons
            buttons: [{
                text: '重置',
                handler: function() {
                    this.up('form').getForm().reset();
                }
            },{
                text: '关闭',
                handler: function() {
                    auth_user_win.hide();
                    Ext.getBody().unmask();
                }
            }, {
                text: '提交',
                formBind: true, //only enabled once the form is valid
                disabled: true,
                handler: function() {
                    var form = this.up('form').getForm();
                    if (form.isValid()) {
                        form.submit({
                        	url: APP_ROOT+'/auth/addUser.do',
						    waitMsg: 'Loading...',
						    method: 'POST',
                            success: function(form, action) {
                               Ext.example.msg('','添加成功');
                               auth_user_win.hide();
                               Ext.getBody().unmask();
                               store.load();
                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('添加失败', action.result.msg);
                            }
                        });
                    }
                }
            }]
        });
	
	var auth_user_win = Ext.create('Ext.window.Window', {
		resizable: false,
		closable : true,
		closeAction: 'hide',
		iconCls:'Useradd',
    	title: '',
    	items:[auth_user_form],
    	listeners:{
	        'beforehide':function(win){
	            Ext.getBody().unmask();
	        }
	    }
    });
    
    function deleteUser(id,username){
    	Ext.MessageBox.confirm("删除", "确认删除用户 "+username+" ?", function (btn) {
	        if(btn == 'yes'){
				Ext.Ajax.request({
	                url: APP_ROOT+'/auth/deleteUser.do',
	                params: { id: id},
	                method: 'post',
	                success: function (response, options) {
	                    var result = Ext.JSON.decode(response.responseText);
	                    if(result.success){
		                    Ext.example.msg('','删除成功');
	                    }else{
	                    	Ext.Msg.alert('删除失败', result.msg);
	                    }
	                    store.load();
	                },
	                failure: function (response, options) {
	                    Ext.Msg.alert('删除失败', action.result.msg);
	                    store.load();
	                }
	            });
	        }
	    });
    }
});