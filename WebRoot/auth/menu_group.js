Ext.onReady(function() {
	Ext.QuickTips.init();
	
	var id_list = "";
	function treeCheckChange(node, checked){
		//当前节点选中则所有子节点自动选中
		//当前节点取消选中则所有子节点自动取消选中
		var setChildNode = function(node,check){
			node.set('checked',check);
			if(node.isNode){
				node.eachChild(function (child) {
					setChildNode(child,check);
				});
			}
		}
		node.eachChild(function (child) {
			setChildNode(child,checked);
		});
		//如果选中当前节点，则所有父节点都选中
		if(checked){
			var parent_node = node.parentNode;
			while(parent_node != null){
				parent_node.set('checked', true);
				parent_node = parent_node.parentNode;
			}
		}
	}

	var group_store = new Ext.data.JsonStore({
	    storeId: 'group_name',
	    proxy: {
	        type: 'ajax',
	        //url: APP_ROOT+'/demo/data/auth_menu_group1.json',
	        url: APP_ROOT+'/auth/getAllGroup.do',
	        reader: {
	            type: 'json',
	            idProperty: 'name'
	        }
	    },
	    fields: ['id','group_name', 'group_desc'],
	    autoLoad: true
	});
	
	var group_grid = Ext.create('Ext.grid.Panel', {
	    store: group_store,
	    title:'用户组列表',
	    selModel:new Ext.selection.CheckboxModel({mode:'SINGLE'}),
	    columns : [
	        {header : "组名",autoHeight : true,align : 'left',dataIndex : 'group_name',sortable:true,flex:1},
    		{header : "组描述",autoHeight : true,align : 'left',dataIndex : 'group_desc',sortable:true,flex:1}
	    ],
	    listeners:{
	    	selectionchange : groupCheck
		},
	    tbar: [{
					xtype:'button',
					text:'查询',
					iconCls:'Zoom',
					handler:function(){
						group_store.load();
						tree_store.load({params:{}});
					}
			   },{
					xtype:'button',
					text:'添加组',
					iconCls:'Groupadd',
					disabled:true,
					handler:function(){
						group_store.load();
			   		}
			   },{
					xtype:'button',
					text:'删除组',
					disabled:true,
					iconCls:'Groupdelete',
					handler:function(){
						group_store.load();
			   		}
			   },{
					xtype:'button',
					text:'编辑组',
					disabled:true,
					iconCls:'Groupedit',
					handler:function(){
						group_store.load();
			   		}
			   }
		],
	    //width:500,
	    //autoWidth:true,
	    //height: 500,
	    //autoHeight:true,
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
	 var tree_store = Ext.create('Ext.data.TreeStore', {
			proxy : {
				type : 'ajax',
				//url : APP_ROOT+'/demo/data/auth_menu_group2.json',
				url: APP_ROOT+'/auth/getJsonCheckedMenuByGroupId.do',
				reader : {
					type : 'json'
				},
				expanded : true
			},
			fields: ['id', 'text','title','expanded','iconCls','leaf','children','url'],
		    autoLoad: false
		});
	
	var tree_panel = Ext.create('Ext.tree.Panel', {
			store : tree_store,
			title : '菜单分配',
			//split : true,
			//width : 700,
			autoWidth:true,
			collapsible : false,
			animCollapse : true,
			margins : '0 0 0 0',
			//height : 500,
			//autoHeight:true,
			border:false,
			//autoHeight:true,
			rootVisible : false,
			listeners : {
				checkchange:treeCheckChange
			},
			//fields : ['text','iconCls', 'expanded'],
			//root: {expanded: true,text:'菜单根节点',id:0,iconCls:'Textrotate0',leaf:false},
			columns : [{
						xtype : 'treecolumn',
						text : '菜单名',
						dataIndex : 'text',
						//width : 300,
						flex:1,
						sortable : false
					}, {
						text : '资源地址',
						flex : 1,
						dataIndex : 'url',
						sortable : false
					}],
		    tbar: [{
					xtype:'button',
					text:'刷新',
					iconCls:'Zoom',
					handler:function(){
						var sm = group_grid.getSelectionModel();
						if(!sm.hasSelection()){
							Ext.example.msg('','请先选中待编辑的用户组');
							tree_store.load({params:{}});
							return;
						}else{
							var record = sm.getSelection()[0];
							tree_store.load({params:{group_id:record.get('id')}});
						}
					}
				},{
					xtype:'button',
					text:'展开',
					iconCls:'Applicationsideexpand',
					handler:function(){
						tree_panel.expandAll();
					}
				},{
					xtype:'button',
					text:'保存权限',
					iconCls:'save',
					handler:function(){
						var sm = group_grid.getSelectionModel();
						if(!sm.hasSelection()){
							Ext.example.msg('','请先选中要保存的用户组');
							return;
						}
						var record = sm.getSelection()[0];
						var group_id = record.get('id');
						var group_name = record.get('group_name');
						id_list = "";
						getAllCheckedNodes(tree_panel.getRootNode());
						
						Ext.MessageBox.confirm("报错", "确认保存对组 "+group_name+" 的修改?", function (btn) {
				        if(btn == 'yes'){
							Ext.Ajax.request({
				                url: APP_ROOT+'/auth/addMenuGroup.do',
				                params: { group_id: group_id,id_list:id_list},
				                method: 'post',
				                success: function (response, options) {
				                    var result = Ext.JSON.decode(response.responseText);
				                    if(result.success){
					                    Ext.example.msg('','保存成功');
					                    tree_store.load({params:{group_id:record.get('id')}});
				                    }else{
				                    	Ext.Msg.alert('保存失败', result.msg);
				                    }
				                },
				                failure: function (response, options) {
				                    Ext.Msg.alert('保存失败', action.result.msg);
				                }
				            });
				        }
				    });
					}
				}
			]
		});
		
	var panel_1 = Ext.create('Ext.Panel', {title:'|'});
	var panel = Ext.create('Ext.Panel', {
		//width:300,
        renderTo:random_id,
		autoWidth : true,
		//height:500,
        //title:'hbox布局',
        layout:'column',
        layout_: {
			type: 'hbox',
    		pack: 'start',
    		align: 'stretch'
		},
        //items:[group_grid,tree_panel],
        items:[{
		        	columnWidth: .44,
		        	items:[group_grid]
		        },{
		        	columnWidth: .01,
		        	items:[panel_1]
		        },{
		        	columnWidth: .55,
		        	items:[tree_panel]
		        }
        ]
	});
	
	function groupCheck(){
		var sm = group_grid.getSelectionModel();
		if(!sm.hasSelection()){
			return;
		}
		var record = sm.getSelection()[0];
		tree_store.load({params:{group_id:record.get('id')}});
	}
	
	function getAllCheckedNodes(node){
    	var childnodes = node.childNodes;
    	Ext.each(childnodes, function (){
	        var childnode = this;
	        if(childnode.get('checked')){
	        	if(id_list == ""){
	        		id_list = childnode.raw.id;
	        	}else{
	        		id_list = id_list + "," + childnode.raw.id;
	        	}
	        }else{
	        	return;
	        }
	        if(childnode.hasChildNodes()){
	            getAllCheckedNodes(childnode);
	        }  
	    });
    }
});
