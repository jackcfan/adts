Ext.onReady(function() {
	var group_id;
	var id_list = "";
	var store = new Ext.data.JsonStore({
	    storeId: 'revision',
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/auth/getAllGroup.do',
	        reader: {
	            type: 'json',
	            root: 'images',
	            idProperty: 'name'
	        }
	    },
	    fields: ['id', 'group_name','group_desc'],
	    autoLoad: true
	});
	
	var menu_store = Ext.create('Ext.data.TreeStore', {
			proxy : {
				type : 'ajax',
				url: APP_ROOT+'/auth/getJsonCheckedMenuByGroupId.do',
				reader : {
					type : 'json'
				},
				expanded : true
			},
			fields: ['id', 'text','title','expanded','iconCls','leaf','children','url'],
		    autoLoad: false
		});
		
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        {header : "组ID",width: 80,autoHeight : true,align : 'left',dataIndex : 'id',sortable:true},
	        {header : "组名",width: 200,autoHeight : true,align : 'left',dataIndex : 'group_name',sortable:true},
    		{header : "组描述",width: 200,autoHeight : true,align : 'left',dataIndex : 'group_desc',sortable:true}
	    ],
	    tbar: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			},{
					xtype:'button',
					text:'添加组',
					iconCls:'Groupadd',
					handler:function(){
						Ext.getBody().mask();
						auth_group_form.getForm().reset();
						auth_group_win.setTitle('添加组');
						auth_group_win.setIconCls('Groupadd');
						auth_group_win.show().center();
						
						auth_group_form.getForm().url = APP_ROOT+'/auth/addGroup.do';
			   		}
			   },{
					xtype:'button',
					text:'删除组',
					iconCls:'Groupdelete',
					handler:function(){
						var sm = grid.getSelectionModel();
						if(!sm.hasSelection()){
							Ext.example.msg('','请先选中要删除的组');
							return;
						}
						var record = sm.getSelection()[0];
						
						var id   = record.get('id');
						var group_name = record.get('group_name');
						Ext.MessageBox.confirm("删除", "确认删除组 "+group_name+" ?", function (btn) {
					        if(btn == 'yes'){
								Ext.Ajax.request({
					                url: APP_ROOT+'/auth/deleteGroup.do',
					                params: { id: id},
					                method: 'post',
					                success: function (response, options) {
					                    var result = Ext.JSON.decode(response.responseText);
					                    if(result.success){
						                    Ext.example.msg('','删除成功');
					                    }else{
					                    	Ext.Msg.alert('保存失败', result.msg);
					                    }
					                    store.load();
					                },
					                failure: function (response, options) {
					                    Ext.Msg.alert('删除失败', action.result.msg);
					                    tree_store.load();
					                }
					            });
					        }
					    });
			   		}
			   },{
					xtype:'button',
					text:'编辑组',
					iconCls:'Groupedit',
					handler:function(){
						var sm = grid.getSelectionModel();
						if(!sm.hasSelection()){
							Ext.example.msg('','请先选中要编辑的组');
							return;
						}
						var record = sm.getSelection()[0];
						Ext.getBody().mask();
						auth_group_form.getForm().reset();
						auth_group_win.setTitle('编辑组');
						auth_group_win.show().center();
						auth_group_win.setIconCls('Groupedit');
						
						setFormPanelFieldValue(auth_group_form,'id',record.get('id'));
						setFormPanelFieldValue(auth_group_form,'group_name',record.get('group_name'));
						setFormPanelFieldValue(auth_group_form,'group_desc',record.get('group_desc'));
						
						auth_group_form.getForm().url = APP_ROOT+'/auth/updateGroup.do';
			   		}
			   },{
					xtype:'button',
					text:'用户管理',
					iconCls:'Usertick',
					handler:function(){
						var sm = grid.getSelectionModel();
						if(!sm.hasSelection()){
							Ext.example.msg('','请先选中要编辑的组');
							return;
						}
						var record = sm.getSelection()[0];
						Ext.getBody().mask();
						group_id = record.get('id');
						user_store.load({params:{id:group_id}});
						user_win.show();
						user_win.center();
			   		}
			   },{
					xtype:'button',
					text:'菜单分配',
					iconCls:'Grouplink',
					handler:function(){
						var sm = grid.getSelectionModel();
						if(!sm.hasSelection()){
							Ext.example.msg('','请先选中要编辑的组');
							return;
						}
						var record = sm.getSelection()[0];
						Ext.getBody().mask();
						group_id = record.get('id');
						menu_store.load({params:{group_id:group_id}});
						menu_win.show();
						menu_win.center();
			   		}
			   }
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: Ext.get('center-panel').getHeight() - 40,
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
	var auth_group_form = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            width: 350,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            defaultType: 'textfield',
            items: [{
                fieldLabel: '组名',
                name: 'group_name',
                allowBlank: false
            },{
                fieldLabel: '组描述',
                name: 'group_desc',
                allowBlank: false
            },{
                fieldLabel: 'id',
                name: 'id',
                allowBlank: true,
                xtype: 'hiddenfield'
            }],
            // Reset and Submit buttons
            buttons: [{
                text: '重置',
                handler: function() {
                    this.up('form').getForm().reset();
                }
            },{
                text: '关闭',
                handler: function() {
                    auth_group_win.hide();
                    Ext.getBody().unmask();
                }
            }, {
                text: '提交',
                formBind: true, //only enabled once the form is valid
                disabled: true,
                handler: function() {
                    var form = this.up('form').getForm();
                    if (form.isValid()) {
                        form.submit({
						    waitMsg: 'Loading...',
						    method: 'POST',
                            success: function(form, action) {
                               Ext.example.msg('','操作成功');
                               auth_group_win.hide();
                               Ext.getBody().unmask();
                               store.load();
                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('操作失败', action.result.msg);
                            }
                        });
                    }
                }
            }]
        });
	
	var auth_group_win = Ext.create('Ext.window.Window', {
		resizable: false,
		closable : false,
    	title: '',
    	items:[auth_group_form]
    });
    
    var user_store = new Ext.data.JsonStore({
	    storeId: 'revision',
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/auth/getAllUserWithinGroup.do',
	        reader: {
	            type: 'json',
	            root: 'images',
	            idProperty: 'name'
	        }
	    },
	    fields: ['username', 'realname','state','tel_no','email','create_time','last_login_time','group_state','user_id'],
	    autoLoad: false
	});
	
	var user_grid = Ext.create('Ext.grid.Panel', {
	    store: user_store,
	    columns : [
	        {header : "账户名",width: 150,autoHeight : true,align : 'left',dataIndex : 'username',sortable:true},
    		{header : "实名",width: 90,autoHeight : true,align : 'left',dataIndex : 'realname',sortable:true},
    		{header : "状态",width: 100,autoHeight : true,align : 'left',dataIndex : 'state',sortable:true},
    		{header : "手机号",width: 130,autoHeight : true,align : 'left',dataIndex : 'tel_no',sortable:true},
    		{header : "创建时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'create_time',sortable:true},
    		{header : "操作",width: 120,autoHeight : true,align : 'left',dataIndex : '',sortable:true,
    			renderer:function (v, m, r) {
				    var id = Ext.id();
				    var group_state = r.get('group_state');
				    var text;var icon;
				    var user_id = r.get('user_id');
				    if(group_state == 'N'){
				    	text = '加入';
				    	icon = 'Add';
				    }else{
				    	text = '移除';
				    	icon = 'Delete';
				    }
				    Ext.defer(function () {
				        Ext.widget('button', {
				            renderTo: id,
				            text: text,
				            iconCls:icon,
				            handler: function () {editUser(user_id,group_id,group_state);}
				        });
				    }, 50);
				    return Ext.String.format('<div id="{0}"></div>', id);
				}
  			}
	    ],
	    tbar__: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			},{
				xtype:'button',
				text:'添加',
				iconCls:'Useradd',
				handler:function(){
					Ext.getBody().mask();
					auth_user_win.setIconCls('Useradd');
					auth_user_form.getForm().reset();
					auth_user_win.setTitle('新增用户');
					auth_user_win.show().center();
				}
			},{
				xtype:'button',
				text:'编辑',
				iconCls:'Useredit',
				disabled:true,
				handler:function(){
					auth_user_win.setIconCls('Useredit');
					
					auth_user_form.getForm().reset();
					auth_user_win.setTitle('编辑用户');
					auth_user_win.show().center();
				}
			},{
				xtype:'button',
				text:'删除',
				disabled:true,
				iconCls:'Userdelete',
				handler:function(){
					store.load();
				}
			}
		],
	    //plugins:new Ext.grid.plugins.AutoResize(),
	    autoWidth : true,
	    //height: Ext.get('center-panel').getHeight() - 40,
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true
	    	//enableTextSelection:true
	    }
	});
	
	var user_win = Ext.create('Ext.window.Window', {
		resizable: false,
		closable : true,
		closeAction: 'hide',
    	title: '用户管理',
    	iconCls:'Usertick',
    	height: 400,  
    	width: 780,  
    	layout: 'fit', 
    	items:[user_grid],
    	listeners:{
	        'beforehide':function(win){
	            Ext.getBody().unmask();
	        }
	    }
    });
    
    function treeCheckChange(node, checked){
		//当前节点选中则所有子节点自动选中
		//当前节点取消选中则所有子节点自动取消选中
		var setChildNode = function(node,check){
			node.set('checked',check);
			if(node.isNode){
				node.eachChild(function (child) {
					setChildNode(child,check);
				});
			}
		}
		node.eachChild(function (child) {
			setChildNode(child,checked);
		});
		//如果选中当前节点，则所有父节点都选中
		if(checked){
			var parent_node = node.parentNode;
			while(parent_node != null){
				parent_node.set('checked', true);
				parent_node = parent_node.parentNode;
			}
		}
	}
    
    var menu_panel = Ext.create('Ext.tree.Panel', {
			store : menu_store,
			//split : true,
			//width : 700,
			autoWidth:true,
			collapsible : false,
			animCollapse : true,
			margins : '0 0 0 0',
			//height : 500,
			//autoHeight:true,
			border:false,
			//autoHeight:true,
			rootVisible : false,
			listeners : {
				checkchange:treeCheckChange
			},
			//fields : ['text','iconCls', 'expanded'],
			//root: {expanded: true,text:'菜单根节点',id:0,iconCls:'Textrotate0',leaf:false},
			columns : [{
						xtype : 'treecolumn',
						text : '菜单名',
						dataIndex : 'text',
						//width : 300,
						flex:1,
						sortable : false
					}, {
						text : '资源地址',
						flex : 1,
						dataIndex : 'url',
						sortable : false
					}],
		    tbar: [{
					xtype:'button',
					text:'刷新',
					iconCls:'Zoom',
					handler:function(){
						var sm = grid.getSelectionModel();
						if(!sm.hasSelection()){
							Ext.example.msg('','请先选中要编辑的组');
							return;
						}
						var record = sm.getSelection()[0];
						menu_store.load({params:{group_id:record.get('id')}});
					}
				},{
					xtype:'button',
					text:'展开',
					iconCls:'Applicationsideexpand',
					handler:function(){
						menu_panel.expandAll();
					}
				},{
					xtype:'button',
					text:'保存权限',
					iconCls:'save',
					handler:function(){
						var sm = grid.getSelectionModel();
						if(!sm.hasSelection()){
							Ext.example.msg('','请先选中要编辑的组');
							return;
						}
						var record = sm.getSelection()[0];
						var group_id = record.get('id');
						var group_name = record.get('group_name');
						id_list = "";
						getAllCheckedNodes(menu_panel.getRootNode());
						
						Ext.MessageBox.confirm("报错", "确认保存对组 "+group_name+" 的修改?", function (btn) {
					        if(btn == 'yes'){
								Ext.Ajax.request({
					                url: APP_ROOT+'/auth/addMenuGroup.do',
					                params: { group_id: group_id,id_list:id_list},
					                method: 'post',
					                success: function (response, options) {
					                    var result = Ext.JSON.decode(response.responseText);
					                    if(result.success){
						                    Ext.example.msg('','保存成功');
						                    menu_store.load({params:{group_id:record.get('id')}});
					                    }else{
					                    	Ext.Msg.alert('保存失败', result.msg);
					                    }
					                },
					                failure: function (response, options) {
					                    Ext.Msg.alert('保存失败', action.result.msg);
					                }
					            });
					        }
					    });
					}
				}
			]
		});
    
    var menu_win = Ext.create('Ext.window.Window', {
		resizable: false,
		closable : true,
		closeAction: 'hide',
    	title: '菜单分配',
    	iconCls:'Grouplink',
    	height: 400,  
    	width: 780,  
    	layout: 'fit', 
    	items:[menu_panel],
    	listeners:{
	        'beforehide':function(win){
	            Ext.getBody().unmask();
	        }
	    }
    });
    
    function editUser(user_id,group_id,state){
    	var url;
    	if(state == 'N'){
    		url = APP_ROOT+'/auth/addUserToGroup.do';
    	}else{
    		url = APP_ROOT+'/auth/delUserFromGroup.do';
    	}
    	Ext.Ajax.request({
            url: url,
            params: { user_id: user_id,group_id:group_id},
            method: 'post',
            success: function (response, options) {
                var result = Ext.JSON.decode(response.responseText);
                if(result.success){
                    Ext.example.msg('','操作成功');
                }else{
                	Ext.Msg.alert('操作失败', result.msg);
                }
                user_store.load({params:{id:group_id}});
            },
            failure: function (response, options) {
                Ext.Msg.alert('操作失败', action.result.msg);
                user_store.load({params:{id:group_id}});
            }
        });
    }
    
    function getAllCheckedNodes(node){
    	var childnodes = node.childNodes;
    	Ext.each(childnodes, function (){
	        var childnode = this;
	        if(childnode.get('checked')){
	        	if(id_list == ""){
	        		id_list = childnode.raw.id;
	        	}else{
	        		id_list = id_list + "," + childnode.raw.id;
	        	}
	        }else{
	        	return;
	        }
	        if(childnode.hasChildNodes()){
	            getAllCheckedNodes(childnode);
	        }  
	    });
    }
});