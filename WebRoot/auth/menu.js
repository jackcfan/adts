Ext.onReady(function() {
	Ext.QuickTips.init();

	var tree_store = Ext.create('Ext.data.TreeStore', {
		proxy : {
			type : 'ajax',
			//url : APP_ROOT+'/demo/data/auth_menu.json',
			url : APP_ROOT+'/auth/getAllJsonMenu.do',
			reader : {
				type : 'json'
			},
			expanded : true
		},
		fields: ['id', 'text','title','expanded','iconCls','leaf','children','url'],
	    autoLoad: true
	});

	var auth_menu = Ext.create('Ext.tree.Panel', {
		store : tree_store,
		renderTo : random_id,
		height : getNewTabHeight(),
		plugins:new Ext.grid.plugins.AutoResize(),
		split : true,
		autoWidth : true,
		collapsible : false,
		animCollapse : true,
		margins : '0 0 0 5',
		rootVisible : false,
		//fields : ['text','iconCls', 'expanded'],
		//root: {expanded: true,text:'菜单根节点',id:0,iconCls:'Textrotate0',leaf:false},
		columns : [{
					xtype : 'treecolumn',
					text : '菜单名',
					dataIndex : 'text',
					width : 350,
					sortable : false
				}, {
					text : 'Tab 标签名',
					width : 150,
					dataIndex : 'title',
					sortable : false
				}, {
					text : '自动展开',
					width : 90,
					sortable : false,
					renderer:function(value,metaData,record,row,col,store,gridView){
						if(record.get("id") == 0){return '';}
						if(record.get('expanded')){
							return 'Y';
						}
						return 'N';
					}
				}, {
					text : '图标样式',
					width : 90,
					dataIndex : 'iconCls',
					sortable : false,
					renderer:function(value,metaData,record,row,col,store,gridView){
						if(record.get("id") == 0){return '';}
						return value;
					}
				}, {
					text : '资源地址',
					flex : 1,
					dataIndex : 'url',
					sortable : false
				}],
	    tbar: [{
				xtype:'button',
				text:'刷新',
				iconCls:'Zoom',
				handler:function(){
					//tree_store.reload();
					auth_menu.getStore().reload();
				}
			},{
				xtype:'button',
				text:'展开',
				iconCls:'Applicationsideexpand',
				handler:function(){
					auth_menu.expandAll();
				}
			},{
				xtype:'button',
				text:'新增菜单',
				iconCls:'Add',
				handler:function(){
					var sm = auth_menu.getSelectionModel();
					if(!sm.hasSelection()){
						Ext.example.msg('','请先选中菜单节点');
						return;
					}
					Ext.getBody().mask();
					auth_menu_form.getForm().reset();
					auth_menu_win.setTitle('新增菜单');
					auth_menu_win.show().center();
					
					var record = sm.getSelection()[0];
					setFormPanelFieldValue(auth_menu_form,'parent_id',record.get('id'));
					
					auth_menu_form.getForm().url = APP_ROOT+'/auth/addMenu.do';
				}
			},{
				xtype:'button',
				text:'删除菜单',
				iconCls:'Delete',
				handler:function(){
					var sm = auth_menu.getSelectionModel();
					if(!sm.hasSelection()){
						Ext.example.msg('','请先选中菜单节点');
						return;
					}
					var record = sm.getSelection()[0];
					if(record.get('id') == '0'){
						Ext.example.msg('','请勿删除根节点');
						return;
					}
					
					if(record.childNodes.length > 0){
						Ext.example.msg('','请先删除子菜单');
						return;
					}
					var text = record.get('text');
					var id   = record.get('id');
					Ext.MessageBox.confirm("删除", "确认删除菜单 : "+text+" ?", function (btn) {
				        if(btn == 'yes'){
							Ext.Ajax.request({
				                url: APP_ROOT+'/auth/deleteMenu.do',
				                params: { id: id},
				                method: 'post',
				                success: function (response, options) {
				                    var result = Ext.JSON.decode(response.responseText);
				                    if(result.success){
					                    Ext.example.msg('','删除成功');
				                    }else{
				                    	Ext.Msg.alert('保存失败', result.msg);
				                    }
				                    tree_store.load();
				                },
				                failure: function (response, options) {
				                    Ext.Msg.alert('删除失败', action.result.msg);
				                    tree_store.load();
				                }
				            });
				        }
				    });
				}
			},{
				xtype:'button',
				text:'编辑菜单',
				iconCls:'Applicationedit',
				handler:function(){
					var sm = auth_menu.getSelectionModel();
					if(!sm.hasSelection()){
						Ext.example.msg('','请先选中菜单节点');
						return;
					}
					var record = sm.getSelection()[0];
					if(record.get('id') == '0'){
						Ext.example.msg('','请勿修改根节点属性');
						return;
					}
					Ext.getBody().mask();
					auth_menu_form.getForm().reset();
					auth_menu_win.setTitle('编辑菜单');
					auth_menu_win.show().center();
					
					setFormPanelFieldValue(auth_menu_form,'text',record.get('text'));
					setFormPanelFieldValue(auth_menu_form,'title',record.get('title'));
					setFormPanelFieldValue(auth_menu_form,'expanded',record.get('expanded')==true?'Y':'N');
					setFormPanelFieldValue(auth_menu_form,'iconcls',record.get('iconCls'));
					setFormPanelFieldValue(auth_menu_form,'url',record.get('url'));
					setFormPanelFieldValue(auth_menu_form,'id',record.get('id'));
					
					auth_menu_form.getForm().url = APP_ROOT+'/auth/updateMenu.do';
				}
			}
		]
	});
	
	var auth_menu_form = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            width: 350,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            defaultType: 'textfield',
            items: [{
                fieldLabel: '菜单名',
                name: 'text',
                allowBlank: false
            },{
                fieldLabel: 'Tab 标签名',
                name: 'title',
                allowBlank: true
            },{
                fieldLabel: '自动展开',
                name: 'expanded',
                allowBlank: true,
                xtype:'combo',
                store: new Ext.data.SimpleStore({
					fields:['value','text'],
					data:[['Y','Yes'],['N','No']]
				}),
				valueField : 'value',
				displayField : 'text',
				editable:false
            },{
                fieldLabel: '图标样式',
                name: 'iconcls',
                allowBlank: true
            },{
                fieldLabel: '资源地址',
                name: 'url',
                allowBlank: true
            },{
                fieldLabel: 'id',
                name: 'id',
                allowBlank: true,
                xtype: 'hiddenfield'
            },{
                fieldLabel: 'parent_id',
                name: 'parent_id',
                allowBlank: false,
                xtype: 'hiddenfield'
            }],
            // Reset and Submit buttons
            buttons: [{
                text: '重置',
                handler: function() {
                    this.up('form').getForm().reset();
                }
            },{
                text: '关闭',
                handler: function() {
                    auth_menu_win.hide();
                    Ext.getBody().unmask();
                }
            }, {
                text: '提交',
                formBind: true, //only enabled once the form is valid
                disabled: true,
                handler: function() {
                    var form = this.up('form').getForm();
                    if (form.isValid()) {
                        form.submit({
						    waitMsg: 'Loading...',
						    method: 'POST',
                            success: function(form, action) {
                               Ext.example.msg('','操作成功');
                               auth_menu_win.hide();
                               Ext.getBody().unmask();
                               tree_store.load();
                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('操作失败', action.result.msg);
                            }
                        });
                    }
                }
            }]
        });
	
	var auth_menu_win = Ext.create('Ext.window.Window', {
		resizable: false,
		closable : false,
    	title: '',
    	items:[auth_menu_form]
    });
    
});