Ext.onReady(function() {
	var store = new Ext.data.JsonStore({
	    storeId: 'revision',
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/manage/getThisWeekReport.do',
	        reader: {
	            type: 'json',
	            root: 'images',
	            idProperty: 'name'
	        }
	    },
	    fields: ['username', 'realname','id','parent_id','begin_date','state','work_dtl','remark','priority','work_type','component','create_time'],
	    autoLoad: true
	});
	
	var action;
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        {header : "开始日期",width: 90,autoHeight : true,align : 'left',dataIndex : 'begin_date',sortable:true},
	        {header : "星期",width: 60,autoHeight : true,align : 'left',dataIndex : 'begin_date',sortable:true,
	            renderer:function(value,metaData,record,row,col,store,gridView){
	            	var date = new Date(value.substr(0,4),value.substr(4,2) - 1,value.substr(6,2));
	            	var day = date.getDay();
	            	if(day == 0){return '周天'}
	            	if(day == 1){return '周一'}
	            	if(day == 2){return '周二'}
	            	if(day == 3){return '周三'}
	            	if(day == 4){return '周四'}
	            	if(day == 5){return '周五'}
	            	if(day == 6){return '周六'}
	            }
	        },
	        {header : "姓名",width: 80,autoHeight : true,align : 'left',dataIndex : 'realname',sortable:true},
    		{header : "工作内容",width: 350,autoHeight : true,align : 'left',dataIndex : 'work_dtl',sortable:true},
    		{header : "备注",width: 230,autoHeight : true,align : 'left',dataIndex : 'remark',sortable:true},
    		{header : "进度",width: 60,autoHeight : true,align : 'left',dataIndex : 'state',sortable:true},
    		{header : "优先级",width: 60,autoHeight : true,align : 'left',dataIndex : 'priority',sortable:true},
    		{header : "类型",width: 70,autoHeight : true,align : 'left',dataIndex : 'work_type',sortable:true},
    		{header : "创建时间",width: 140,autoHeight : true,align : 'left',dataIndex : 'create_time',sortable:true}
	    ],
	    tbar: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: Ext.get('center-panel').getHeight() - 40,
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
});