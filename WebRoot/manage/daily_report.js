Ext.onReady(function() {
	var store = new Ext.data.JsonStore({
	    storeId: 'revision',
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/manage/getDailyReport.do',
	        reader: {
	            type: 'json',
	            root: 'images',
	            idProperty: 'name'
	        }
	    },
	    fields: ['username', 'realname','id','parent_id','begin_date','state','work_dtl','remark','priority','work_type','component','create_time'],
	    autoLoad: true
	});
	
	var action;
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        {header : "开始日期",width: 90,autoHeight : true,align : 'left',dataIndex : 'begin_date',sortable:true},
	        {header : "星期",width: 60,autoHeight : true,align : 'left',dataIndex : 'begin_date',sortable:true,
	            renderer:function(value,metaData,record,row,col,store,gridView){
	            	var date = new Date(value.substr(0,4),value.substr(4,2) - 1,value.substr(6,2));
	            	var day = date.getDay();
	            	if(day == 0){return '周天'}
	            	if(day == 1){return '周一'}
	            	if(day == 2){return '周二'}
	            	if(day == 3){return '周三'}
	            	if(day == 4){return '周四'}
	            	if(day == 5){return '周五'}
	            	if(day == 6){return '周六'}
	            }
	        },
    		{header : "工作内容",width: 350,autoHeight : true,align : 'left',dataIndex : 'work_dtl',sortable:true},
    		{header : "备注",width: 230,autoHeight : true,align : 'left',dataIndex : 'remark',sortable:true},
    		{header : "进度",width: 60,autoHeight : true,align : 'left',dataIndex : 'state',sortable:true},
    		{header : "优先级",width: 60,autoHeight : true,align : 'left',dataIndex : 'priority',sortable:true},
    		{header : "类型",width: 70,autoHeight : true,align : 'left',dataIndex : 'work_type',sortable:true},
    		{header : "创建时间",width: 140,autoHeight : true,align : 'left',dataIndex : 'create_time',sortable:true}
	    ],
	    tbar: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			},{
				xtype:'button',
				text:'添加',
				iconCls:'Add',
				handler:function(){
					Ext.getBody().mask();
					report_form.getForm().reset();
					report_win.setTitle('新增日报');
					report_win.show().center();
					setFormPanelFieldValue(report_form,'begin_date',new Date());
					setFormPanelFieldValue(report_form,'work_dtl','');
					setFormPanelFieldValue(report_form,'remark','');
					setFormPanelFieldValue(report_form,'state','进行中');
					setFormPanelFieldValue(report_form,'priority','正常');
					setFormPanelFieldValue(report_form,'work_type','任务');
					setFormPanelFieldValue(report_form,'component','其他');
					setFormPanelFieldValue(report_form,'parent_id','');
					action = APP_ROOT+'/manage/insertDailyReport.do';
				}
			},{
				xtype:'button',
				text:'删除',
				iconCls:'Delete',
				handler:function(){
					var sm = grid.getSelectionModel();
					if(!sm.hasSelection()){
						Ext.example.msg('','请先选中要删除的记录');
						return;
					}
					var record = sm.getSelection()[0];
					Ext.MessageBox.confirm("删除", "确认删除选中日报?<br/>"+record.get('work_dtl'), function (btn) {
				        if(btn == 'yes'){
							Ext.Ajax.request({
				                url: APP_ROOT+'/manage/deleteDailyReport.do',
				                params: { id: record.get('id')},
				                method: 'post',
				                success: function (response, options) {
				                    var result = Ext.JSON.decode(response.responseText);
				                    if(result.success){
					                    Ext.example.msg('','删除成功');
				                    }else{
				                    	Ext.Msg.alert('保存失败', result.msg);
				                    }
				                    store.load();
				                },
				                failure: function (response, options) {
				                    Ext.Msg.alert('删除失败', action.result.msg);
				                    store.load();
				                }
				            });
				        }
				    });
				}
			},{
				xtype:'button',
				text:'修改',
				iconCls:'Applicationedit',
				handler:function(){
					var sm = grid.getSelectionModel();
					if(!sm.hasSelection()){
						Ext.example.msg('','请先选中要编辑的记录');
						return;
					}
					var record = sm.getSelection()[0];
					Ext.getBody().mask();
					report_form.getForm().reset();
					report_win.setTitle('编辑日报');
					report_win.show().center();
					setFormPanelFieldValue(report_form,'begin_date',record.get('begin_date'));
					setFormPanelFieldValue(report_form,'work_dtl',record.get('work_dtl'));
					setFormPanelFieldValue(report_form,'remark',record.get('remark'));
					setFormPanelFieldValue(report_form,'state',record.get('state'));
					setFormPanelFieldValue(report_form,'priority',record.get('priority'));
					setFormPanelFieldValue(report_form,'work_type',record.get('work_type'));
					setFormPanelFieldValue(report_form,'component',record.get('component'));
					setFormPanelFieldValue(report_form,'id',record.get('id'));
					setFormPanelFieldValue(report_form,'parent_id',record.get('parent_id'));
					action = APP_ROOT+'/manage/updateDailyReport.do';
				}
			},{
				xtype:'button',
				text:'拷贝',
				iconCls:'Pagecopy',
				handler:function(){
					var sm = grid.getSelectionModel();
					if(!sm.hasSelection()){
						Ext.example.msg('','请先选中要拷贝的记录');
						return;
					}
					var record = sm.getSelection()[0];
					Ext.getBody().mask();
					report_form.getForm().reset();
					report_win.setTitle('新增日报');
					report_win.show().center();
					setFormPanelFieldValue(report_form,'begin_date',new Date());
					setFormPanelFieldValue(report_form,'work_dtl',record.get('work_dtl'));
					setFormPanelFieldValue(report_form,'remark',record.get('remark'));
					setFormPanelFieldValue(report_form,'state',record.get('state'));
					setFormPanelFieldValue(report_form,'priority',record.get('priority'));
					setFormPanelFieldValue(report_form,'work_type',record.get('work_type'));
					setFormPanelFieldValue(report_form,'component',record.get('component'));
					setFormPanelFieldValue(report_form,'id',record.get('id'));
					setFormPanelFieldValue(report_form,'parent_id',record.get('parent_id'));
					action = APP_ROOT+'/manage/insertDailyReport.do';
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: Ext.get('center-panel').getHeight() - 40,
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
	var report_form = Ext.create('Ext.form.Panel', {
            bodyPadding: 5,
            width: 650,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            defaultType: 'textfield',
            items: [{
                fieldLabel: '开始日期',
                xtype:'datefield',
                format:'Ymd',
                name: 'begin_date',
                allowBlank: false
            },{
                fieldLabel: '工作内容',
                name: 'work_dtl',
                allowBlank: false,
                height:100,
                xtype:'textarea'
            },{
                fieldLabel: '备注',
                name: 'remark',
                height:80,
                xtype:'textarea'
            },{
                fieldLabel: '进度',
                name: 'state',
                xtype:'combo',
                store: new Ext.data.SimpleStore({
					fields:['value','text'],
					data:[['未开始','未开始'],['进行中','进行中'],['完成','完成'],['放弃','放弃']]
				}),
				valueField : 'value',
				displayField : 'text',
				editable:false
            },{
                fieldLabel: '优先级',
                name: 'priority',
                allowBlank: false,
                xtype:'combo',
                store: new Ext.data.SimpleStore({
					fields:['value','text'],
					data:[['次要','次要'],['正常','正常'],['紧急','紧急']]
				}),
				valueField : 'value',
				displayField : 'text',
				editable:false
            },{
                fieldLabel: '类型',
                name: 'work_type',
                allowBlank: false,
                xtype:'combo',
                store: new Ext.data.SimpleStore({
					fields:['value','text'],
					data:[['任务','任务'],['改进','改进'],['缺陷','缺陷'],['错误','错误']]
				}),
				valueField : 'value',
				displayField : 'text',
				editable:false
            },{
                fieldLabel: '组件',
                name: 'component',
                allowBlank: false,
                xtype:'combo',
                store: new Ext.data.SimpleStore({
					fields:['value','text'],
					data:[['前端','前端'],['后端','后端'],['基础环境','基础环境'],['模型','模型']
					     ,['测试环境运维','测试环境运维'],['生产环境运维','生产环境运维'],['模块设计','模块设计']
					     ,['部署','部署'],['需求','需求'],['其他','其他']
					     ]
				}),
				valueField : 'value',
				displayField : 'text',
				editable:false
            },{
                fieldLabel: 'ID',
                name: 'id',
                height:80,
                xtype:'hidden'
            },{
                fieldLabel: '父ID',
                name: 'parent_id',
                height:80,
                xtype:'hidden'
            }],
            // Reset and Submit buttons
            buttons: [{
                text: '关闭',
                handler: function() {
                    report_win.hide();
                    Ext.getBody().unmask();
                }
            }, {
                text: '提交',
                formBind: true, //only enabled once the form is valid
                disabled: true,
                handler: function() {
                    var form = this.up('form').getForm();
                    if (form.isValid()) {
                        form.submit({
                        	url: action,
						    waitMsg: 'Loading...',
						    method: 'POST',
                            success: function(form, action) {
                               Ext.example.msg('','操作成功');
                               report_win.hide();
                               Ext.getBody().unmask();
                               store.load();
                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('操作失败', action.result.msg);
                            }
                        });
                    }
                }
            }]
        });
	
	var report_win = Ext.create('Ext.window.Window', {
		resizable: false,
		closable : true,
		closeAction: 'hide',
    	title: '',
    	items:[report_form],
    	listeners:{
	        'beforehide':function(win){
	            Ext.getBody().unmask();
	        }
	    }
    });
    
});