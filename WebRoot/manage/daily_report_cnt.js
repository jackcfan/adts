Ext.onReady(function() {
	var store = new Ext.data.JsonStore({
	    storeId: 'revision',
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/manage/getDailyReportCnt.do',
	        reader: {
	            type: 'json',
	            root: 'images',
	            idProperty: 'name'
	        }
	    },
	    fields: ['begin_date', 'realname','cnt'],
	    autoLoad: true
	});
	
	var action;
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        {header : "当前日期",width: 90,autoHeight : true,align : 'left',dataIndex : 'begin_date',sortable:true},
    		{header : "姓名",width: 100,autoHeight : true,align : 'left',dataIndex : 'realname',sortable:true},
    		{header : "日报数",width: 80,autoHeight : true,align : 'left',dataIndex : 'cnt',sortable:true}
	    ],
	    tbar: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: Ext.get('center-panel').getHeight() - 40,
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
});