String.prototype.trim = function(){
	//字符串添加 trim 函数
	return this.replace(/(^\s*)|(\s*$)/g, "");
}

function trim(str){
	if(str == null || str.length == 0){return "";}
	if(typeof str == 'number'){return str;}
	str=str.replace(/(^\s*)|(\s*$)/g, ""); 
	return str;
}

function getCmpValue(id){
	var value = Ext.getCmp(id).getValue();
	if(typeof value == "undefined" || value == null){
		return '';
	}
	return trim(value);
}

function getNewTabHeight(){
	var theme = getQueryParam('theme') || 'neptune';  //当前主题
	var height_offset = 0;
	if(theme == 'neptune'){
		height_offset = 40;
	}else{
		height_offset = 27;
	}
	return Ext.get(CENTER_PANEL_ID).getHeight() - height_offset;
}

//根据指定的FormPanleId获得指定name组件的值
function getFormPanelFieldValue(formPanel,name){
	var field = formPanel.getForm().findField(name);
	if(typeof field == "undefined" || field == null){
		return '';
	}
	var value = field.getValue();
	if(typeof value == "undefined" || value == null){
		return '';
	}
	return value.trim();
}

//根据指定的FormPanleId设置指定name组件的值
function setFormPanelFieldValue(formPanel,name,value){
	var field = formPanel.getForm().findField(name);
	if(typeof field == "undefined" || field == null){
		return '';
	}
	field.setValue(value);
}

function addTabPanel(para){
	var title = para.title.trim();
	var url   = para.url;
	var icon  = para.iconCls;
	var id    = "tabid_" + para.id;
	var center_panel = Ext.getCmp(CENTER_PANEL_ID);
	for(var i=0;i<center_panel.items.items.length;i++){
		var item = center_panel.items.items[i];
		if(id == item.id){
			center_panel.items.items[i].show();
			return;
		}
	}
	var autoLoad;
	if(url.indexOf('.js') >= 0 && url.length - 3 == url.indexOf('.js')){
		autoLoad = {url:'ext.gridpanel.jsp',scripts:true,params:{js:url}};
	}else{
		autoLoad = {url:url,scripts:true};
	}
	Ext.getCmp(CENTER_PANEL_ID).add({
    	title: title,
    	id:id,
    	iconCls:icon||'Applicationviewlist',
        closable: true,
        autoScroll: true,
        autoLoad:autoLoad,
        autoShow : true,
        border : false,
        layout : 'fit'
    }).show();
}

function removeTabPanel(para){
	var id    = "tabid_" + para.id;
	var center_panel = Ext.getCmp(CENTER_PANEL_ID);
	for(var i=0;i<center_panel.items.items.length;i++){
		var item = center_panel.items.items[i];
		if(id == item.id){
			center_panel.items.items[i].destroy();
			return;
		}
	}
}

function getQueryParam(name, queryString) {
    var match = RegExp(name + '=([^&]*)').exec(queryString || location.search);
    return match && decodeURIComponent(match[1]);
}

function switchTheme(theme){
	var current_theme = getQueryParam('theme') || 'neptune';  //当前主题
	if(current_theme == theme){
		return;
	}else{
		window.location.href=APP_ROOT+'?theme='+theme;
	}
}