<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<br>&nbsp;&nbsp;&nbsp;&nbsp;ADTS 更新日志
<ul>
	<li>2015-07-17 加入工作日报维护</li>
	<li>2015-06-02 加入数据字典历史清理：只保留10个版本</li>
	<li>2015-05-28 加入DB2模型管理</li>
	<li>ADTS 加入主题切换</li>
	<li>ADTS 全新升级，支持所有主流浏览器，包括非主流的IE</li>
	<li>ADTS 加入权限管理模块</li>
</ul>