Ext.onReady(function() {
	var diff_store = new Ext.data.JsonStore({
		proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/etl_svn/doSummarizeDiff.do',
	        reader: {
	            type: 'json'
	        }
	    },
	    fields: ['path','type'],
	    timeout:9999999999999
	});
	
	var env_store = new Ext.data.JsonStore({
	    proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/etl_svn/getEnvList.do',
	        reader: {
	            type: 'json'
	        }
	    },
	    fields: ['name'],
	    autoLoad: true
	});
	
	var app_store = new Ext.data.JsonStore({
	    proxy: {
	        type: 'ajax',
	        url:APP_ROOT+'/etl_svn/getAppList.do',
	        reader: {
	            type: 'json'
	        }
	    },
	    fields: ['name'],
	    autoLoad: true
	});
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: diff_store,
	    selModel:selModel,
	    columns : [
	        new Ext.grid.RowNumberer({width:30}),
	        {header : "路径",width: 650,autoHeight : true,align : 'left',dataIndex : 'path',sortable:true},
    		{header : "状态",width: 90,autoHeight : true,align : 'left',dataIndex : 'type',sortable:true}
	    ],
	    tbar: ['环境',
            {
				xtype:'combo',
				id:'adts_etl_svn_svn_diff_env_name',
				//name:'env_name',
				store:env_store,
				width:80,
				valueField : 'name',
				displayField : 'name',
				triggerAction : "all",
				selectOnFocus:true,
				editable:false,
				mode : 'local'
			},'&nbsp;APP',
            {
				xtype:'combo',
				id:'adts_etl_svn_svn_diff_app_name',
				//name:'app_name',
				store:app_store,
				width:80,
				valueField : 'name',
				displayField : 'name',
				triggerAction : "all",
				selectOnFocus:true,
				//editable:false,
				disabled:true,
				mode : 'local',
				value:''
			},'-',{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					var env = getCmpValue('adts_etl_svn_svn_diff_env_name');
					var app = getCmpValue('adts_etl_svn_svn_diff_app_name');
					if(env == ''){
						Ext.example.msg('','请先选择要对比的环境名');
						return;
					}
					diff_store.load();
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: getNewTabHeight(),
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
	
	diff_store.on('beforeload', function(store, options) {
		diff_store.removeAll();
		var env = getCmpValue('adts_etl_svn_svn_diff_env_name');
		var app = getCmpValue('adts_etl_svn_svn_diff_app_name');
		Ext.apply(store.proxy.extraParams, {env:env,etl_system:app});
	});
	
});