Ext.onReady(function() {
	var store = new Ext.data.JsonStore({
	    storeId: 'revision',
	    proxy: {
	        type: 'ajax',
	        url: APP_ROOT+'/version/getEtlUpdateList.do',
	        reader: {
	            type: 'json',
	            idProperty: 'name'
	        }
	    },
	    fields: ['name', 'lastauthor','lastchangeddate','fullpath'],
	    autoLoad: true,
	    sorters:[{
	    	property:'name',direction:'DESC'
	    }]
	});
	
	var selModel = new Ext.selection.CheckboxModel({mode:'SINGLE'});
	var grid = Ext.create('Ext.grid.Panel', {
	    store: store,
	    selModel:selModel,
	    columns : [
	        {header : "版本号",width: 80,autoHeight : true,align : 'left',dataIndex : 'name',sortable:true},
    		{header : "最近更新作者",width: 150,autoHeight : true,align : 'left',dataIndex : 'lastauthor',sortable:true},
    		{header : "最近更新时间",width: 150,autoHeight : true,align : 'left',dataIndex : 'lastchangeddate',sortable:true,
    			renderer:function (v, m, r) {
    				return v.replace('T',' ');
    			}
    		}/*,
    		{header : "查看列表",width: 130,autoHeight : true,align : 'left',dataIndex : 'fullpath',sortable:true},
    		{header : "打包下载",width: 150,autoHeight : true,align : 'left',dataIndex : 'create_time',sortable:true}*/
	    ],
	    tbar: [{
				xtype:'button',
				text:'查询',
				iconCls:'Zoom',
				handler:function(){
					store.load();
				}
			},{
				xtype:'button',
				text:'打包',
				iconCls:'Packagedown',
				handler:function(){
					var sm = grid.getSelectionModel();
					if(!sm.hasSelection()){
						Ext.example.msg('','请先选中要打包的版本');
						return;
					}
					var record = sm.getSelection()[0];
					window.open(APP_ROOT+"/version/downloadPackage.do?version="+record.get('name'));
				}
			}
		],
	    plugins:new Ext.grid.plugins.AutoResize(),
	    renderTo: random_id,
	    autoWidth : true,
	    height: getNewTabHeight(),
	    border : false,
	    selType: 'rowmodel',
	    viewConfig:{
	    	forceFit:true,
	    	enableTextSelection:true
	    }
	});
});