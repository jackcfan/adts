<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
<ul class="sf-menu" id="example">
    <li class="current">
        <a href="#">ETL 工具箱</a>
        <ul>
            <li>
                <a href="#">menu item</a>
            </li>
            <li class="current">
                <a href="#">long menu item sets sub width</a>
                <ul>
                    <li class="current"><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                </ul>
            </li>
            <li>
                <a href="#">menu item</a>
                <ul>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                </ul>
            </li>
            <li>
                <a href="#">menu item</a>
                <ul>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
    	<a href="#">版本管理</a>
    	<ul>
            <li><a href="/adts/version/make_pkg.jsp">自动打包</a></li>
            <li><a href="#">自动对比</a></li>
            <li><a href="#">版本变更历史</a></li>
            <li><a href="#">生产部署说明</a></li>
    	</ul>
    </li>
    <li>
    	<a href="#">模型管理</a>
    	<ul>
            <li><a href="/adts/model/svn_his.do">提交历史</a></li>
            <li><a href="#">自动对比</a></li>
            <li><a href="#">版本变更历史</a></li>
            <li><a href="#">生产部署说明</a></li>
    	</ul>
    </li>
    <li>
        <a href="#">测试</a>
        <ul>
            <li>
                <a href="#">menu item</a>
                <ul>
                    <li><a href="#">short</a></li>
                    <li><a href="#">short</a></li>
                    <li><a href="#">short</a></li>
                    <li><a href="#">short</a></li>
                    <li><a href="#">short</a></li>
                </ul>
            </li>
            <li>
                <a href="#">menu item</a>
                <ul>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                </ul>
            </li>
            <li>
                <a href="#">menu item</a>
                <ul>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                </ul>
            </li>
            <li>
                <a href="#">menu item</a>
                <ul>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                </ul>
            </li>
            <li>
                <a href="#">menu item</a>
                <ul>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                    <li><a href="#">menu item</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">运维监控</a>
    </li>
</ul>
</div>