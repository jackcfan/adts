<%@page import="adts.util.ZipCompressor"%>
<%@page import="org.tmatesoft.svn.core.SVNException"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="adts.util.Universalchardet"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.tmatesoft.svn.core.SVNDepth"%>
<%@page import="org.tmatesoft.svn.core.wc.SVNRevision"%>
<%@page import="java.io.File"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="adts.util.SVNKitUtil"%>
<%@page import="adts.util.IniUtil"%>
<%@page import="adts.util.Util"%>
<%
	
	String version = request.getParameter("version");
	SVNKitUtil svn = new SVNKitUtil("ETL_SVN_CFG");
	if(version == null || version.trim().length() == 0){
		out.print("请输入版本号");
		return;
	}else{
		String version_dir    = "/ETL/init/update/" + version;
		String work_dir       = System.getenv("tmp")+"/adts/"+Util.getUUID();
		String zip_file       = work_dir + "/" + version + ".zip";
		try{
			File work = new File(work_dir + "/ETL/init/update/" + version);
			work.mkdirs();
			
			String des = work_dir + "/ETL/init/update/" + version+"/scripts_update.conf";
			File file = new File(des);
			svn.export(version_dir, new File(work_dir + "/ETL/init/update/" + version), SVNRevision.HEAD, SVNRevision.HEAD, null, true, SVNDepth.INFINITY);
			String encoding = Universalchardet.getFileEncoding(file);
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),encoding));
			String line;
			while((line = reader.readLine()) != null){
				line = line.trim();
				if(line.startsWith("#") || line.length() == 0){
					continue;
				}
				String svn_file = "/ETL/" + line;
				svn.export(svn_file, new File(work_dir + "/ETL/" + line) , SVNRevision.HEAD, SVNRevision.HEAD, null, true, SVNDepth.INFINITY);
			}
			reader.close();
		}catch(SVNException e){
			out.print(e.getMessage());
			return;
		}
		/*导出所有文件后打zip包  */
		if(ZipCompressor.zipDir(work_dir,zip_file) != 1){
			out.print("文件打包失败");
			return;
		}
		
		ServletOutputStream output = response.getOutputStream();
		String file_name = version + ".zip";
		File file = new File(work_dir+"/"+file_name);
		FileInputStream fis = new FileInputStream(file);
		
		response.setContentType("application/octet-stream;charset=ISO8859-1");
		response.setHeader("Content-Disposition", "attachment;filename="+new String(file_name.getBytes(),"ISO8859-1"));
		
		byte buff[] = new byte[1024 * 8];
		while(fis.read(buff) > 0){
			output.write(buff);
		}
		fis.close();
		output.close();
		/*避免报错  getOutputStream() has already been called for this response */
		out.clear();
		out = pageContext.pushBody();
	}
%>
