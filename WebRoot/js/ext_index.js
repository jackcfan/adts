var viewport;
var global_grid_height = 576;
var valid_col_type1 = ['BIGINT','DATE','INTEGER','NUMBER','NUMERIC','SMALLINT','TIME','TIMESTAMP'];
var valid_col_type2 = ['CHAR','CHARACTER','NARCHAR','NCHAR','NVARCHAR','VARCHAR','VARCHAR2'];

var tools = [/*{
    id:'gear',
    handler: function(){
        Ext.Msg.alert('Message', 'This function need to be modified.');
    }
},*/{
    id:'close',
    handler: function(e, target, panel){
        panel.ownerCt.remove(panel, true);
    }
}];

var indexPanel = new Ext.Panel({
	title : '主页',
	iconCls : 'tabpanel_home',
	closable : false,
	layout : 'table',
	layoutConfig : {
		columns : 3
	},
	defaults : {
		frame : true,
		width : 300,
		height : 200
	},
	items:[
	{
		title : '常用工具',
		collapsible : false,
		style : 'margin: 5px;',
		iconCls : 'tabs',
		layoutConfig : {
			padding : 10
		},
		region : 'center',
		layout : 'anchor',
	    items:[{
			xtype : 'button',
			text : '更新缓存',
			iconCls : 'set',
			handler : function() {
				Ext.Ajax.request({
					url : APP_ROOT+'/common/updateVersion.do',
					success : function(response, opts) {
						var obj = Ext.decode(response.responseText);
						var result = obj.result;
						if (result == 1) {
							Ext.example.msg('提示', '缓存更新完成');
						}
					},
					failure : function() {
						Ext.example.msg('提示', '缓存更新失败');
					}
				});
			}
		}]
	}],
	bbar:[{
		anchor : '100%',
		baseCls : 'x-plain',
		layout : 'hbox',
		layoutConfig : {
			padding : 10
		},
		height:20,
		defaults : {
			margins : '0 5 0 0',
			pressed : false
		},
		html:"<div id='chg_log' style='overflow:hidden;height:30'>" +
				"<div id='chg_log1'></div>" +
				"<div id='chg_log2'></div>" +
			 "</div>"
	}]
});

/** 定义中心区域, 本系统的核心区域, 所有打开的Tab都将在该区域展示 */
var centerRegion = new Ext.TabPanel({
    region:'center',
    deferredRender:false,
    activeTab:0,
    enableTabScroll:true,
//    listeners:{
//    	remove: function(tp, c){
//    		c.hide();   //if autoDestroy: false
//    	}
//    },
    autoDestroy: true,
    items:[indexPanel]
});
/** 这里是页面展示的开始 */
Ext.onReady(function(){
	/** 处理ie提交数据中文乱码问题 */
    Ext.lib.Ajax.defaultPostHeader += '; charset=utf-8';
    
    viewport = new Ext.Viewport({
        layout:'border',
        items:[
        	/** 北面板, 定义在 ext_north.js */
        	//northRegion,
        	/** 南面板, 定义在 ext_south.js */
        	//southRegion,
        	/** 西面板, 定义在 ext_west.js */
			westRegion,
			/** 中心面板 ********************************/
			centerRegion
        ]
    });
    global_grid_height = viewport.getHeight() - 30;
});

/**给 tabPanel 添加子项目tabItem,以 'centerPanel_'+title 属性作为此tabItem唯一id*/
function addTapPanel(url,title){
	var id = 'centerPanel_'+title;
	//如果tabPanel已经打开了某个item，再次点击菜单时则激活它
	if(centerRegion.get(id) != undefined && centerRegion.get(id) != null){
		centerRegion.activate(centerRegion.get(id));
		return;
	}
	var tabItem = centerRegion.add({
						//iconCls:'tab',
						//text:text,
						//nocache : true,
						title:' '+title,
						iconCls:'cmp',
						//autoScroll: true,
						id:id,
						closable:true,
						autoScroll:true,
						autoLoad:{url:url,scripts:true}
					});
	centerRegion.activate(tabItem);
}
/**给 tabPanel 添加子项目tabItem,以 'centerPanel_'+title 属性作为此tabItem唯一id*/
function addTapPanel(url,title,update){
	var id = 'centerPanel_'+title;
	//如果tabPanel已经打开了某个item，再次点击菜单时则激活它
	if(centerRegion.get(id) != undefined && centerRegion.get(id) != null){
		if(update){
			//强制刷新已经打开的tab
			centerRegion.get(id).getUpdater().refresh(url);
		}
		centerRegion.activate(centerRegion.get(id));//激活该打开的页面
		return;
	}
	var tabItem = centerRegion.add({
						iconCls:'tab',
						//text:text,
						//nocache : true,
						title:' '+title,
						iconCls:'cmp',
						autoScroll: true,
						id:id,
						closable:true,
						autoScroll:true,
						autoLoad:{url:url,scripts:true}
					});
	centerRegion.activate(tabItem);
}

function removeTapPanel(title){
	var id = 'centerPanel_'+title;
	if(centerRegion.getComponent(id) != undefined && centerRegion.getComponent(id) != null){
		centerRegion.getComponent(id).destroy();
	}
}

function getRadioValue(RadioName){
    var obj;    
    obj=document.getElementsByName(RadioName);
    if(obj!=null){
        var i;
        for(i=0;i<obj.length;i++){
            if(obj[i].checked){
                return obj[i].value;            
            }
        }
    }
    return null;
}

//去掉字符串首尾的空格
function trim(str){
	if(str == null || str.length == 0){return "";}
	if(typeof str == 'number'){return str;}
	str=str.replace(/(^\s*)|(\s*$)/g, ""); 
	return str;
}

function nvl(value){
	if(typeof value == "undefined" || value == null){
		return '';
	}
	return trim(value);
}

function getCmpValue(id){
	var value = Ext.getCmp(id).getValue();
	if(typeof value == "undefined" || value == null){
		return '';
	}
	return trim(value);
}

//根据指定的FormPanleId获得指定name组件的值
function getFormPanelFieldValue(formPanelId,name){
	var value = Ext.getCmp(formPanelId).getForm().findField(name).getValue();
	if(typeof value == "undefined" || value == null){
		return '';
	}else if(typeof value == "string"){
		return trim(value);
	}else{
		return Ext.getCmp(formPanelId).getForm().findField(name).getRawValue();
	}
}

//根据指定的FormPanleId设置指定name组件的值
function setFormPanelFieldValue(formPanelId,name,value){
	if(Ext.getCmp(formPanelId).getForm().findField(name) == null || value == null){
		return;
	}
	Ext.getCmp(formPanelId).getForm().findField(name).setValue(value);
}

function getUrlParam(param){
	var params = Ext.urlDecode(location.search.substring(1));
	return param?params[param]:params;
}

//窗口大小改变时触发事件
window.onresize = function(){
	global_grid_height = viewport.getHeight() - 77;
};

/**
 * 创建单个grid自拖动对象
 * */
function createSingleGridDropTarget(grid){
	return new Ext.dd.DropTarget(grid.getEl(), {
        ddGroup: grid.getView().dragZone.ddGroup,  // Data come from
        // copy:true,
        notifyDrop : function(dd, e, data){
        	var rows=grid.getSelectionModel().getSelections();
            var count = rows.length;
            var cindex=dd.getDragData(e).rowIndex;
            var array=[];
            for(var i=0;i<count;i++){
                var index = cindex+i;
                array.push(index);
            }
            grid.store.insert(cindex,data.selections);
            grid.getView().refresh(); 
            grid.getSelectionModel().selectRows(array);
        }
    });
}

function getMonday(){
	var date = new Date();
	date.setDate(date.getDate()+1-date.getDay());
	var month = date.getMonth()+1;
	var day   = date.getDate();
	return date.getFullYear() + "-" + (month>9?month:'0'+month)+"-"+(day>9?day:'0'+day);
}

function getNextMonday(){
	var date = new Date();
	date.setDate(date.getDate()+1-date.getDay()+7);
	var month = date.getMonth()+1;
	var day   = date.getDate();
	return date.getFullYear() + "-" + (month>9?month:'0'+month)+"-"+(day>9?day:'0'+day);
}
