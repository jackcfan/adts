Ext.apply(Ext.form.VTypes,{
	number:function(val,field){
		return /^\d+$/.test(val);
	},
	numberText:'该输入项为数字类型',
	
	column_type:function(val,field){
		if(typeof column_type_regexp1 == "undefined"){
			var str1 = "^(("+valid_col_type1.join(')|(')+"))(\\(\\d+((\\,\\d+)|(\\d*))\\))?$";
			var str2 = "^(("+valid_col_type2.join(')|(')+"))(\\(\\d+((\\,\\d+)|(\\d*))\\))$";
			column_type_regexp1 = new RegExp(str1,"i");
			column_type_regexp2 = new RegExp(str2,"i");
		}
		return column_type_regexp1.test(val)||column_type_regexp2.test(val);
//		return /^[a-zA-Z_]+\w*(\(\d+((\,\d+)|(\d*))\))?$/.test(val);
	},
	column_typeText:'该输入项为数据库字段类型',
	
	column:function(val,field){
		return /^[a-zA-Z_]+\w*$/.test(val);
	},
	columnText:'该输入项为数据库字段',
	
	table:function(val,field){
		return /^[a-zA-Z_]+\w*$/.test(val);
	},
	tableText:'该输入项为数据库表名字段',
	
	monday:function(val,field){
		Ext.example.msg(val);
		return true;
		return /^\d+$/.test(val);
	},
	mondayText:'输入周一日期',
	
	comma_date:function(val,field){
		var ary = trim(val).split(',');
		for(var i=0;i<ary.length;i++){
			if(!/^\d{4}\-\d{2}\-\d{2}$/.test(ary[i])){
				return false;
			}
		}
		return true;
	},
	comma_dateText:'该输入项为逗号分隔的YYYY-MM-DD日期字符串',
	
	date:function(val,field){
		return /^\d{4}\-\d{2}\-\d{2}$/.test(trim(val));
	},
	dateText:'该输入项为YYYY-MM-DD日期字符串'
});

Ext.apply(Ext.grid.ColumnModel.prototype,{
	/**
	 * 重写ColumnModel的getCellEditor方法，避免config找不到getCellEditor报错
	 * rowIndex 默认时没有作用，可以不传入该参数
	 * */
	getCellEditor:function (colIndex,rowIndex){
		var config = this.config[colIndex];
		if(config.getCellEditor){
			return this.config[colIndex].getCellEditor(rowIndex);
		}else{
			return null;
		}
	}
});

Ext.apply(Ext.grid.EditorGridPanel.prototype,{  
    //增加EditorGridPanel的通用校验方法  
	isValidated : function(records){
		var cm = this.cm || this.colModel;
		if(records && records.length == 0){
	    	return true;
	    }
		if(!records){
			records = [];
			this.store.each(function(record){
				records.push(record);
			});
		}
	    for(var i=0;i<records.length;i++){
	    	var record = records[i];
//	    	fields:record 中的键数组
	    	var fields = record.fields.keys;
	    	for(var j=0;j<fields.length;j++){
	    		var col_name = fields[j];
	    		var col_value= record.data[col_name];
//	    		根据名称找到在grid中所在的列
	    		var col_index= cm.findColumnIndex(col_name);
	    		if(col_index == -1){
	    			continue;
	    		}
//	    		当前验证属性的行
	    		var row_index = this.store.indexOfId(record.id);
	    		if(cm.getCellEditor(col_index) != null){
	    			cm.getCellEditor(col_index).field.reset();
	    		}else{
	    			continue;
	    		}
	    		var editor = cm.getCellEditor(col_index).field;
	    		if(!editor.validateValue(col_value)){
	    			this.startEditing(row_index,col_index);
	    			return false;
	    		}
	    	}
	    }
	    return true;
	}
});

Ext.ns('Ext.ux.form.TextField');
Ext.ux.form.TextField = Ext.extend(Ext.form.TextField, {
	tooltip : {},
	onRender : function(ct, position) {
		Ext.ux.form.TextField.superclass.onRender.call(this, ct, position);
		if (typeof this.tooltip.text != "undefined") {
			var dismissDelay = 5000;
			if(typeof this.tooltip.dismissDelay != "undefined"){
				dismissDelay = this.tooltip.dismissDelay;
			}
			var maxWidth = 200;
			if(typeof this.tooltip.maxWidth != "undefined"){
				maxWidth = this.tooltip.maxWidth;
			}
			var title = '信息提示';
			if(typeof this.tooltip.title != "undefined"){
				title = this.tooltip.title;
			}
			new Ext.ToolTip({
				target : this.id,
				trackMouse : false,
				draggable : true,
				//maxWidth : maxWidth,
				autoWidth:true,
				minWidth : 100,
				title : title,
				dismissDelay:dismissDelay,
				html : this.tooltip.text
			});
		}
	}
});
Ext.reg('uxtextfield', Ext.ux.form.TextField);