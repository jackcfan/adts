Ext.ns("Ext.grid.plugins");
Ext.grid.plugins.AutoResize = Ext.extend(Ext.util.Observable,{
	init:function(grid){
		grid.applyToMarkup = function(el){
			grid.render(el);
		}
		var containerId = Ext.get(grid.renderTo || grid.applyTo).id;
		Ext.EventManager.onWindowResize(function () {
		    if(Ext.get(containerId) && Ext.get(containerId).getHeight()){
			    Ext.get(containerId).setHeight(Ext.get(CENTER_PANEL_ID).getHeight()-40);
			    grid.setHeight(Ext.get(CENTER_PANEL_ID).getHeight() - 40);
		    }
	    });
	}
});