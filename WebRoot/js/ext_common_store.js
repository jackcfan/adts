adts_model_revision_store = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getRevisionList.do'}),
	reader : new Ext.data.JsonReader({}, [
	    {name : 'revision'}
	])
});

adts_model_env_store = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({url:APP_ROOT+'/model/getEnvList.do'}),
	reader : new Ext.data.JsonReader({}, [
	    {name : 'env_name'}
	])
});