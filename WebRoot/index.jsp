<%@page language="java" contentType="text/html; charset=utf-8"pageEncoding="utf-8"%>
<%@page import="adts.util.Util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ADTS 辅助开发平台</title>
<link href="js/ext4.2/examples/shared/example.css" rel="stylesheet" />
<link href="js/ext4.2/resources/css/icon.css" rel="stylesheet" />

<script type="text/javascript" src="js/ext4.2/examples/shared/include-ext.js"></script>
<!-- <script type="text/javascript" src="js/ext4.2/examples/shared/options-toolbar.js"></script> -->
<script type="text/javascript" src="js/ext4.2/locale/ext-lang-zh_CN.js"></script>
<script type="text/javascript" src="js/ext4.2/examples/shared/examples.js"></script>

<script type="text/javascript" src="js/ext4.plugins.js?<%=Util.getVersion()%>"></script>
<script type="text/javascript" src="index.js?<%=Util.getVersion()%>"></script>

<script type="text/javascript">
    Ext.require(['*']);
    APP_ROOT = '/adts';
    APP_USER = '<c:out value="${ADTS_USER.username }"/>';
    APP_USER_REALNAME = '<c:out value="${ADTS_USER.realname }"/>';
    CENTER_PANEL_ID = 'center-panel';
    THEME = getQueryParam('theme') || 'neptune';  //当前主题
    
    Ext.onReady(function() {
        Ext.QuickTips.init();
        // NOTE: This is an example showing simple state management. During development,
        // it is generally best to disable state management as dynamically-generated ids
        // can change across page loads, leading to unpredictable results.  The developer
        // should ensure that stable state ids are set for stateful components in real apps.
        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));
        
        var tree_store = Ext.create('Ext.data.TreeStore',{
        	proxy:{
        		type:'ajax',
        		url:APP_ROOT+'/auth/getJsonMenu.do',
        		//url:'menu.json',
        		reader:{
        			type:'json'
        		},
        		root:'Root',
        		expanded:true
        	}
        });

        var menu = Ext.create('Ext.tree.Panel',{
        	region: 'west',
            stateId: 'navigation-panel',
            id: 'west-panel', // see Ext.getCmp() below
            title: '系统菜单',
            split: true,
            width: 200,
            minWidth: 200,
            maxWidth: 200,
            collapsible: false,
            animCollapse: true,
            margins: '0 0 0 0',
        	width: 150,
        	height: 150,
        	rootVisible:false,
        	//frame:true,
        	useArrows: false,
        	store:tree_store,
        	listeners : {
                'itemclick' : function(view,record){
                    var id    = record.raw.id;
                    var title = record.raw.title || record.raw.text;
                    var url   = record.raw.url;
                    var iconCls = record.raw.tabIconCls||record.data.iconCls;
                    if(url == undefined || url.trim() == ''){
                    	if(record.data.leaf){
        	            	Ext.example.msg('','未配置菜单');
                    	}
                    	return;
                    }
                    addTabPanel({title:title,url:url,iconCls:iconCls,id:id});
                }  
            }
        });

        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            Ext.create('Ext.Panel',{
                region: 'north',
                //height: 32, // give north and south regions a height
                autoHeight:true,
                id:'north_panel',
                margins: '0 0 0 0',
                tbar:[
						/* {xtype:'label',text:'ADTS',iconCls:'Usersuit',handler:function(){}}, */
						{xtype:'box',autoEl:{tag:'img',src:APP_ROOT+'/images/logo.png',height:20}},
						{xtype:"tbfill"},
						//'-',
						{xtype:'button',text:APP_USER_REALNAME,iconCls:'Usersuit',handler:function(){}},
						{text: '主题',iconCls: 'Theme',menu: [
							{text: 'access',  handler:function(){switchTheme('access')}}
							,{text: 'classic',handler:function(){switchTheme('classic')}}
							,{text: 'gray',   handler:function(){switchTheme('gray')}}
							,{text: 'neptune',handler:function(){switchTheme('neptune')}}]
						},
						{xtype:'button',text:'注销',iconCls:'Controlpowerblue',handler:function(){
								Ext.MessageBox.confirm("注销", "确认退出系统?", function (btn) {
							        if(btn == 'yes'){
										window.location.href=APP_ROOT+"/auth/logout.do"
							        }
							    });
							}
						}
                     ]
            }),menu,
            // in this instance the TabPanel is not wrapped by another panel
            // since no title is needed, this Panel is added directly
            // as a Container
            Ext.create('Ext.tab.Panel', {
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 0,     // first tab initially active
                id:CENTER_PANEL_ID,
                items: [{
                    contentEl: 'center1',
                    title: '首页',
                    iconCls:'Applicationhome',
                    closable: false,
                    autoScroll: true
                }]
            })]
        });
        
        if(THEME == 'neptune'){
        	//如果是 neptune 主题，则改变toolbar北京颜色
	        Ext.DomHelper.applyStyles(Ext.get('north_panel').child ('.x-toolbar'),'background:#3892D3;');
        }
    });
    
    </script>
</head>
<body>
    <div id="center1" class="x-hide-display">
		<jsp:include page="update_list.jsp"></jsp:include>
    </div>
</body>
</html>
