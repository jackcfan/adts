#!/usr/bin/perl
use strict;
use ECIF;
use Logger;

my $dbh = getDbhByTag('ECIF_DPF');
my $cnt = 0;

#检查(除主索引外最多支持六组索引)
info("检查除主索引外的组数");
my $sql = "select tabschema,tabname,count(*) from syscat.indexes where uniquerule in ('D','U')
            and (tabschema) in (select schema_name from ECIF_SYSADMIN.ECIF_MODEL_SCHEMA)
            group by tabschema,tabname
            having count(*) > 6";
my $ary_ref = $dbh->selectall_arrayref($sql);
if(scalar @$ary_ref > 0){
    info("找到超过6组索引的表，跳过执行");
    $dbh->disconnect();
    exit;
}

info("清理表级信息");
my $sql = "truncate ECIF_SYSADMIN.ECIF_MODEL_TABLE immediate";

$dbh->do($sql) or fatal("failed\n$sql");

$sql = "truncate ECIF_SYSADMIN.ECIF_MODEL_COLUMN immediate";
info("清理字段级信息");
$dbh->do($sql) or fatal("failed\n$sql");

#初始化表级信息
$sql = "    insert into ECIF_SYSADMIN.ECIF_MODEL_TABLE(SCHEMA_NAME,TAB_NAME,TAB_NAME_CN,TAB_SPACE,IDX_SPACE,REMARK)
			select x.tabschema,x.tabname,remarks,coalesce(z1.tab_space_code,tbspace) tbspace,coalesce(z2.tab_space_code,index_tbspace) index_tbspace,''
			 from(
			    select trim(tabschema) tabschema,trim(tabname) tabname,remarks
			      from syscat.tables where type = 'T'
			       and trim(tabschema) in (select schema_name from ECIF_SYSADMIN.ECIF_MODEL_SCHEMA)
			)x left join (
			    select tabschema,tabname,b.tbspace ,c.tbspace index_tbspace
			      from (
			              select distinct tabschema,tabname,tbspaceid,index_tbspaceid from syscat.DATAPARTITIONS
			               where tabschema in (select schema_name from ECIF_SYSADMIN.ECIF_MODEL_SCHEMA)
			             ) a
			       left join syscat.TABLESPACES b
			         on a.tbspaceid = b.tbspaceid
			       left join syscat.TABLESPACES c
			         on a.index_tbspaceid = c.tbspaceid
			)y on x.tabschema = y.tabschema and x.tabname = y.tabname
			left join ECIF_SYSADMIN.ECIF_MODEL_TABSPACE z1
			on tbspace = z1.tab_space
			left join ECIF_SYSADMIN.ECIF_MODEL_TABSPACE z2
			on index_tbspace = z2.tab_space";

info("初始化表级信息");
$dbh->do($sql) or fatal("failed\n$sql");

$sql = "update ECIF_SYSADMIN.ECIF_MODEL_TABLE
		   set grant_sel = 'NMA_SEL'
		 where (schema_name,tab_name) in (
		    select distinct tabschema,tabname from syscat.TABAUTH 
		    where tabschema in (select schema_name from ECIF_SYSADMIN.ECIF_MODEL_SCHEMA)
		    and trim(grantor) = 'ECIFETL'
		    and selectauth <> 'N'
		    and tabschema <> 'ECIF_SDDL'
		    and grantee like 'M%'
		)";

info("更新管会查询用户权限组");
$dbh->do($sql) or fatal("failed\n$sql");

$sql = "delete from ECIF_SYSADMIN.ECIF_MODEL_TABLE
where schema_name = 'ECIF_LOG' 
and substr(tab_name,length(tab_name)-5) like '14%' and length(tab_name) > 5";

info("删除ECIF_LOG 无用表1");
$dbh->do($sql) or fatal("failed\n$sql");

$sql = "delete from ECIF_SYSADMIN.ECIF_MODEL_TABLE
where schema_name = 'ECIF_LOG' 
and substr(tab_name,length(tab_name)-5) like '%TDB' and length(tab_name) > 5";

info("删除ECIF_LOG 无用表2");
$dbh->do($sql) or fatal("failed\n$sql");

$sql = "delete from ECIF_SYSADMIN.ECIF_MODEL_TABLE where tab_name like '%BAK' or tab_name like '%TMP' or tab_name like '%TEST'";
info("删除 BAK 或 TMP 结尾的表");
$dbh->do($sql) or fatal("failed\n$sql");

$sql = "insert into ECIF_SYSADMIN.ECIF_MODEL_COLUMN(SCHEMA_NAME,TAB_NAME,COL_NAME,COL_NAME_CN,COL_TYPE,COL_LEN,COL_SCALE,col_no,NULLABLE,is_dk)
select a.* from(
        select tabschema,tabname,colname,remarks,typename,length
                ,case when typename = 'DECIMAL' then scale else 0 end scale,(colno+1) colno,nulls
                ,case when partkeyseq is null or partkeyseq = 0 then '' else cast(partkeyseq as varchar(10)) end partkeyseq
        from syscat.columns
        where (tabschema,tabname) in (select SCHEMA_NAME,TAB_NAME from ECIF_SYSADMIN.ECIF_MODEL_TABLE)
)a
order by a.tabname,colno asc";

info("初始化字段信息");
$dbh->do($sql) or fatal("failed\n$sql");

$sql = "update ECIF_SYSADMIN.ECIF_MODEL_COLUMN
            set IS_PTK = 'Y'
            where (SCHEMA_NAME,TAB_NAME,COL_NAME) in (
                    select tabschema,tabname,cast(datapartitionexpression as varchar(100)) ptk
                    from syscat.datapartitionexpression
                     where (tabschema,tabname) in (select SCHEMA_NAME,TAB_NAME from ECIF_SYSADMIN.ECIF_MODEL_TABLE)
            )";
info("初始化分区键");
$dbh->do($sql) or fatal("failed\n$sql");


$sql = "update ECIF_SYSADMIN.ECIF_MODEL_COLUMN 
set case_no_sens = 'Y'
where schema_name = 'ECIF_SDDL'
  and (tab_name,col_name) in (select table_name,col_name from ECIF_SYSADMIN.SYS_FILE_COL_CFG)";

info("初始化 ECIF_SDDL 大小写不敏感字段");
$dbh->do($sql) or fatal("failed\n$sql");

$sql = "update ECIF_SYSADMIN.ECIF_MODEL_COLUMN set is_pk = 'P'
        where (schema_name,tab_name,col_name) in (
        select tabschema,tabname,substr(colnames,2) colnames from syscat.indexes
        where (tabschema,tabname) in (select SCHEMA_NAME,TAB_NAME from ECIF_SYSADMIN.ECIF_MODEL_TABLE)
        and uniquerule = 'P'
        and substr(colnames,2) not like '%+%'
)";

info("更新唯一主键");
$dbh->do($sql) or fatal("failed\n$sql");

info("循环更新联合主键");
$sql = "select tabschema,tabname,substr(colnames,2) colnames from syscat.indexes
        where (tabschema,tabname) in (select SCHEMA_NAME,TAB_NAME from ECIF_SYSADMIN.ECIF_MODEL_TABLE)
        and uniquerule = 'P'
        and substr(colnames,2) like '%+%'";
$ary_ref = $dbh->selectall_arrayref($sql);

for my $ary (@$ary_ref){
    my $schema   = $ary->[0];
    my $table    = $ary->[1];
    my $colnames = $ary->[2];
    my @cols     = split(/\+/,$colnames);
    $cnt = 0;
    for my $col (@cols){
    	$cnt++;
        $sql = "update ECIF_SYSADMIN.ECIF_MODEL_COLUMN
                   set is_pk = 'P$cnt'
                 where schema_name = '$schema' and tab_name='$table' and col_name='$col'";
        $dbh->do($sql) or fatal("failed\n$sql");
    }
}

info("更新单索引");
for(my $i = 1;$i <= 6;$i++){
    $sql = "update ECIF_SYSADMIN.ECIF_MODEL_COLUMN a
                set IDX_$i = (
                        select uniquerule from (
                                select tabschema,tabname,substr(colnames,2) colnames,uniquerule
                                        ,row_number() over(partition by tabschema,tabname order by create_time asc) index_no
                                from syscat.indexes
                               where (tabschema,tabname) in (select SCHEMA_NAME,TAB_NAME from ECIF_SYSADMIN.ECIF_MODEL_TABLE)
                                 and indschema = tabschema
                        )b 
                        where colnames not like '%+%' and index_no = $i and uniquerule in ('U','D')
                        and a.SCHEMA_NAME = b.tabschema and a.TAB_NAME = b.tabname and a.COL_NAME = b.colnames
                )
             where 1 = 1";
    info("更新第 $i 组单索引");
    $dbh->do($sql) or fatal("failed\n$sql");
}

info("更新联合索引");
for(my $i = 1;$i <= 6;$i++){
    $sql = "select tabschema,tabname,''''||replace(colnames,'+',''',''')||'''',uniquerule from (
                    select tabschema,tabname,substr(colnames,2) colnames,uniquerule
                            ,row_number() over(partition by tabschema,tabname order by create_time asc) index_no
                    from syscat.indexes 
                   where (tabschema,tabname) in (select SCHEMA_NAME,TAB_NAME from ECIF_SYSADMIN.ECIF_MODEL_TABLE)
                     and indschema = tabschema
            )b 
            where colnames like '%+%' and index_no = $i and uniquerule in ('U','D')";
    
    $sql = "select tabschema,tabname,colnames,uniquerule from (
                    select tabschema,tabname,substr(colnames,2) colnames,uniquerule
                            ,row_number() over(partition by tabschema,tabname order by create_time asc) index_no
                    from syscat.indexes
                   where (tabschema,tabname) in (select SCHEMA_NAME,TAB_NAME from ECIF_SYSADMIN.ECIF_MODEL_TABLE)
                     and indschema = tabschema
            )b 
            where colnames like '%+%' and index_no = $i and uniquerule in ('U','D')";
    
    
    info("更新第 $i 组联合索引");
    $ary_ref = $dbh->selectall_arrayref($sql);
    for my $ary (@$ary_ref){
        my $schema = $ary->[0];
        my $tab    = $ary->[1];
        my $colnames = $ary->[2];
        my $role   = $ary->[3];
        
        my @cols   = split(/\+/,$colnames);
	    $cnt = 0;
	    for my $col (@cols){
	    	$cnt++;
	    	$sql = "update ECIF_SYSADMIN.ECIF_MODEL_COLUMN
	                   set idx_$i = '$role$cnt' 
	                 where SCHEMA_NAME = '$schema' and TAB_NAME = '$tab' and COL_NAME = '$col'";
	        $dbh->do($sql) or fatal("failed\n$sql");
	    }
        
        
    }
}

$sql = "update ECIF_SYSADMIN.ECIF_MODEL_TABLE
set SCHEMA_NAME = trim(replace(replace(replace(SCHEMA_NAME,chr(9),''),chr(13),' '),chr(10),'')),
    TAB_NAME = trim(replace(replace(replace(TAB_NAME,chr(9),''),chr(13),' '),chr(10),'')),
    TAB_NAME_CN = trim(replace(replace(replace(TAB_NAME_CN,chr(9),''),chr(13),' '),chr(10),'')),
    TAB_SPACE = trim(replace(replace(replace(TAB_SPACE,chr(9),''),chr(13),' '),chr(10),'')),
    IDX_SPACE = trim(replace(replace(replace(IDX_SPACE,chr(9),''),chr(13),' '),chr(10),'')),
    REMARK = trim(replace(replace(replace(REMARK,chr(9),''),chr(13),' '),chr(10),''))
where 1 = 1";

$dbh->do($sql) or fatal("failed\n$sql");

$sql = "update ECIF_SYSADMIN.ECIF_MODEL_COLUMN
set SCHEMA_NAME = trim(replace(replace(replace(SCHEMA_NAME,chr(9),''),chr(13),' '),chr(10),'')),
TAB_NAME = trim(replace(replace(replace(TAB_NAME,chr(9),''),chr(13),' '),chr(10),'')),
COL_NAME = trim(replace(replace(replace(COL_NAME,chr(9),''),chr(13),' '),chr(10),'')),
COL_NAME_CN = trim(replace(replace(replace(COL_NAME_CN,chr(9),''),chr(13),' '),chr(10),'')),
COL_TYPE = trim(replace(replace(replace(COL_TYPE,chr(9),''),chr(13),' '),chr(10),'')),
NULLABLE = trim(replace(replace(replace(NULLABLE,chr(9),''),chr(13),' '),chr(10),'')),
CASE_NO_SENS = trim(replace(replace(replace(CASE_NO_SENS,chr(9),''),chr(13),' '),chr(10),'')),
REMARK = trim(replace(replace(replace(REMARK,chr(9),''),chr(13),' '),chr(10),''))
where 1 =1 ";

$dbh->do($sql) or fatal("failed\n$sql");

#MDC ORGANIZE BY ROW USING 列表
my @org_by = qw{
ECIF_CDATA.C03_AGT_VOUCH_RELA|AGT_MODIF_NUM
ECIF_CDATA.C03_CHREM_AGT_INFO|AGT_MODIF_NUM
ECIF_CDATA.C03_CRD_ACT_REAL|MAIN_CRD_IND,MAIN_ACT_FLG
ECIF_CDATA.C03_GBOND_AGT_INFO|AGT_MODIF_NUM
ECIF_CDATA.C03_LOAN_AGT_INFO|AGT_MODIF_NUM
ECIF_NMA_CDATA.C03_AGT_VOUCH_RELA_HIS|AGT_MODIF_NUM
ECIF_NMA_CDATA.C03_CHREM_AGT_INFO_HIS|AGT_MODIF_NUM
ECIF_NMA_CDATA.C03_GBOND_AGT_INFO_HIS|AGT_MODIF_NUM
ECIF_NMA_CDATA.C03_LOAN_AGT_INFO_HIS|AGT_MODIF_NUM
ECIF_NMA_CDATA.C03_AGT_VOUCH_RELA|AGT_MODIF_NUM
ECIF_NMA_CDATA.C03_CHREM_AGT_INFO|AGT_MODIF_NUM
ECIF_NMA_CDATA.C03_GBOND_AGT_INFO|AGT_MODIF_NUM
ECIF_NMA_CDATA.C03_LOAN_AGT_INFO|AGT_MODIF_NUM
ECIF_PDATA.T01_PARTY_IDTFY_H|IDTFY_TYPE_CD
ECIF_PDATA.T01_PARTY_IMP_LINKMAN_H|LINKMAN_TYPE_CD	
};

info("更新 MDC ORGANIZE BY");
for my $org (@org_by){
    my ($table,$org_col) = split(/\|/,trim($org));
    $sql = "update ECIF_SYSADMIN.ECIF_MODEL_TABLE set org_by = '$org_col' 
            where SCHEMA_NAME||'.'||TAB_NAME = '$table'";
    $dbh->do($sql) or fatal("failed\n$sql");
}

info("处理完毕");



sub trim{
    my $str = $_[0];
    unless(defined $str){return undef}
    $str =~ s/^\s+//;
    $str =~ s/\s+$//;
    return $str;
}
