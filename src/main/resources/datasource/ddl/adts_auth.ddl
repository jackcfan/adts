create schema "adts_auth";

CREATE TABLE
    adts_auth.adts_app
    (
        id INTEGER NOT NULL,
        app_name CHARACTER VARYING(64) NOT NULL,
        app_desc CHARACTER VARYING(128),
        PRIMARY KEY (id),
        UNIQUE (app_name)
    );

CREATE TABLE
    adts_auth.adts_group
    (
        id INTEGER NOT NULL,
        group_name CHARACTER VARYING(64) NOT NULL,
        group_desc CHARACTER VARYING(128),
        app_id INTEGER,
        PRIMARY KEY (id),
        UNIQUE (group_name, app_id)
    );
    
CREATE TABLE
    adts_auth.adts_group_resource
    (
        id INTEGER NOT NULL,
        group_id INTEGER NOT NULL,
        resource_id INTEGER NOT NULL,
        resource_type CHARACTER VARYING(32),
        PRIMARY KEY (id),
        UNIQUE (group_id, resource_id, resource_type)
    );
    
CREATE TABLE
    adts_auth.adts_resource_menu
    (
        id INTEGER NOT NULL,
        parent_id INTEGER,
        text CHARACTER VARYING(128) NOT NULL,
        url CHARACTER VARYING(1024),
        title CHARACTER VARYING(128),
        iconcls CHARACTER VARYING(64),
        order_no INTEGER,
        app_id INTEGER,
        expanded CHARACTER VARYING(1),
        PRIMARY KEY (id),
        UNIQUE (id, parent_id, app_id)
    );
    
CREATE TABLE
    adts_auth.adts_user
    (
        id INTEGER NOT NULL,
        username CHARACTER VARYING(32) NOT NULL,
        password CHARACTER VARYING(64) NOT NULL,
        realname CHARACTER VARYING(128) NOT NULL,
        state INTEGER NOT NULL,
        tel_no CHARACTER VARYING(32),
        email CHARACTER VARYING(256),
        create_time TIMESTAMP(6) WITH TIME ZONE,
        token CHARACTER VARYING(64),
        PRIMARY KEY (id),
        UNIQUE (username)
    );
    
CREATE TABLE
    adts_auth.adts_user_group
    (
        id INTEGER NOT NULL,
        user_id INTEGER NOT NULL,
        group_id INTEGER NOT NULL
    );
    
CREATE TABLE
    adts_auth.adts_user_resource
    (
        id INTEGER NOT NULL,
        user_id INTEGER NOT NULL,
        resource_id INTEGER NOT NULL,
        resource_type CHARACTER VARYING(32),
        PRIMARY KEY (id),
        UNIQUE (user_id, resource_id, resource_type)
    );
    
CREATE TABLE
    adts_auth.adts_user_session
    (
        user_id INTEGER NOT NULL,
        token CHARACTER VARYING(64),
        PRIMARY KEY (user_id)
    );
    
create sequence "adts_auth"."adts_auth_seq" start with 5000 increment by 1 no maxvalue no minvalue cache 20 no cycle;

insert into adts_auth.adts_app (id, app_name, app_desc) values (1, 'ADTS_HOME_MENU', 'ADTS 主页菜单');

insert into adts_auth.adts_group (id, group_name, group_desc, app_id) values (1, 'default', '默认用户组', 1);
insert into adts_auth.adts_group (id, group_name, group_desc, app_id) values (0, 'auth', '权限分配组', 1);

insert into adts_auth.adts_group_resource (id, group_id, resource_id, resource_type) values (1, 0, 0, 'menu');
insert into adts_auth.adts_group_resource (id, group_id, resource_id, resource_type) values (2, 0, 1, 'menu');
insert into adts_auth.adts_group_resource (id, group_id, resource_id, resource_type) values (3, 0, 2, 'menu');
insert into adts_auth.adts_group_resource (id, group_id, resource_id, resource_type) values (4, 0, 6, 'menu');
insert into adts_auth.adts_group_resource (id, group_id, resource_id, resource_type) values (5, 0, 3, 'menu');
insert into adts_auth.adts_group_resource (id, group_id, resource_id, resource_type) values (6, 0, 5, 'menu');
insert into adts_auth.adts_group_resource (id, group_id, resource_id, resource_type) values (7, 0, 4, 'menu');

insert into adts_auth.adts_resource_menu (id, parent_id, text, url, title, iconcls, order_no, app_id, expanded) values (1, 0, '后台管理', '', '后台管理', '', 2, 1, 'Y');
insert into adts_auth.adts_resource_menu (id, parent_id, text, url, title, iconcls, order_no, app_id, expanded) values (2, 1, '权限管理', '', '权限管理', '', 1, 1, 'Y');
insert into adts_auth.adts_resource_menu (id, parent_id, text, url, title, iconcls, order_no, app_id, expanded) values (5, 2, '用户组', '/adts/auth/group.js', '用户组', 'Groupgear', null, 1, 'Y');
insert into adts_auth.adts_resource_menu (id, parent_id, text, url, title, iconcls, order_no, app_id, expanded) values (6, 2, '用户管理', '/adts/auth/user.js', '用户管理', 'Userhome', 2, 1, 'Y');
insert into adts_auth.adts_resource_menu (id, parent_id, text, url, title, iconcls, order_no, app_id, expanded) values (0, -1, '菜单根节点', '', '菜单根节点', 'Textrotate0', 1, 1, 'Y');
insert into adts_auth.adts_resource_menu (id, parent_id, text, url, title, iconcls, order_no, app_id, expanded) values (3, 2, '菜单管理', '/adts/auth/menu.js', '菜单管理', 'menu', null, 1, 'N');

insert into adts_auth.adts_user (id, username, password, realname, state, tel_no, email, create_time, token) values (0, 'admin', '21232F297A57A5A743894A0E4A801FC3', '管理员', 0, '', '', current_timestamp, '');

insert into adts_auth.adts_user_group (id, user_id, group_id) values (1, 0, 0);

