CREATE TABLE
    adts_model_col
    (
        revision BIGINT NOT NULL,
        schema_name CHARACTER VARYING(32) NOT NULL,
        tab_name CHARACTER VARYING(128) NOT NULL,
        col_name CHARACTER VARYING(128) NOT NULL,
        col_name_cn CHARACTER VARYING(256),
        col_type CHARACTER VARYING(32) NOT NULL,
        col_len INTEGER,
        col_scale INTEGER,
        col_no INTEGER,
        nullable CHARACTER VARYING(1),
        case_no_sens CHARACTER VARYING(1),
        is_pk CHARACTER VARYING(8),
        is_dk CHARACTER VARYING(8),
        is_ptk CHARACTER VARYING(8),
        remark CHARACTER VARYING(258),
        idx_1 CHARACTER VARYING(8),
        idx_2 CHARACTER VARYING(8),
        idx_3 CHARACTER VARYING(8),
        idx_4 CHARACTER VARYING(8),
        idx_5 CHARACTER VARYING(8),
        idx_6 CHARACTER VARYING(8),
        PRIMARY KEY (revision, schema_name, tab_name, col_name),
        UNIQUE (revision, schema_name, tab_name, col_no)
    );
    
CREATE TABLE
    adts_model_his
    (
        revision BIGINT NOT NULL,
        svn_author CHARACTER VARYING(128) NOT NULL,
        commit_date TIMESTAMP(6) WITH TIME ZONE,
        import_date TIMESTAMP(6) WITH TIME ZONE,
        import_stat CHARACTER VARYING(8),
        MESSAGE CHARACTER VARYING,
        import_err TEXT,
        PRIMARY KEY (revision)
    );
    
CREATE TABLE
    adts_model_keywords
    (
        keywords CHARACTER VARYING(64) NOT NULL,
        PRIMARY KEY (keywords)
    );
    
CREATE TABLE
    adts_model_tab
    (
        revision BIGINT NOT NULL,
        schema_name CHARACTER VARYING(32) NOT NULL,
        tab_name CHARACTER VARYING(128) NOT NULL,
        tab_name_cn CHARACTER VARYING(256),
        tab_space_code CHARACTER VARYING(128),
        idx_space_code CHARACTER VARYING(128),
        remark CHARACTER VARYING(256),
        org_by CHARACTER VARYING(1024),
        grant_sel CHARACTER VARYING(1024),
        grant_ins CHARACTER VARYING(1024),
        grant_del CHARACTER VARYING(1024),
        grant_upd CHARACTER VARYING(1024),
        grant_alt CHARACTER VARYING(1024),
        grant_all CHARACTER VARYING(1024),
        grant_rud CHARACTER VARYING(1024),
        PRIMARY KEY (revision, schema_name, tab_name)
    );
    
CREATE TABLE
    adts_model_tabspace
    (
        revision BIGINT NOT NULL,
        env_name CHARACTER VARYING(32) NOT NULL,
        tab_space CHARACTER VARYING(32) NOT NULL,
        tab_space_code CHARACTER VARYING(32) NOT NULL,
        UNIQUE (revision, env_name, tab_space_code)
    );
    
CREATE TABLE
    adts_model_user_group
    (
        revision BIGINT NOT NULL,
        env_name CHARACTER VARYING(32) NOT NULL,
        usr_name CHARACTER VARYING(32) NOT NULL,
        grp_name CHARACTER VARYING(32) NOT NULL,
        remark CHARACTER VARYING(256),
        PRIMARY KEY (revision, env_name, usr_name, grp_name)
    );    