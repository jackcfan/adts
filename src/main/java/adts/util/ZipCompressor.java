package adts.util;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Zip;
import org.apache.tools.ant.types.FileSet;

public class ZipCompressor {

	public static int zipDir(File dir,String des_file){
		if(!dir.exists() || !dir.isDirectory()){
			return 0;
		}
		Project project = new Project();
		File zip_file = new File(des_file);
		if(zip_file.exists()){zip_file.delete();}
		Zip zip = new Zip();
		zip.setProject(project);
		zip.setDestFile(zip_file);
		FileSet fileSet = new FileSet();
		fileSet.setProject(project);
		fileSet.setDir(dir);
//		fileSet.setIncludes("*.pl");
//		fileSet.setExcludes(".svn");
		zip.addFileset(fileSet);
		zip.execute();
		return 1;
	}
	
	public static int zipDir(String dir,String zip_file){
		return zipDir(new File(dir),zip_file);
	}
	
	public static void main(String[] args) {
		String dir = "C:\\Users\\ecif-yanjingying\\AppData\\Local\\Temp\\adts\\0FF9B0F55BD94B318684DF828E4E081B";
		String des = "C:\\Users\\ecif-yanjingying\\AppData\\Local\\Temp\\adts\\0FF9B0F55BD94B318684DF828E4E081B\\v4.9.zip";
		System.out.println(zipDir(dir,des));
	}
}
