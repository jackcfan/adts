package adts.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.ISVNDiffStatusHandler;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCommitClient;
import org.tmatesoft.svn.core.wc.SVNDiffClient;
import org.tmatesoft.svn.core.wc.SVNDiffStatus;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.wc2.ISvnObjectReceiver;
import org.tmatesoft.svn.core.wc2.SvnCommit;
import org.tmatesoft.svn.core.wc2.SvnList;
import org.tmatesoft.svn.core.wc2.SvnOperationFactory;
import org.tmatesoft.svn.core.wc2.SvnTarget;

public class SVNKitUtil {
	private SVNClientManager client_manager;
	private SVNUpdateClient update_client;
	private SVNCommitClient commit_client;
	private SvnOperationFactory factory;
	private SVNRepository repository;
	private String base_url;
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static ISVNOptions default_options = SVNWCUtil.createDefaultOptions(true);

	public SVNKitUtil(String svn_tag) throws SVNException{
		this(IniUtil.getValByKey(svn_tag, "base_url")
				,IniUtil.getValByKey(svn_tag, "user")
				,IniUtil.getValByKey(svn_tag, "pwd"));
	}
	
	public SVNKitUtil(String url,String user,String pwd) throws SVNException{
		base_url  = url;
		
		client_manager = SVNClientManager.newInstance((DefaultSVNOptions) default_options, user, pwd);
		update_client = client_manager.getUpdateClient();
		update_client.setIgnoreExternals(false);
		
		commit_client = client_manager.getCommitClient();
		
		ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(user, pwd);
		
		factory = new SvnOperationFactory();
		factory.setAuthenticationManager(authManager);
		
		repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(base_url));
		repository.setAuthenticationManager(authManager);
	}
	
	@SuppressWarnings("unused")
	private SVNKitUtil(){}
	
	/**
	 * svn diff --summarize url1 url2
	 * kind:file,dir
	 * */
	public List<Map<String,String>> doSummarizeDiff(String url1,String url2,final String kind) throws SVNException{
		SVNDiffClient client = client_manager.getDiffClient();
		final List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		client.doDiffStatus(SVNURL.parseURIEncoded(base_url+url1)
				, SVNRevision.HEAD
				, SVNURL.parseURIEncoded(base_url+url2)
				, SVNRevision.HEAD, SVNDepth.INFINITY, false, new ISVNDiffStatusHandler() {
			
			@Override
			public void handleDiffStatus(SVNDiffStatus paramSVNDiffStatus)
					throws SVNException {
				if("none".equalsIgnoreCase(paramSVNDiffStatus.getModificationType().toString())){return;}
				Map<String,String> map = new HashMap<String, String>();
				String type = paramSVNDiffStatus.getModificationType().toString();
				if(kind != null && !kind.equalsIgnoreCase(paramSVNDiffStatus.getKind().toString())){
					return;
				}
				//diff 类型:M 修改 D 删除 A 新增
				if("modified".equalsIgnoreCase(type)){
					map.put("type","M");
				}else if("deleted".equalsIgnoreCase(type)){
					map.put("type","D");
				}else if("added".equalsIgnoreCase(type)){
					map.put("type","A");
				}else{
					map.put("type",type);
				}
				//diff 对象类型:文件 目录
				map.put("kind",paramSVNDiffStatus.getKind().toString());
				//diff 对象
				map.put("path",paramSVNDiffStatus.getPath());
				//map.put("url", paramSVNDiffStatus.getURL().toString());
				list.add(map);
			}
		});
		return list;
	}
	/**
	 * 导出指定修订号文件
	 * */
	public long export(String url, File destPath,long revision) throws SVNException {
		update_client.setIgnoreExternals(false);
		long version = update_client.doExport(SVNURL.parseURIEncoded(base_url+url)
				, destPath, SVNRevision.HEAD, SVNRevision.create(revision), null, true, SVNDepth.INFINITY);
		return version;
	}
	
	/**
	 * 导出最新文件
	 * */
	public long export(String url, File dstPath) throws SVNException {
		update_client.setIgnoreExternals(false);
		long version = update_client.doExport(SVNURL.parseURIEncoded(base_url+url)
				, dstPath, SVNRevision.HEAD, SVNRevision.HEAD, null, true, SVNDepth.INFINITY);
		return version;
	}
	
	public long export(String url, File dstPath,
			SVNRevision pegRevision, SVNRevision revision, String eolStyle,
			boolean overwrite, SVNDepth depth) throws SVNException {
		update_client.setIgnoreExternals(false);
		long version = update_client.doExport(SVNURL.parseURIEncoded(base_url+url), dstPath, pegRevision, revision, eolStyle, overwrite, depth);
		return version;
	}
	
	/*
	 * 获取指定 url 下的文件列表
	 * */
	public List<Map<String,Object>> getFileList(String url) throws SVNException{
		SvnList list = factory.createList();
		list.setSingleTarget(SvnTarget.fromURL(SVNURL.parseURIEncoded(base_url+url), SVNRevision.HEAD));
		final List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
		list.setReceiver(new ISvnObjectReceiver<SVNDirEntry>() {
			@Override
			public void receive(SvnTarget target, SVNDirEntry entry) throws SVNException {
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("name", entry.getName());
				map.put("kind", entry.getKind());
				map.put("size", entry.getSize());
				map.put("lastauthor", entry.getAuthor());
				map.put("lastchangeddate", entry.getDate());
				map.put("fullpath", target.getPathOrUrlString());
				result.add(map);
			}
		});
		list.run();
		return result;
	}
	
	/**
	 * 获得文件所有的提交历史
	 * */
	public List<Map<String,String>> getSvnLog(String file) throws SVNException{
		 return getSvnLog(file,0,-1);
	}
	
	/**
	 * 获得文件指定版本号间的提交历史
	 * */
	public List<Map<String,String>> getSvnLog(String file,long startRevision, long endRevision) throws SVNException{
		 Collection logEntries = repository.log(new String[] {file}, null,startRevision, endRevision, true, true);
		 List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		 for (Iterator entries = logEntries.iterator(); entries.hasNext();) {
			 SVNLogEntry logEntry = (SVNLogEntry) entries.next();
			 Map<String,String> map = new HashMap<String, String>();
			 map.put("revision", String.valueOf(logEntry.getRevision()));
			 map.put("author", logEntry.getAuthor());
			 map.put("date", sdf.format(logEntry.getDate()));
			 map.put("message", logEntry.getMessage());
			 list.add(0, map);
		 }
		 return list;
	}
	
	public void commit() throws SVNException{
		commit_client.doImport(new File("D://README.txt"), SVNURL.parseURIEncoded("https://197.1.4.29:8443/svn/ECIF_CODE_UPGRADE/trunk/ETL/test/README.txt"), "", true);
		SvnCommit commit = factory.createCommit();
		commit.setSingleTarget(SvnTarget.fromFile(new File("D://README.txt")));
		
		commit.setCommitMessage("for test");
		commit.run();
	}
}
