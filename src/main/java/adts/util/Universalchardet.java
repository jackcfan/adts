package adts.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.mozilla.universalchardet.UniversalDetector;

/**
 * 文件编码自动探测器
 * 探测不到时，默认返回 defaultEncondig 编码
 * 对于小文件探测的正确率较低
 */
public class Universalchardet {

	private static String defaultEncondig = "UTF8";
	
	public static String getFileEncoding(File file) throws IOException{
		UniversalDetector detector = new UniversalDetector(null);
		byte[] buf = new byte[4096];
		FileInputStream fis = new FileInputStream(file);
		int nread;
		
		while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
			detector.handleData(buf, 0, nread);
		}
		detector.dataEnd();
		String encoding = detector.getDetectedCharset();
		detector.reset();
		fis.close();
		return encoding==null?defaultEncondig:encoding;
	}
	
	public static String getFileEncoding(String file) throws IOException{
		return getFileEncoding(new File(file));
	}
}
