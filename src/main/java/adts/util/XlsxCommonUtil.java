package adts.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XlsxCommonUtil extends XlsxAbstractUtil {
	private List<Map<String,String>> rows = null;
	
	public XlsxCommonUtil(String xlsx_file) throws Exception {
		super(xlsx_file);
	}
	
	public XlsxCommonUtil(File file) throws Exception {
		super(file.getAbsolutePath());
	}

	@Override
	protected void processRows(int cur_row, Map<String, String> row_map,
			String xls_mapping_worksheet_name, String worksheet_name) {
		Map<String, String> map_clone = new HashMap<String,String>();
		map_clone.putAll(row_map);
		map_clone.put("__ROW_NO__", String.valueOf(cur_row));
		rows.add(map_clone);
	}
	
	public List<Map<String,String>> readSheet(int index,String xls_mapping_worksheet_name) throws Exception{
		rows = new ArrayList<Map<String,String>>();
		processSheet(index, xls_mapping_worksheet_name);
		return rows;
	}
	
	public List<Map<String,String>> readSheet(String sheet_name,String xls_mapping_worksheet_name) throws Exception{
		rows = new ArrayList<Map<String,String>>();
		processSheet(sheet_name, xls_mapping_worksheet_name);
		return rows;
	}
	
	public static void main(String[] args) throws Exception {
		XlsxCommonUtil xlsx = new XlsxCommonUtil("C:\\Users\\ecif-yanjingying\\AppData\\Local\\Temp\\adts\\test\\prd_update_list.xlsx");
		List<Map<String,String>> rows = xlsx.readSheet("脚本部署", "ETL_Script_Update");
		for(Map<String,String> map : rows){
			String ver = map.get("version");
			if(!ver.equalsIgnoreCase("v4.9m")){
				continue;
			}
			System.out.println(map);
		}
		xlsx.closeXlsx();
		File file = new File("C:\\Users\\ecif-yanjingying\\AppData\\Local\\Temp\\adts\\test\\prd_update_list.xlsx");
		file.delete();
	}
}
