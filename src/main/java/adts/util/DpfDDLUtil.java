package adts.util;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adts.dao.BaseDao;

public class DpfDDLUtil implements DDLUtilInterface{
	private BaseDao dao;
	private int revision;
	private String env_name;
	private String tag = "DPF";
	private String mapper_namespace = "adts_model";
	private List<String> ptk_info_list;
	
	public DpfDDLUtil(BaseDao dao,int revision,String env_name,String tag){
		super();
		this.dao = dao;
		this.revision = revision;
		this.env_name = env_name;
		this.tag = tag;
		ptk_info_list = genPtInfo();
	}
	
	@Override
	public String genDDL(String[] tables){
		StringBuffer sb = new StringBuffer();
		StringBuffer header = new StringBuffer();
		Map<String,String> table_exists = new HashMap<String,String>();
		for(String full_table : tables){
			full_table = full_table.trim().toUpperCase();
			if(full_table.length() == 0){
				continue;
			}
			if(full_table.indexOf(".") == -1){
				header.append("--未找到:"+full_table+"\n");
				continue;
			}
			String schema_name = full_table.substring(0,full_table.indexOf("."));
			String tab_name    = full_table.substring(full_table.indexOf(".")+1);
			if(table_exists.containsKey(full_table)){
				header.append("--重复表:"+full_table+"\n");
				continue;
			}else{
				table_exists.put(full_table,"");
			}
			
			Map<String,String> map = genDDL(schema_name, tab_name);
			if(map.get("ddl") == null){
				header.append("--未找到:"+full_table+"\n");
				continue;
			}else{
				header.append("--drop table "+full_table+";\n");
			}
			sb.append(map.get("ddl")+"\n");
			if(map.get("index") != null){
				sb.append(map.get("index"));
				sb.append("\n");
			}
			if(map.get("grant") != null && map.get("grant").trim().length() > 0){
				sb.append("--授权\n");
				sb.append(map.get("grant"));
				sb.append("\n");
			}
			if(map.get("col_sens") != null && map.get("col_sens").trim().length() > 0){
				sb.append("--字段大小写不敏感\n");
				sb.append(map.get("col_sens"));
				sb.append("\n");
			}
			if(map.get("mask") != null && map.get("mask").trim().length() > 0){
				sb.append("--敏感字段屏蔽\n");
				sb.append(map.get("mask"));
				sb.append("\n");
			}
			sb.append("\n");
		}
		sb.insert(0, header.toString()+"\n");
		sb.insert(0,"--生成时间 " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"\n\n");
		if("ECIF".equalsIgnoreCase(IniUtil.getValByKey("ADTS", "app"))){
			sb.insert(0,"--对于ECIF系统的ECIF_PDATA下的表,CHAR和VARCHAR类型默认不允许为空\n");
		}
		sb.insert(0,"--本脚本由 ADTS 系统自动生成,修订号 " + revision +" 对应环境 " + env_name + "\n");
		return sb.toString();
	}

	/**
	 * result 	返回结果
	 * 		grant	授权
	 * 		col_sens大小写不敏感
	 * 		index	索引
	 * 		ddl		基础建表语句
	 *      mask    Db2 列控制(Y 启用 D 删除)
	 * */
	@Override
	public Map<String,String> genDDL(String schema_name,String tab_name){
		schema_name = schema_name.toUpperCase();
		tab_name    = tab_name.toUpperCase();
		
		Map<String,Object> paraMap = new HashMap<String,Object>();
		Map<String,String> result = new HashMap<String,String>();
		
		paraMap.put("schema_name", schema_name);
		paraMap.put("tab_name", tab_name);
		paraMap.put("revision", revision);
		paraMap.put("env_name", env_name);
		paraMap.put("tag", tag);
		paraMap.put("_MAPPER_NAMESPACE_", mapper_namespace);
		List<Map<String, Object>> list;
		try {
			list = dao.select("getModelDDL", paraMap);
			String mask_role = getMaskRole(paraMap);
			if(list.size() == 0){return result;}
			StringBuffer comment = new StringBuffer();
			StringBuffer ddl = new StringBuffer();
			StringBuffer grant = new StringBuffer();
			StringBuffer col_case = new StringBuffer();
			StringBuffer index = new StringBuffer();
			StringBuffer mask  = new StringBuffer();
			
			String tab_cn = coalesce(list.get(0).get("tab_name_cn"), "");
			String tab_space = coalesce(list.get(0).get("tab_space"), "");
			String idx_space = coalesce(list.get(0).get("idx_space"), "");
			
			String org_by    = coalesce(list.get(0).get("org_by"), "");
			String grant_sel = coalesce(list.get(0).get("grant_sel"), "");
			String grant_ins = coalesce(list.get(0).get("grant_ins"), "");
			String grant_del = coalesce(list.get(0).get("grant_del"), "");
			String grant_upd = coalesce(list.get(0).get("grant_upd"), "");
			String grant_alt = coalesce(list.get(0).get("grant_alt"), "");
			String grant_rud = coalesce(list.get(0).get("grant_rud"), "");
			
			if(!"".equals(tab_cn)){
				comment.append("COMMENT ON TABLE  "+schema_name+"."+tab_name+" IS '"+tab_cn+"';\n");
			}
			ddl.append("CREATE TABLE "+schema_name+"."+tab_name+" (\n");
//			String pk = "";
			String dk_flag = "";
			String pk_flag = "";
			String ptk= "";
			
			String idx1 = "";
			String idx2 = "";
			String idx3 = "";
			String idx4 = "";
			String idx5 = "";
			String idx6 = "";
			
			for(Map<String, Object> map : list){
				String col_mask = coalesce(map.get("mask"), "");
				String nullable = coalesce(map.get("nullable"), "");
				String col_name = coalesce(map.get("col_name"), "");
				String col_type = coalesce(map.get("col_type"), "");
				String col_type_str= coalesce(map.get("col_type_str"), "");
				String col_name_cn = coalesce(map.get("col_name_cn"), "");
				String case_no_sens= coalesce(map.get("case_no_sens"), "");
				
				ddl.append("    "+col_name +" "+ col_type_str);
				if("Y".equalsIgnoreCase(col_mask)/* && mask_role.length() > 0*/){
					mask.append("create mask "+schema_name+"."+tab_name+"_MASK_"+col_name+" on "+schema_name+"."+tab_name+" for column "+col_name+" return\n");
					mask.append("  case when verify_role_for_user(session_user,'"+mask_role+"') = 1 THEN ");
					if("CHAR".equalsIgnoreCase(col_type) || "VARCHAR".equalsIgnoreCase(col_type) || "CHARACTER".equalsIgnoreCase(col_type)){
						mask.append("'^_^'\n");
					}else{
						mask.append("null\n");
					}
					mask.append("       else "+col_name+" end enable;\n");
					mask.append("alter table "+schema_name+"."+tab_name+" activate column access control;\n\n");
				}else if("D".equalsIgnoreCase(col_mask)){
					mask.append("alter table "+schema_name+"."+tab_name+" deactivate column access control;\n");
					mask.append("drop mask "+schema_name+"."+tab_name+"_MASK_"+col_name+";\n\n");
				}
				
				if("ECIF".equalsIgnoreCase(IniUtil.getValByKey("ADTS", "app"))){
					//对于ECIF系统的ECIF_PDATA的特殊处理:varchar和char默认不允许为空
					if("ECIF_PDATA".equalsIgnoreCase(schema_name)
							&& ("CHAR".equalsIgnoreCase(col_type) || "VARCHAR".equalsIgnoreCase(col_type) || "CHARACTER".equalsIgnoreCase(col_type))
							&& !"Y".equalsIgnoreCase(nullable)){
						ddl.append(" NOT NULL,\n");
					}else{
						//否则默认允许为null
						if("N".equalsIgnoreCase(nullable)){
							ddl.append(" NOT NULL,\n");
						}else{
							ddl.append(",\n");
						}
					}
				}else{
					if("N".equalsIgnoreCase(nullable)){
						ddl.append(" NOT NULL,\n");
					}else{
						ddl.append(",\n");
					}
				}
				
				if("Y".equalsIgnoreCase(case_no_sens)){
					col_case.append("delete from ECIF_SYSADMIN.SYS_FILE_COL_CFG where table_name = '"
							+tab_name+"' and col_name = '"+col_name+"';\n");
					col_case.append("insert into ECIF_SYSADMIN.SYS_FILE_COL_CFG(table_name,col_name,col_title,letter_case) values(");
					col_case.append("'"+tab_name+"',");
					col_case.append("'"+col_name+"',");
					col_case.append("'"+col_name_cn+"',");
					col_case.append("'U');\n");
				}
				if(!"".equals(col_name_cn)){
					comment.append("COMMENT ON COLUMN "+schema_name+"."+tab_name+"."+col_name +" IS '"+col_name_cn+"';\n");
				}
				//主键顺序需要排序
				if(!"".equals(coalesce(map.get("is_pk"), ""))){
					pk_flag = "Y";
				}
				//分布键需要排序
				if(!"".equals(coalesce(map.get("is_dk"), ""))){
					dk_flag = "Y";
				}
				//分区键默认填写规则为单字段
				if(!"".equals(coalesce(map.get("is_ptk"), ""))){
					if("".equals(ptk)){
						ptk = col_name;
					}else{
						ptk = ptk + "," +col_name;
					}
				}
				
				if(isIdx(map.get("idx_1"))){
					idx1 = "Y";
				}
				if(isIdx(map.get("idx_2"))){
					idx2 = "Y";
				}
				if(isIdx(map.get("idx_3"))){
					idx3 = "Y";
				}
				if(isIdx(map.get("idx_4"))){
					idx4 = "Y";
				}
				if(isIdx(map.get("idx_5"))){
					idx5 = "Y";
				}
				if(isIdx(map.get("idx_6"))){
					idx6 = "Y";
				}
			}
			
			if("Y".equals(pk_flag)){
				String pk_list = genPKList(paraMap);
				ddl.append("    PRIMARY KEY("+pk_list+"),\n");
			}
			
			ddl.delete(ddl.lastIndexOf(","),ddl.length());
			
			ddl.append("\n)\nCOMPRESS YES ADAPTIVE\n");
			if("Y".equals(dk_flag)){
				String dk_list = genDKList(paraMap);
				ddl.append("DISTRIBUTE BY HASH("+dk_list+")\n");
			}
			if(!"".equals(tab_space)){
				ddl.append(" IN "+tab_space);
			}
			if(!"".equals(idx_space)){
				ddl.append(" INDEX IN "+idx_space+"\n");
			}
			
			if(!"".equals(ptk) && ptk_info_list != null && ptk_info_list.size() > 0){
				ddl.append(" PARTITION BY RANGE("+ptk+" NULLS FIRST)(\n");
				for(int i=0;i<ptk_info_list.size();i++){
					String ptk_info = ptk_info_list.get(i).toString();
					ptk_info = ptk_info.trim().replace("$TABLE_SPACE", tab_space);
					if(!"".equals(idx_space)){
						ptk_info = ptk_info.replace("$INDEX_IN_INDEX_SPACE", "INDEX IN "+idx_space);
					}else{
						ptk_info = ptk_info.replace("$INDEX_IN_INDEX_SPACE", "");
					}
					if(i != ptk_info_list.size() - 1){
						ddl.append("    "+ptk_info+",\n");
					}else{
						ddl.append("    "+ptk_info+"\n)\n");
					}
				}
				
			}
			//处理 ORGANIZE BY 信息
			if(!"".equals(org_by)){
				String[] cols = org_by.split("\\,");
				ddl.append("ORGANIZE BY ROW USING (");
				for(int i = 0;i<cols.length;i++){
					if(i != cols.length - 1){
						ddl.append("("+cols[i]+"),");
					}else{
						ddl.append("("+cols[i]+")");
					}
				}
				ddl.append(");\n");
			}else{
				ddl.append("ORGANIZE BY ROW;\n");
			}
			
			// 处理索引信息
			if("Y".equalsIgnoreCase(idx1)){
				String index_ddl = genIndexDDL("idx_1",paraMap);
				index.append(index_ddl+"\n");
			}
			if("Y".equalsIgnoreCase(idx2)){
				String index_ddl = genIndexDDL("idx_2",paraMap);
				index.append(index_ddl+"\n");
			}
			if("Y".equalsIgnoreCase(idx3)){
				String index_ddl = genIndexDDL("idx_3",paraMap);
				index.append(index_ddl+"\n");
			}
			if("Y".equalsIgnoreCase(idx4)){
				String index_ddl = genIndexDDL("idx_4",paraMap);
				index.append(index_ddl+"\n");
			}
			if("Y".equalsIgnoreCase(idx5)){
				String index_ddl = genIndexDDL("idx_5",paraMap);
				index.append(index_ddl+"\n");
			}
			if("Y".equalsIgnoreCase(idx6)){
				String index_ddl = genIndexDDL("idx_6",paraMap);
				index.append(index_ddl+"\n");
			}
			ddl.append("\n");
			ddl.append(comment);
			// 生成 grant 信息
			if(!"".equals(grant_sel)){
				String grant_sql = genGrantDDL("SELECT",grant_sel,paraMap);
				grant.append(grant_sql + "\n");
			}
			if(!"".equals(grant_ins)){
				String grant_sql = genGrantDDL("INSERT",grant_ins,paraMap);
				grant.append(grant_sql + "\n");
			}
			if(!"".equals(grant_del)){
				String grant_sql = genGrantDDL("DELETE",grant_del,paraMap);
				grant.append(grant_sql + "\n");
			}
			if(!"".equals(grant_upd)){
				String grant_sql = genGrantDDL("UPDATE",grant_upd,paraMap);
				grant.append(grant_sql + "\n");
			}
			if(!"".equals(grant_alt)){
				String grant_sql = genGrantDDL("ALTER",grant_alt,paraMap);
				grant.append(grant_sql + "\n");
			}
			if(!"".equals(grant_rud)){
				String grant_sql = genGrantDDL("SELECT,INSERT,DELETE,UPDATE",grant_rud,paraMap);
				grant.append(grant_sql + "\n");
			}
			
			result.put("grant", grant.toString());
			result.put("col_sens", col_case.toString());
			result.put("index", index.toString());
			result.put("ddl", ddl.toString());
			result.put("mask", mask.toString());
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String genPKList(Map<String,Object> paraMap){
		try {
			List<Map<String, Object>>  list = dao.select("getPKCol", paraMap);
			if(list.size() == 0){
				return "";
			}
			String cols = "";
			for(int i=0;i<list.size();i++){
				Map<String, Object> map = list.get(i);
				String col_name = map.get("col_name").toString();
				if(i != list.size() - 1){
					cols += col_name + ",";
				}else{
					cols += col_name;
				}
			}
			return cols;
		} catch (SQLException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private String genDKList(Map<String,Object> paraMap){
		try {
			List<Map<String, Object>>  list = dao.select("getDKCol", paraMap);
			if(list.size() == 0){
				return "";
			}
			String cols = "";
			for(int i=0;i<list.size();i++){
				Map<String, Object> map = list.get(i);
				String col_name = map.get("col_name").toString();
				if(i != list.size() - 1){
					cols += col_name + ",";
				}else{
					cols += col_name;
				}
			}
			return cols;
		} catch (SQLException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public String genIndexDDL(String idx_no,Map<String,Object> paraMap){
		paraMap.put("idx_no", idx_no);
		try {
			List<Map<String, Object>>  list = dao.select("getIndexByIdxNo", paraMap);
			if(list.size() == 0){
				return "";
			}
			String index_type = list.get(0).get("idx_no").toString();
			String cols = "";
			for(int i=0;i<list.size();i++){
				Map<String, Object> map = list.get(i);
				String col_name = map.get("col_name").toString();
				if(i != list.size() - 1){
					cols += col_name + ",";
				}else{
					cols += col_name;
				}
			}
			if(index_type.startsWith("D")){
				return "CREATE INDEX "+paraMap.get("schema_name") + "."+paraMap.get("tab_name")+"_"+idx_no.toUpperCase()+
					" ON "+paraMap.get("schema_name") + "."+paraMap.get("tab_name")+ "("+cols+") COMPRESS YES ALLOW REVERSE SCANS;";
			}else if(index_type.startsWith("U")){
				return "CREATE UNIQUE INDEX "+paraMap.get("schema_name") + "."+paraMap.get("tab_name")+"_"+idx_no.toUpperCase()+
				" ON "+paraMap.get("schema_name") + "."+paraMap.get("tab_name")+ "("+cols+") COMPRESS YES ALLOW REVERSE SCANS;";
			}
			return "";
		} catch (SQLException e) {
			e.printStackTrace();
			return "";
		}
		
	}
	
	public String getMaskRole(Map<String,Object> paraMap) throws SQLException{
		paraMap.put("grp_in", "'DPF_MASK'");
		List<Map<String, Object>> list = dao.select("getUserByGroupName", paraMap);
		if(list.size() > 0){
			return list.get(0).get("usr_name").toString();
		}else{
			return "";
		}
	}
	
	public String genGrantDDL(String grant_type,String grp_name,Map<String,Object> paraMap){
		try {
			String[] grp = grp_name.split("\\,");
			String grp_in = "";
			for(int i = 0;i<grp.length;i++){
				if(i != grp.length - 1){
					grp_in += "'" + grp[i] + "',";
				}else{
					grp_in += "'" + grp[i] + "'";
				}
			}
			
			paraMap.put("grp_in", grp_in);
			List<Map<String, Object>> list = dao.select("getUserByGroupName", paraMap);
			String usr_list = "";
			if(list.size() == 0){return "";}
			for(int i = 0;i<list.size();i++){
				Map<String, Object> map = list.get(i);
				if(i<list.size() - 1){
					usr_list += map.get("usr_name") + ",";
				}else{
					usr_list += map.get("usr_name");
				}
			}
			return "GRANT " + grant_type + " on table " + paraMap.get("schema_name") + "." + paraMap.get("tab_name") 
					+ " to user " + usr_list + ";";
		} catch (SQLException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private boolean isIdx(Object obj){
		String flag = coalesce(obj, "");
		if(flag.startsWith("U") || flag.startsWith("D")){
			return true;
		}else{
			return false;
		}
	}
	
	private String coalesce(Object obj,String str){
		if(obj == null){
			return str;
		}else{
			return obj.toString();
		}
	}
	
	private List<String> genPtInfo(){
		List<String> list = new ArrayList<String>();
		//分区区间，从前 dpf_pt_month_start 或 dpf_pt_month_offset 月开始，共 dpf_pt_month_total 个分区
		String month_start = IniUtil.getValByKey("MODEL_PT_INFO", tag.toLowerCase()+"_pt_month_start");
		int offset = Integer.parseInt(IniUtil.getValByKey("MODEL_PT_INFO", tag.toLowerCase()+"_pt_month_offset"));
		int total = Integer.parseInt(IniUtil.getValByKey("MODEL_PT_INFO", tag.toLowerCase()+"_pt_month_total"));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-01");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMM");
		Calendar ca = Calendar.getInstance();
		if(month_start != null && month_start.length() == 6){
			//固定配置
			ca.set(Calendar.YEAR, Integer.parseInt( month_start.substring(0, 4)));
			ca.set(Calendar.MONTH, Integer.parseInt( month_start.substring(4)) - 1);
		}else{
			//动态配置
			ca.setTime(new Date());
			ca.add(Calendar.MONTH, -offset);
		}
		list.add("PART PARTMIN STARTING(MINVALUE) IN $TABLE_SPACE $INDEX_IN_INDEX_SPACE");
		for(int i=0;i < total; i++){
			String pt_info = "PART PART"+(sdf2.format(ca.getTime()))+" STARTING('" + sdf.format(ca.getTime()) + "') ENDING('";
			ca.add(Calendar.MONTH, 1);
			pt_info += sdf.format(ca.getTime()) + "') EXCLUSIVE IN $TABLE_SPACE $INDEX_IN_INDEX_SPACE";
			list.add(pt_info);
		}
		list.add("PART PARTMAX ENDING(MAXVALUE) IN $TABLE_SPACE $INDEX_IN_INDEX_SPACE");
		return list;
	}

	
}
