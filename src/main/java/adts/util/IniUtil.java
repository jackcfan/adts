package adts.util;

import java.io.IOException;
import java.util.List;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

import adts.dao.BaseDao;

public class IniUtil {
	private static Ini ini;
	private BaseDao baseDao = null;
	
	static{
		try {
			ini = new Ini(IniUtil.class.getClassLoader().getResourceAsStream("ini/adts.ini"));
		} catch (InvalidFileFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Ini getIniObj(){
		return ini;
	}
	public static String getValByKey(String section,String key){
		if(ini == null){return null;}
		return ini.get(section, key);
	}
	
	public static List<String> getListByKey(String section,String key){
		if(ini == null){return null;}
		return ini.get(section).getAll(key);
	}
}
