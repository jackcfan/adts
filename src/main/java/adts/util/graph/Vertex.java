package adts.util.graph;

import java.util.Set;

public class Vertex{
	private Set<String> incomingEdges;
	private Set<String> outgoingEdges;
	private String name;
//	private Map<String,Object> attribute;

	public Vertex(String name) {
		super();
		this.name = name;
	}

	public Vertex() {
		super();
	}

	public Vertex(String name, Set<String> incomingEdges,
			Set<String> outgoingEdges) {
		super();
		this.name = name;
		this.incomingEdges = incomingEdges;
		this.outgoingEdges = outgoingEdges;
	}

	public Set<String> getIncomingEdges() {
		return incomingEdges;
	}

	public void setIncomingEdges(Set<String> incomingEdges) {
		this.incomingEdges = incomingEdges;
	}

	public Set<String> getOutgoingEdges() {
		return outgoingEdges;
	}

	public void setOutgoingEdges(Set<String> outgoingEdges) {
		this.outgoingEdges = outgoingEdges;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}