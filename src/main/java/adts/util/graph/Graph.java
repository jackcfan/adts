package adts.util.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Graph {
	private Map<String,Vertex> graph;
	
	public Graph() {
		graph = new HashMap<String, Vertex>();
	}

	public void addVertex(String vertex_name){
		if(graph.containsKey(vertex_name)){return;}
		if(null == vertex_name){vertex_name = "";}
		Set<String> incomingEdges = new HashSet<String>();
		Set<String> outgoingEdges = new HashSet<String>();
		Vertex vertex = new Vertex(vertex_name,incomingEdges,outgoingEdges);
		graph.put(vertex_name, vertex);
	}
	
	public Vertex getVertex(String vertex_name){
		if(null == vertex_name){vertex_name = "";}
		return graph.get(vertex_name);
	}
	
	public boolean hasVertex(String vertex_name){
		if(null == vertex_name){vertex_name = "";}
		if(graph.containsKey(vertex_name)){
			return true;
		}else{
			return false;
		}
	}
	
	public void addEdge(String from,String to){
		if(null == from){from = "";}
		if(null == to){to = "";}
		addVertex(from);
		addVertex(to);
		Vertex v_f = graph.get(from);
		Vertex v_t = graph.get(to);
		Set<String> v_f_outgoing = v_f.getOutgoingEdges();
		Set<String> v_t_incoming = v_t.getIncomingEdges();
		v_f_outgoing.add(to);
		v_t_incoming.add(from);
	}
	
	public Set<String> getSuccessors(String vertex_name){
		if(!graph.containsKey(vertex_name)){return null;}
		Vertex vertex = graph.get(vertex_name);
		return vertex.getOutgoingEdges();
	}
	
	public boolean hasSuccessors(String vertex_name){
		if(!graph.containsKey(vertex_name)){return false;}
		Set<String> successors = getSuccessors(vertex_name);
		if(successors.isEmpty()){
			return false;
		}else{
			return true;
		}
	}
	
	public Set<String> getAllSuccessors(String vertex_name){
		if(!graph.containsKey(vertex_name)){return null;}
		Set<String> all_set = new HashSet<String>();
		_getAllSuccessors(vertex_name,all_set);
		return all_set;
	}
	
	private void _getAllSuccessors(String vertex_name,Set<String> all){
		Vertex vertex = graph.get(vertex_name);
		Set<String> set = vertex.getOutgoingEdges();
		for(String s : set){
			if(all.contains(s)){
//				System.out.println("cycle dep found "+s);
//				continue;    //cycle
			}
			all.add(s);
			if(hasSuccessors(s)){
				_getAllSuccessors(s,all);
			}
		}
	}
	
	public Set<String> getPredecessors(String vertex_name){
		if(!graph.containsKey(vertex_name)){return null;}
		Vertex vertex = graph.get(vertex_name);
		return vertex.getIncomingEdges();
	}
	
	public boolean hasPredecessors(String vertex_name){
		if(!graph.containsKey(vertex_name)){return false;}
		Set<String> successors = getPredecessors(vertex_name);
		if(successors.isEmpty()){
			return false;
		}else{
			return true;
		}
	}
	
	public Set<String> getAllPredecessors(String vertex_name){
		if(!graph.containsKey(vertex_name)){return null;}
		Set<String> all_set = new HashSet<String>();
		_getAllPredecessors(vertex_name,all_set);
		return all_set;
	}
	
	private void _getAllPredecessors(String vertex_name,Set<String> all){
		Vertex vertex = graph.get(vertex_name);
		Set<String> set = vertex.getIncomingEdges();
		for(String s : set){
			if(all.contains(s)){
//				continue;    //cycle
			}
			all.add(s);
			if(hasPredecessors(s)){
				_getAllPredecessors(s,all);
			}
		}
	}
}