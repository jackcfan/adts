package adts.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.mozilla.intl.chardet.nsDetector;
import org.mozilla.intl.chardet.nsICharsetDetectionObserver;
import org.mozilla.intl.chardet.nsPSMDetector;

public class Jchardet {

	public String[] detectChineseCharset(InputStream in) throws IOException {
		boolean found = false;
		String result = null;
		int lang;
		lang = nsPSMDetector.CHINESE;
		String[] prob;
		nsDetector det = new nsDetector(lang);
		// The Notify() will be called when a matching charset is found.
		det.Init(new nsICharsetDetectionObserver() {
			public void Notify(String charset) {
//				found = true;
//				result = charset;
			}
		});
		BufferedInputStream imp = new BufferedInputStream(in);
		byte[] buf = new byte[1024];
		int len;
		boolean isAscii = true;
		while ((len = imp.read(buf, 0, buf.length)) != -1) {
			// Check if the stream is only ascii.
			if (isAscii)
				isAscii = det.isAscii(buf, len);
			// DoIt if non-ascii and not done yet.
			if (!isAscii) {
				if (det.DoIt(buf, len, false))
					break;
			}
		}
		imp.close();
		in.close();
		det.DataEnd();
		if (isAscii) {
			found = true;
			prob = new String[] { "ASCII" };
		} else if (found) {
			prob = new String[] { result };
		} else {
			prob = det.getProbableCharsets();
		}
		return prob;
	}

	public String[] detectAllCharset(InputStream in) throws IOException {
		try {
			int lang = nsPSMDetector.ALL;
			return detectChineseCharset(in);
		} catch (IOException e) {
			throw e;
		}
	}

	public static void main(String[] args) throws IOException {
		Jchardet charDect = new Jchardet();
		URL url = new URL("http://www.oschina.net/");
//		String[] probableSet = charDect.detectChineseCharset(url.openStream());
		String sourcePage = "D:\\doc\\���õļ����㷨����Щ.txt";
		String[] probableSet = charDect.detectChineseCharset(new FileInputStream(sourcePage));
		for (String charset : probableSet) {
			System.out.println(charset);
		}
		
		String encoding = probableSet[0];
		String cread;
		StringBuffer content = new StringBuffer();
		InputStreamReader r = new InputStreamReader(new FileInputStream(
				sourcePage), encoding);
		BufferedReader in = new BufferedReader(r);
		while ((cread = in.readLine()) != null) {
			content.append(cread);
		}
		System.out.print(content.toString());
	}
}
