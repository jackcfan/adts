package adts.util;

import java.util.Map;

public interface DDLUtilInterface{
	
	public Map<String,String> genDDL(String schema_name,String tab_nam);
	
	public String genDDL(String[] tables);
}
