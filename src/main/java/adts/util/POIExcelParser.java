package adts.util;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class POIExcelParser {
	public static final String xls_mapping = "excelmapping/xls_mapping.xml";
	
	private static Logger logger = Logger.getLogger(POIExcelParser.class);
	
	/**
	 * 根据xml文件返回xls指定sheet页的字段映射
	 * */
	public static HashMap<String, Object> getMapping(String file,String worksheet) throws Exception{
		SAXReader reader = new SAXReader();
		Document doc = reader.read(ExcelParser.class.getClassLoader().getResourceAsStream(file));
		HashMap<String, Object> mapping = new HashMap<String, Object>();
		for(Iterator<Element> it = doc.getRootElement().elementIterator("worksheet");it.hasNext();){
			Element ele_worksheet = it.next();
			if(!ele_worksheet.attributeValue("name").trim().equals(worksheet)){
				continue;
			}
			mapping.put("fields", new HashMap<String,String>());
			for(int i=0;i<ele_worksheet.elements().size();i++){
				Element property = ((Element)ele_worksheet.elements().get(i));
				if("header".equalsIgnoreCase(property.getName())){
					mapping.put("header", Integer.parseInt(property.attributeValue("rows")));
				}else if("fields".equalsIgnoreCase(property.getName())){
					HashMap<String,HashMap<String,String>> fields = (HashMap<String, HashMap<String, String>>) mapping.get("fields");
					for(int j=0;j<property.elements().size();j++){
						Element ele_field = (Element) property.elements().get(j);

						HashMap<String,String> field = new HashMap<String,String>();
						field.put("col_index", ele_field.attributeValue("col_index"));
						field.put("col_name", ele_field.attributeValue("col_name"));
						field.put("comment", ele_field.attributeValue("comment"));
						field.put("col_type", ele_field.attributeValue("col_type"));
						field.put("format", ele_field.attributeValue("format"));
						field.put("lettercase", ele_field.attributeValue("lettercase"));
						fields.put(ele_field.attributeValue("col_name"), field);
					}
				}
			}
		}
		return mapping;
	}
	
	public static HashMap<String, Object> getMapping_(String file,String worksheet) throws Exception{
		SAXReader reader = new SAXReader();
		Document doc = reader.read(ExcelParser.class.getClassLoader().getResourceAsStream(file));
		HashMap<String, Object> mapping = new HashMap<String, Object>();
		for(Iterator<Element> it = doc.getRootElement().elementIterator("worksheet");it.hasNext();){
			Element e = it.next();
			if(!e.attributeValue("name").trim().equals(worksheet)){
				continue;
			}
			mapping.put("fields", new HashMap<String,String>());
			for(int i=0;i<e.elements().size();i++){
				Element property = ((Element)e.elements().get(i));
				if("header".equalsIgnoreCase(property.getName())){
					mapping.put("header", Integer.parseInt(property.attributeValue("rows")));
				}else{
					HashMap<String,HashMap<String,String>> fields = (HashMap<String, HashMap<String, String>>) mapping.get("fields");
					HashMap<String,String> field = new HashMap<String,String>();
					field.put("col_index", property.attributeValue("col_index"));
					field.put("col_name", property.attributeValue("col_name"));
					field.put("comment", property.attributeValue("comment"));
					field.put("col_type", property.attributeValue("col_type"));
					field.put("format", property.attributeValue("format"));
					field.put("lettercase", property.attributeValue("lettercase"));
					
					fields.put(property.attributeValue("col_name"), field);
				}
			}
		}
		return mapping;
	}
	
	/**
	 * 读取指定xls文件的指定sheet页，并返回 数组
	 * xml_mapping_name xml配置中的worksheet名字，如果指定则用指定的sheet配置，如果不指定则默认取和sheet页相同的名字
	 * */
	public static List<Map<String,Object>> readXLS(String xls,String sheet_name,String xml_mapping_name) throws Exception{
		return readXLS(new File(xls),sheet_name,xml_mapping_name,null);
	}
	
	/**
	 * 读取指定xls文件的指定sheet页，并返回 数组
	 * xml_mapping_name xml配置中映射的名字worksheet和excel中的sheet页名字相同
	 * */
	public static List<Map<String,Object>> readXLS(String xls,String sheet_name) throws Exception{
		return readXLS(new File(xls),sheet_name,sheet_name,null);
	}
	
	/**
	 * 读取指定xls文件的指定sheet页，并返回 数组
	 * POIExcelFilter filter 结果集过滤器,不满足条件的记录会被过滤掉
	 * */
	public static List<Map<String,Object>> readXLS(String xls,String sheet_name,String xml_mapping_name,POIExcelFilter filter) throws Exception{
		return readXLS(new File(xls),sheet_name,xml_mapping_name,null);
	}
	
	/**
	 * 读取指定xls文件的指定sheet页，并返回 数组
	 * POIExcelFilter filter 结果集过滤器,不满足条件的记录会被过滤掉
	 * */
	public static List<Map<String,Object>> readXLS(String xls,String sheet_name,POIExcelFilter filter) throws Exception{
		return readXLS(new File(xls),sheet_name,sheet_name,filter);
	}
	
	public static List<Map<String,Object>> readXLS(File xls,String sheet_name) throws Exception{
		return readXLS(xls,sheet_name,sheet_name,null);
	}
	
	public static List<Map<String,Object>> readXLS(File xls,String sheet_name,String xml_mapping_name,POIExcelFilter filter) throws Exception{
		List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
		String name = xls.getName();
		if(name.indexOf(".") == -1){return null;}
		String suffix = name.substring(name.lastIndexOf("."));
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		HashMap<String, Object> mapping = getMapping(xls_mapping,xml_mapping_name==null?sheet_name:xml_mapping_name);
		int header = (Integer) mapping.get("header");
		int total  = 0;
		Object sheet;
//		根据扩展名识别xls还是xlsx
		if(".xls".equalsIgnoreCase(suffix)){
			HSSFWorkbook book = new HSSFWorkbook(new FileInputStream(xls));
			sheet = book.getSheet(sheet_name);
			if(sheet == null){return null;}
			total = ((HSSFSheet)sheet).getLastRowNum();
		}else{
			XSSFWorkbook book = new XSSFWorkbook(new FileInputStream(xls));
			sheet = book.getSheet(sheet_name);
			if(sheet == null){return null;}
//			物理行数，不计空行
//			total = ((XSSFSheet)sheet).getPhysicalNumberOfRows();
//			getLastRowNum 逻辑上的行数，从0开始计数
			total = ((XSSFSheet)sheet).getLastRowNum();
		}
		for(int i=header;i<= total;i++){
			if(filter != null && filter.end()){
				break;
			}
			Map<String,Object> map = readXLSByRow(sheet, mapping, i);
			if(map == null){
				continue;  //跳过空行(逻辑空行或物理空行)
			}
			if(filter != null && !filter.accept(map)){
				continue;  //跳过不满足过滤器的条件的记录
			}
			result.add(map);
		}
		return result;
	}
	
//	读取指定sheet页的指定行并返回该行的hash结构
	private static Map<String,Object> readXLSByRow(Object sheet,HashMap<String, Object> mapping,int row){
		if(!(sheet instanceof HSSFSheet) && !(sheet instanceof XSSFSheet)){
			return null;
		}
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String,Object> fields = (HashMap<String,Object>)mapping.get("fields");
		boolean empty_row = true;	//空行标志位,如果为空行则直接返回null
		for(String col_name : fields.keySet()){
			Map<String,Object> column_mapping = (Map<String, Object>) fields.get(col_name);
			String type = column_mapping.get("col_type").toString();
			int col = getColIndex(mapping,col_name);
			Object col_value = getCellValueByRowCol(sheet,column_mapping, row,col);
			if(col_value != null && !"".equals(col_value.toString()) && empty_row){
				empty_row = false;
			}
			result.put(col_name, col_value);
		}
		if(empty_row){ //如果是空行，则返回null
			return null;
		}else{
			return result;
		}
	}
	
	/**
	private static Map<String,Object> readXLSByRow_bak(Object sheet,HashMap<String, Object> mapping,int row){
		if(!(sheet instanceof HSSFSheet) && !(sheet instanceof XSSFSheet)){
			return null;
		}
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String,Object> fields = (HashMap<String,Object>)mapping.get("fields");
		boolean empty_row = true;	//空行标志位,如果为空行则直接返回null
		for(String col_name : fields.keySet()){
			Map<String,Object> column = (Map<String, Object>) fields.get(col_name);
			String type = column.get("col_type").toString();
			int col = getColIndex(mapping,col_name);
			Object col_value = getCellValueByRowCol(sheet,column, row,col);
			if(col_value != null && !"".equals(col_value.toString()) && empty_row){
				empty_row = false;
			}
			if("String".equalsIgnoreCase(type)){
				result.put(col_name, col_value);
			}else if("Integer".equalsIgnoreCase(type)){
				result.put(col_name, Integer.valueOf(col_value.toString()));
			}else{
				result.put(col_name, col_value);
			}
		}
		if(empty_row){
			return null;
		}else{
			return result;
		}
	}*/
	
	/**
	 * 根据字段名获得该字段在Excel中列的索引位置
	 * */
	private static int getColIndex(HashMap<String, Object> mapping,String col_name){
		Map<String,Object> fields = (Map<String,Object>)mapping.get("fields");
		Map<String,Object> field = (Map<String,Object>)fields.get(col_name);
		String col_str = field.get("col_index").toString().toUpperCase();
		int col = -1;
		char[] cs = col_str.toCharArray();
		for(int i=0;i<cs.length;i++){
			col += (cs[i]-64)*Math.pow(26, cs.length-1-i);
		}
		return col;
	}
	
//	#TODO 根据单元格的实际类型返回格式化后的对象(测试中) 
	private static Object getCellValueByRowCol(Object sheet,Map<String,Object> field_mapping,int row,int col){
		if(sheet instanceof HSSFSheet){
			HSSFRow hss_row = ((HSSFSheet )sheet).getRow(row);
			if(hss_row == null){return "";}
			HSSFCell cell = hss_row.getCell(col);
			if(cell == null){return "";}

			switch (hss_row.getCell(col).getCellType()) {
			/*case HSSFCell.CELL_TYPE_BLANK:
				cell.setCellType(Cell.CELL_TYPE_STRING);
				return cell.getStringCellValue().trim();
			case HSSFCell.CELL_TYPE_BOOLEAN:
				return cell.getBooleanCellValue();
			case HSSFCell.CELL_TYPE_ERROR:
				return cell.getErrorCellString();
			case HSSFCell.CELL_TYPE_FORMULA:
				return cell.getCellFormula();*/
			case HSSFCell.CELL_TYPE_NUMERIC:
//				日期、时间均以数值形式存储
				if(HSSFDateUtil.isCellDateFormatted(cell)){
					Date date = cell.getDateCellValue();
					if(date == null){
						return "";
					}else if(field_mapping.containsKey("format")){
						//如果限定了格式则以限定格式转换并返回格式化后的字符串
						if(field_mapping.get("format") == null){
							logger.warn("单元格["+row+","+col+"]为日期类型，未配置format。日期值:"+date);
							return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
						}
						return new SimpleDateFormat(field_mapping.get("format").toString()).format(date);
					}else{
						return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
					}
				}else{
					if("Integer".equalsIgnoreCase(field_mapping.get("col_type").toString())){
						return (int)cell.getNumericCellValue();
					}else if("String".equalsIgnoreCase(field_mapping.get("col_type").toString())){
						cell.setCellType(Cell.CELL_TYPE_STRING);
						return cell.getStringCellValue();
					}else{
						return cell.getNumericCellValue();
					}
				}
			case XSSFCell.CELL_TYPE_STRING:
				if(cmpObjectString(field_mapping.get("lettercase"), "upper")){
					return cell.getStringCellValue().trim().toUpperCase();
				}
				if(cmpObjectString(field_mapping.get("lettercase"), "lower")){
					return cell.getStringCellValue().trim().toLowerCase();
				}
				return cell.getStringCellValue().trim();
			default:
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if(cmpObjectString(field_mapping.get("lettercase"), "upper")){
					return cell.getStringCellValue().trim().toUpperCase();
				}
				if(cmpObjectString(field_mapping.get("lettercase"), "lower")){
					return cell.getStringCellValue().trim().toLowerCase();
				}
				return cell.getStringCellValue().trim();
			}
		}else if(sheet instanceof XSSFSheet){
			XSSFRow xssf_row = ((XSSFSheet )sheet).getRow(row);
			if(xssf_row == null){return "";}
			XSSFCell cell = xssf_row.getCell(col);
			if(cell == null){return "";}
			
			switch (xssf_row.getCell(col).getCellType()) {
			/*case XSSFCell.CELL_TYPE_BLANK:
				cell.setCellType(Cell.CELL_TYPE_STRING);
				return cell.getStringCellValue().trim();
			case XSSFCell.CELL_TYPE_BOOLEAN:
				return cell.getBooleanCellValue();
			case XSSFCell.CELL_TYPE_ERROR:
				return cell.getErrorCellString();
			case XSSFCell.CELL_TYPE_FORMULA:
				return cell.getCellFormula();*/
			case XSSFCell.CELL_TYPE_NUMERIC:
//				日期、时间均以数值形式存储
				if(HSSFDateUtil.isCellDateFormatted(cell)){
					Date date = cell.getDateCellValue();
					System.out.println(date);
					if(field_mapping.containsKey("format")){	//如果限定了格式则以限定格式转换并返回格式化后的字符串
						return new SimpleDateFormat(field_mapping.get("format").toString()).format(date);
					}else{
						return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
					}
				}else{
					if("Integer".equalsIgnoreCase(field_mapping.get("col_type").toString())){
						return (int)cell.getNumericCellValue();
					}else if("String".equalsIgnoreCase(field_mapping.get("col_type").toString())){
						cell.setCellType(Cell.CELL_TYPE_STRING);
						return cell.getStringCellValue();
					}else{
						return cell.getNumericCellValue();
					}
				}
			case XSSFCell.CELL_TYPE_STRING:
				if(cmpObjectString(field_mapping.get("lettercase"), "upper")){
					return cell.getStringCellValue().trim().toUpperCase();
				}
				if(cmpObjectString(field_mapping.get("lettercase"), "lower")){
					return cell.getStringCellValue().trim().toLowerCase();
				}
				return cell.getStringCellValue().trim();
			default:
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if(cmpObjectString(field_mapping.get("lettercase"), "upper")){
					return cell.getStringCellValue().trim().toUpperCase();
				}
				if(cmpObjectString(field_mapping.get("lettercase"), "lower")){
					return cell.getStringCellValue().trim().toLowerCase();
				}
				return cell.getStringCellValue().trim();
			}
		}
		return "";
	}
	
	private static boolean cmpObjectString(Object obj,String string){
		if(obj == null){
			return false;
		}
		if(string.equalsIgnoreCase(obj.toString())){
			return true;
		}
		return false;
	}

}
