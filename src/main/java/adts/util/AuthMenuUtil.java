package adts.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 菜单展示工具类
 * 初始化工具类时需要传入菜单列表 List<Map<String,Object>> list 和菜单的root id
 * */
public class AuthMenuUtil {
    /**
     * 返回指定id下所有菜单的json字符串数组
     * */
    public static String getJsonMenu(int root_id,List<Map<String,Object>> list){
    	StringBuffer json = new StringBuffer();
        json.append("[");
        initSubMenu(root_id,json,list,false);
        if(json.charAt(json.length() - 1) == ','){
            json.deleteCharAt(json.length() - 1);
        }
        json.append("]");
        return json.toString();
    }
    
    public static String getJsonMenuWithChecked(int root_id,List<Map<String,Object>> list){
    	StringBuffer json = new StringBuffer();
        json.append("[");
        initSubMenu(root_id,json,list,true);
        if(json.charAt(json.length() - 1) == ','){
            json.deleteCharAt(json.length() - 1);
        }
        json.append("]");
        return json.toString();
    }
    /**
     * List<Map<String,Object>> list demo
     *  id parent_id title
     *  0         ROOT
     *  7    6    菜单管理
     *  6    5    权限管理
     *  2    1    模型更新历史
     *  3    1    表级信息
     *  4    1    字段级信息
     *  1    0    逻辑模型
     *  5    0    后台管理
     *  
     * json 字符串 demo
     * [
     *   {id:'1',title:'逻辑模型',isLeaf:false,children:[
     *       {id:'2',title:'模型更新历史',isLeaf:true},
     *       {id:'3',title:'表级信息',isLeaf:true},
     *       {id:'4',title:'字段级信息',isLeaf:true}]
     *   },
     *   {id:'5',title:'后台管理',isLeaf:false,children:[
     *       {id:'6',title:'权限管理',isLeaf:false,children:[
     *           {id:'7',title:'菜单管理',isLeaf:true}]
     *       }]
     *   }
     *  ]
     * */
    private static void initSubMenu(int id,StringBuffer json,List<Map<String,Object>> list,boolean with_check){
        List<Map<String,Object>> sub_list = getSubMenu(id,list);
        if(sub_list.size() == 0){ //is leaf
            Map<String,Object> map = getNode(id,list);
            if(map != null){
            	String checked;
            	if(with_check){
            		checked = ",checked:"+map.get("checked");
            	}else{
            		checked = "";
            	}
            	json.append("{id:'"+map.get("id")+"',text:'"+map.get("text")+"',title:'"+map.get("title")+"'"+checked
            			    +",expanded:"+getExpanded(map.get("expanded"))+",iconCls:'"+map.get("iconcls")+"',url:'"+map.get("url")
                            +"',leaf:true},");
            }
        }else{
            for(Map<String,Object> map : sub_list){
                int r_id = (Integer)map.get("id");
                boolean is_leaf = isLeaf(r_id,list);
                if(!is_leaf){
                	String checked;
                	if(with_check){
                		checked = ",checked:"+map.get("checked");
                	}else{
                		checked = "";
                	}
                    json.append("{id:'"+map.get("id")+"',text:'"+map.get("text")+"',title:'"+map.get("title")+"',url:'"+map.get("url")
                    		    +"',expanded:"+getExpanded(map.get("expanded"))+",iconCls:'"+map.get("iconcls")+"'"+checked
                	            +",leaf:false,children:[");
                }
                initSubMenu(r_id,json,list,with_check);
                if(!is_leaf){
                    if(json.charAt(json.length() - 1) == ','){
                        json.deleteCharAt(json.length() - 1);
                    }
                    json.append("]},");
                }
            }
        }
    }
    
    private static String getExpanded(Object obj){
    	if(obj  == null){
    		return "false";
    	}
    	if("Y".equalsIgnoreCase(obj.toString()) || "1".equalsIgnoreCase(obj.toString())){
    		return "true";
    	}else{
    		return "false";
    	}
    }
    private static List<Map<String,Object>> getSubMenu(int id,List<Map<String,Object>> list){
        List<Map<String,Object>> sub_list = new ArrayList<Map<String,Object>>();
        for(Map<String,Object> map : list){
            if(map.containsKey("parent_id") && (Integer)map.get("parent_id") == id){
                sub_list.add(map);
            }
        }
        return sub_list;
    }
    
    private static Map<String,Object> getNode(int id,List<Map<String,Object>> list){
        for(Map<String,Object> map : list){
            if(map.containsKey("id") && (Integer)map.get("id") == id){
                return map;
            }
        }
        return null;
    }
    
    private static boolean isLeaf(int id,List<Map<String,Object>> list){
        for(Map<String,Object> map : list){
            if(map.containsKey("parent_id") && (Integer)map.get("parent_id") == id){
                return false;
            }
        }
        return true;
    }
}
