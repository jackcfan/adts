package adts.util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.Date; 
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;

public class Util {
	
	private static String version = updateVersion();	//页面缓存版本号
	
	public static String getUUID(){
		UUID uuid = UUID.randomUUID();
		return uuid.toString().replace("-", "").toUpperCase();
	}
	
	public static String getCurrentDateTime(){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		return df.format(new Date());
	}
	
	public static String join(List<String> list,String separator,String prefix,String postfix){
		if(list == null){return "";}
		if(separator == null){separator = "";}
		if(prefix == null){prefix = "";}
		if(postfix == null){postfix = "";}
		if(list.size() == 0){return "";}
		StringBuffer buff = new StringBuffer();
		for(String str : list){
			buff.append(prefix).append(str).append(postfix).append(separator);
		}
		if(buff.length() == 0){
			return "";
		}
		return buff.deleteCharAt(buff.length()-1).toString();
	}
	
	/**
	 * 根据列表生成DDL语句
	 * 列表字段：tname_en,tname_cn,col_en,col_type,col_cn,is_dk
	 * 列表字段默认已按照字段序号升序排序
	 * */
	public static List<String> genDDLList(List<Map<String, Object>> list) {
		List<String> ddl = new ArrayList<String>();
		Map<String, Object> head = list.get(0);
		StringBuffer buff = new StringBuffer();
		StringBuffer dk = new StringBuffer();
		String tname_en = head.get("tname_en").toString();
		Object tname_cn     = head.get("tname_cn");
		if(tname_cn !=null && tname_cn.toString().trim().length() > 0){
			ddl.add("COMMENT ON TABLE  "+tname_en+" IS '"+tname_cn+"';");
		}
		buff.append("CREATE TABLE "+tname_en+" \n(\n");
		for(int i=0;i<list.size();i++){
			Map<String, Object> map = list.get(i);
			Object col_en   = map.get("col_en");
			Object col_type = map.get("col_type");
			Object col_cn   = map.get("col_cn");
			Object is_dk    = map.get("is_dk");
			buff.append("    "+col_en.toString());
			buff.append(" "+(col_type==null?"":col_type.toString()));
			if(i!=list.size()-1){
				buff.append(",\n");
			}else{
				buff.append("\n");
			}
			if(col_cn != null && col_cn.toString().trim().length() > 0){
				ddl.add("COMMENT ON COLUMN "+tname_en+"."+col_en.toString() +" IS '"+col_cn+"';");
			}
			if(is_dk != null && is_dk.toString().trim().equalsIgnoreCase("Y")){
				dk.append(col_en+",");
			}
		}
		buff.append(")");
		if(dk.length()>0){
			dk.deleteCharAt(dk.length()-1);
			buff.append("\nDISTRIBUTE ON ("+dk.toString()+");");
		}else{
			buff.append(";");
		}
		ddl.add(0,buff.toString());
		return ddl;
	}
	
	/**
	 * 根据列表生成DDL语句
	 * 列表字段：tname_en,tname_cn,col_en,col_type,col_cn,is_dk
	 * 列表字段默认已按照表名、字段序号升序排序
	 * */
	public static List<String> genMultiTableDDLList(List<Map<String, Object>> list) {
		List<String> ddl = new ArrayList<String>();
		if(list == null || list.size() == 0){
			return ddl;
		}
		Map<String,List<Map<String, Object>>> tables = new HashMap<String,List<Map<String, Object>>>();
		for(Map<String, Object> map : list){
			String tname_en = map.get("tname_en").toString();
			if(tables.containsKey(tname_en)){
				List<Map<String, Object>> col = tables.get(tname_en);
				col.add(map);
			}else{
				List<Map<String, Object>> col = new ArrayList<Map<String,Object>>();
				col.add(map);
				tables.put(tname_en, col);
			}
		}
		Set<String> set = tables.keySet();
		TreeSet<String> tree = new TreeSet<String>(set); 
		for(String table : tree){
			List<Map<String, Object>> col = tables.get(table);
			List<String> ddl_list = genDDLList(col);
			for(String d : ddl_list){
				ddl.add(d);
			}
		}
		return ddl;
	}

	public static String getVersion() {
		return version;
	}

	public static String updateVersion() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
		Util.version = df.format(new Date());
		return Util.version;
	}
	
	public static String getMD5(File file) {
		byte[] buffer;
		FileInputStream in = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			in = new FileInputStream(file);
			buffer = new byte[8192]; // 8KB
			int length = -1;
			while ((length = in.read(buffer)) != -1) {
				md.update(buffer, 0, length);
			}
			String str = bytesToString(md.digest());
			return str;
		} catch (IOException ex) {
			buffer = null;
			return new String(buffer);
		} catch (NoSuchAlgorithmException ex) {
			buffer = null;
			return new String(buffer);
		} finally {
			try {
				in.close();
			} catch (IOException ex) {
				
			}
		}
	}
	public static String getMD5(String str) {
		if(str == null){return null;}
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			return bytesToString(md.digest());
		} catch (Exception ex) {
			return null;
		}
	}
	public static String getSHA(File file) {
		byte[] buffer;
		FileInputStream in = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA");
			in = new FileInputStream(file);
			buffer = new byte[8192];
			int length = -1;
			while ((length = in.read(buffer)) != -1) {
				md.update(buffer, 0, length);
			}
			String str = bytesToString(md.digest());
			return str;
		} catch (IOException ex) {
			buffer = null;
			return new String(buffer);
		} catch (NoSuchAlgorithmException ex) {
			buffer = null;
			return new String(buffer);
		} finally {
			try {
				in.close();
			} catch (IOException ex) {
			}
		}
	}
	public static String getSHA(String  str) {
		if(str == null){return null;}
		try {
			MessageDigest md = MessageDigest.getInstance("SHA");
			md.update(str.getBytes());
			
			return bytesToString(md.digest());
		} catch (Exception ex) {
			return null;
		}
	}
	
	public static boolean makePath(String path){
		File dir = new File(path);
		if(dir.exists() && dir.isFile()){
			return false;
		}else if(dir.exists() && dir.isDirectory()){
			return true;
		}else if(!dir.exists()){
			return dir.mkdirs();
		}
		return false;
	}

	private static String bytesToString(byte[] data) {
		char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f'};
		
		char[] temp = new char[data.length * 2];
		for (int i = 0; i < data.length; ++i) {
			byte b = data[i];
			temp[(i * 2)] = hexDigits[(b >>> 4 & 0xF)];
			temp[(i * 2 + 1)] = hexDigits[(b & 0xF)];
		}
		return new String(temp).toUpperCase();
	}
}
