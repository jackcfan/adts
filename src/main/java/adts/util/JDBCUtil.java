package adts.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCUtil {
	
	public static Connection getConnection(String driver,String url,String usr,String pwd){
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		Connection con;
		try {
			con = DriverManager.getConnection(url, usr, pwd);
			return con;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Connection getConnectionByTag(String tag){
		String driver = IniUtil.getValByKey(tag, "driver");
		String url    = IniUtil.getValByKey(tag, "url");
		String usr    = IniUtil.getValByKey(tag, "usr");
		String pwd    = IniUtil.getValByKey(tag, "pwd");
		return getConnection(driver, url, usr, pwd);
	}
	
	public static void close(ResultSet rs,Statement st,Connection con){
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		try {
			if (st != null)
				st.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		try {
			if (con != null)
				con.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	public static Integer toInt(String s){
		Integer n =null;
		try{
			n = Integer.parseInt(s);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return n;
	}
}