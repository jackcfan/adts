package adts.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * XSSF and SAX (Event API)
 * Excel 2007 XLSX 格式采用xml存储数据
 * 本身的xlsx文件是经过zip压缩后的文件，大文件无法一次性载入内存
 * 本抽象工具以SAX事件触发方式读取，避免内存溢出
 */
public abstract class XlsxAbstractUtil extends DefaultHandler {
	private String XLS_MAPPING = "xls_mapping.xml";		//单元格映射配置文件,相对于class path
	private static final String XML_READER_CLASS = "org.apache.xerces.parsers.SAXParser";

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat stf = new SimpleDateFormat("HH:mm:ss");
	private static SimpleDateFormat sdtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private boolean date_flag;
	private boolean string_flag;
	private boolean time_flag;
	private boolean timestamp_flag;
	private boolean empty_row_flag = true; //空行标记，默认为空行
	
	private int cur_row = 0;
	private int cur_col = 0;
	private String last_contents;
	
	private XSSFReader reader;
	private XMLReader  parser;
	private SharedStringsTable sst;				//共享字符串表
	private OPCPackage opcp;

	private Map<String,String> row_map = new HashMap<String,String>();
	
	private int header_row = 0;  				//表头行数
	private String xls_mapping_worksheet_name;	//当前处理的sheet页对应 XLS_MAPPING 中的配置名
	private String worksheet_name;             //当前处理的sheet页名字
	private Map<Integer,HashMap<String,String>> xml_mapping_cfg;
	
	public XlsxAbstractUtil(String xlsx_file) throws Exception{
		opcp = OPCPackage.open(xlsx_file);
		reader = new XSSFReader(opcp);
		sst    = reader.getSharedStringsTable();
		parser = XMLReaderFactory.createXMLReader(XML_READER_CLASS);
		parser.setContentHandler(this);
	}
	
	/**
	 * 关闭Excel文件，否则无法删除文件（文件句柄被占用）
	 * */
	public void closeXlsx(){
		try {
			opcp.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 对一行所有单元格的处理逻辑,由子类实现
	 * @param cur_row 当前sheet页中的行数
	 * @param row_map 传给子类处理的结果集
	 * @param xls_mapping_worksheet_name	当前处理的sheet页对应 XLS_MAPPING 中的配置名
	 * */
	protected abstract void processRows(int cur_row,Map<String,String> row_map,String xls_mapping_worksheet_name,String worksheet_name);
	
	/**
	 * 根据xml文件初始化xml配置的三个属性
	 * int header_row = 0;  表头
	 * String xls_mapping_worksheet_name;  当前处理的sheet页对应  XLS_MAPPING 中的配置名
	 * Map<String,Object> xml_mapping_cfg; XLS_MAPPING 中 workbook.worksheet.fields 配置hash
	 *     fields={
	 *             0={col_name=schema_name, comment=, col_type=String}, 
	 *             1={col_name=tab_name,col_type=String}
	 *            }
	 * */
	private void initXmlMapping(String worksheet) throws Exception{
		header_row = 0;
		xls_mapping_worksheet_name = worksheet;
		xml_mapping_cfg = null;
		if(worksheet == null || "".equals(worksheet)){return;}
		SAXReader reader = new SAXReader(false);
		//关闭 dtd 校验,在开发阶段确定xml的合法性
		reader.setValidation(false);
		reader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		Document doc = reader.read(ExcelParser.class.getClassLoader().getResourceAsStream(XLS_MAPPING));
		for(Iterator<Element> it = doc.getRootElement().elementIterator("worksheet");it.hasNext();){
			Element ele_worksheet = it.next();
			if(!ele_worksheet.attributeValue("name").trim().equals(worksheet)){
				continue;
			}
			for(int i=0;i<ele_worksheet.elements().size();i++){
				Element property = ((Element)ele_worksheet.elements().get(i));
				if("header".equalsIgnoreCase(property.getName())){
					header_row = Integer.parseInt(property.attributeValue("rows"));
				}else if("fields".equalsIgnoreCase(property.getName())){
					xml_mapping_cfg = new HashMap<Integer, HashMap<String, String>>();
					for(int j=0;j<property.elements().size();j++){
						Element ele_field = (Element) property.elements().get(j);
						HashMap<String,String> field = new HashMap<String,String>();
						field.put("col_name", ele_field.attributeValue("col_name"));
						field.put("comment", ele_field.attributeValue("comment"));
						//field.put("col_type", ele_field.attributeValue("col_type"));
						field.put("format", ele_field.attributeValue("format"));
						field.put("lettercase", ele_field.attributeValue("lettercase"));
						field.put("chomp", ele_field.attributeValue("chomp"));
						field.put("default", ele_field.attributeValue("default"));
						int col_no = getColIndex(ele_field.attributeValue("col_index"));
						xml_mapping_cfg.put(col_no, field);
					}
				}
			}
		}
		if(xml_mapping_cfg == null){
			throw new Exception(XLS_MAPPING + " 中未找到 " + worksheet + " 的配置");
		}
	}
	
	/**
	 * 根据Excel字段名获得该字段在Excel中列的索引位置
	 * 例如 A->0 B->1 Z->25 AA->26
	 * */
	private static int getColIndex(String col_name){
		int col = -1;
		char[] cs = col_name.trim().toUpperCase().toCharArray();
		for(int i=0;i<cs.length;i++){
			col += (cs[i]-64)*Math.pow(26, cs.length-1-i);
		}
		return col;
	}
	
	/**
	 * 根据索引处理sheet页，索引从1开始
	 * */
	public void processSheet(int sheet_id,String xls_mapping_worksheet_name) throws Exception{
		initXmlMapping(xls_mapping_worksheet_name);
		InputStream sheet = reader.getSheet("rId"+sheet_id);
		List<String> list = getSheetsName();
		for(int i=0;i<list.size();i++){
			if(i+1 == sheet_id){
				worksheet_name = list.get(i);
				break;
			}
		}
		parser.parse(new InputSource(sheet));
		sheet.close();
		cur_row = 0;
		cur_col = 0;
	}
	
	/**
	 * 根据名字处理sheet页
	 * */
	public void processSheet(String sheet_name,String xls_mapping_worksheet_name) throws Exception {
		initXmlMapping(xls_mapping_worksheet_name);
		worksheet_name = sheet_name;
		InputStream sheet = null;
		XSSFReader.SheetIterator it = (XSSFReader.SheetIterator)reader.getSheetsData();
	    while(it.hasNext()) {
	    	InputStream inp = it.next();
	        if(it.getSheetName().equals(sheet_name)){
	        	sheet = inp;
	        	break;
	        }
	    }
	    if(sheet == null){
	    	throw new Exception(sheet_name + "not found");
	    }
	    InputSource is = new InputSource(sheet);
		parser.parse(is);
		sheet.close();
		cur_row = 0;
		cur_col = 0;
	}

	/**
	 * 遍历 xlsx 所有的sheet页并处理
	 */
	public void processAllSheet(String xls_mapping_worksheet_name) throws Exception {
		this.xls_mapping_worksheet_name = xls_mapping_worksheet_name;
		XSSFReader.SheetIterator it = (XSSFReader.SheetIterator)reader.getSheetsData();
		while(it.hasNext()) {
			cur_row = 0;
			cur_col = 0;
			InputStream sheet = it.next();
			worksheet_name = it.getSheetName();
			parser.parse(new InputSource(sheet));
			sheet.close();
		}
	}

	/**
	 * 获得 Xlsx 所有的sheet页名字
	 * */
	public List<String> getSheetsName() throws InvalidFormatException, IOException{
		List<String> names = new ArrayList<String>();
		XSSFReader.SheetIterator it = (XSSFReader.SheetIterator)reader.getSheetsData();
	    while(it.hasNext()) {
	    	it.next();
	        names.add(it.getSheetName());
	    }
	    return names;
	}
	
	public void startElement(String uri, String localName, String name,Attributes attributes) throws SAXException {
		if(name.equals("c")) { //c => 单元格
			String col  = attributes.getValue("r").replaceAll("\\d", ""); //A1 -> A, B2 -> B
			cur_col = getColIndex(col);
			
			date_flag = false;
			string_flag = false;
			time_flag = false;
			timestamp_flag = false;
			
			if("s".equals(attributes.getValue("t"))) {
				string_flag = true;
			}else if("1".equals(attributes.getValue("s"))) {
				date_flag = true;
			}else if("2".equals(attributes.getValue("s"))) {
				time_flag = true;
			}else if("3".equals(attributes.getValue("s"))){
				timestamp_flag = true;
			}
		}
		last_contents = "";
	}

	public void endElement(String uri, String localName, String name) throws SAXException {
		// 根据SST的索引值的到单元格的真正要存储的字符串
		// 这时characters()方法可能会被调用多次
		if(string_flag){
			try {
				int idx = Integer.parseInt(last_contents);
				last_contents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
			} catch (Exception e) {
				
			}
		}

		// v => 单元格的值，如果单元格是字符串则v标签的值为该字符串在SST中的索引
		if(name.equals("v")) {
			String value = last_contents;
			if(date_flag){
				Date date = HSSFDateUtil.getJavaDate(Double.valueOf(value));
				value = sdf.format(date);
			}else if(time_flag){
				Date date = HSSFDateUtil.getJavaDate(Double.valueOf(value));
				value = stf.format(date);
			}else if(timestamp_flag){
				Date date = HSSFDateUtil.getJavaDate(Double.valueOf(value));
				value = sdtf.format(date);
			}
			
			if(xml_mapping_cfg != null){
				Map<String, String> cfg = xml_mapping_cfg.get(cur_col);
				if(cfg != null){
					String col_name = cfg.get("col_name");
					String trim     = cfg.get("trim");
					String chomp    = cfg.get("chomp");
					String default_ = cfg.get("default");
					String lettercase = cfg.get("lettercase");
					
					if(!"false".equalsIgnoreCase(trim)){value=value.trim();}
					if(!"false".equalsIgnoreCase(chomp)){value=value.replaceAll("[\n\r]", ";");}
					if("upper".equalsIgnoreCase(lettercase)){value=value.toUpperCase();}
					if("lower".equalsIgnoreCase(lettercase)){value=value.toLowerCase();}
					if(value.length() == 0 && default_ != null){value = default_;}
					row_map.put(col_name, value);
				}
			}else{
				row_map.put(String.valueOf(cur_col), value);
			}
			if(value.length() > 0){
				empty_row_flag = false;
			}
		}else if(name.equals("row")) {
			//如果标签名称为 row ，这说明已到行尾，调用 processRows() 方法
			if(cur_row >= header_row && !empty_row_flag){
				if(xml_mapping_cfg != null){ 
					//对于已经配置了xls_mapping的字段，如果Excel中没有值则置空字符串
					Iterator it = xml_mapping_cfg.entrySet().iterator();
					while(it.hasNext()){
						Map.Entry<Integer, Map<String,String>> entry = (Map.Entry)it.next();
						String col_name = entry.getValue().get("col_name");
						if(!row_map.containsKey(col_name)){
							row_map.put(col_name, "");
						}
					}
				}
				processRows((cur_row+1),row_map,xls_mapping_worksheet_name,worksheet_name);
			}
			cur_row++;
			row_map.clear();
			empty_row_flag = true;
		}
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		//得到单元格内容的值
		last_contents += new String(ch, start, length);
	}
}
