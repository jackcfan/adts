package adts.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class ExcelParser {
	
	public static HashMap<String, Object> getMapping(String file,String worksheet) throws Exception{
		SAXReader reader = new SAXReader();
		Document doc = reader.read(ExcelParser.class.getClassLoader().getResourceAsStream("excelmapping/"+file));
		HashMap<String, Object> mapping = new HashMap<String, Object>();
		for(Iterator<Element> it = doc.getRootElement().elementIterator("worksheet");it.hasNext();){
			Element e = it.next();
			if(!e.attributeValue("name").equals(worksheet)){
				continue;
			}
			mapping.put("fields", new HashMap<String,String>());
			for(int i=0;i<e.elements().size();i++){
				Element property = ((Element)e.elements().get(i));
				if("header".equalsIgnoreCase(property.getName())){
					mapping.put("header", Integer.parseInt(property.attributeValue("rows")));
					
				}else{
					HashMap<String,HashMap<String,String>> fields = (HashMap<String, HashMap<String, String>>) mapping.get("fields");
					HashMap<String,String> field = new HashMap<String,String>();
					field.put("col_index", property.attributeValue("col_index"));
					field.put("col_name", property.attributeValue("col_name"));
					field.put("comment", property.attributeValue("comment"));
					field.put("col_type", property.attributeValue("col_type"));
					
					fields.put(property.attributeValue("col_name"), field);
				}
			}
		}
		return mapping;
	}
	
	public static List<Map<String,String>> readFieldXLS(File xls,String table_name) throws Exception{
		Workbook workbook = Workbook.getWorkbook(xls);
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		String sheet_name = "字段级分析";
		HashMap<String, Object> mapping = getMapping("field_mapping.xml",sheet_name);
		Sheet sheet = workbook.getSheet(sheet_name);
		int header = (Integer) mapping.get("header");
		int i = 1;
		if(header > 0){
			i = header + 1;
		}
		for(;i<=sheet.getRows();i++){
			String full_tname_en = getCellValue(sheet, mapping, "full_tname_en", i);
			if(!table_name.equalsIgnoreCase(full_tname_en)){
				continue;
			}
			String col_en = getCellValue(sheet, mapping, "col_en", i);
			String col_no = getCellValue(sheet, mapping, "col_no", i);
			String sys_code = getCellValue(sheet, mapping, "sys_code", i);
			String col_cn = getCellValue(sheet, mapping, "col_cn", i);
			String sys_col_type = getCellValue(sheet, mapping, "sys_col_type", i);
			String col_type = getCellValue(sheet, mapping, "col_type", i);
			String null_able = getCellValue(sheet, mapping, "null_able", i);
			String is_dk = getCellValue(sheet, mapping, "is_dk", i);
			if(!"".equals(is_dk)){
				is_dk = "Y";
			}else{
				is_dk = "N";
			}
			HashMap<String,String> column = new HashMap<String,String>();
			column.put("full_tname_en", full_tname_en);
			column.put("col_en", col_en);
			column.put("col_no", col_no);
			column.put("sys_code", sys_code);
			column.put("col_cn", col_cn);
			column.put("sys_col_type", sys_col_type);
			column.put("col_type", col_type);
			column.put("null_able", null_able);
			column.put("is_dk", is_dk);
			list.add(column);
//			System.out.println(full_tname_en);
		}
		workbook.close();
		return list;
	}
	
	private static String getColIndex(HashMap<String, Object> mapping,String col_name){
		Map<String,Object> fields = (Map<String,Object>)mapping.get("fields");
		Map<String,Object> field = (Map<String,Object>)fields.get(col_name);
		return field.get("col_index").toString();
	}
	
	private static String getCellValue(Sheet sheet,HashMap<String, Object> mapping,String col_name,int row){
		String col_index = getColIndex(mapping,col_name);
		return sheet.getCell(col_index+row).getContents().trim();
	}
	
	public static void main(String[] args) throws Exception {
//		System.out.println(getMapping("field_mapping.xml","字段级分析"));
		List<Map<String,String>> list = readFieldXLS(new File("C:\\网上银行系统_fieldTask.xls"),"COR_GDT0PF");
		System.out.println(list);
	}
}
