package adts.util;

import java.util.Map;

public interface POIExcelFilter {
	/**
	 * 退出循环filter
	 * 返回true时退出循环，返回false时继续下一次循环
	 * */
	boolean end();
	
	/**
	 * POIExcelParser 行记录过滤器
	 * accept 返回 true 时，加入该行记录
	 * */
	boolean accept(Map<String,Object> map);
	
}
