package adts.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface BaseDao {

	public Object selectOne(String id, Object para) throws SQLException;

	public List<Map<String, Object>> select(String id,Object para) throws SQLException;

	public int insert(String id, Object para) throws SQLException;

	public int update(String id, Object para) throws SQLException;
	
	public int delete(String id, Object para) throws SQLException;

	public int selectIntVal(String id, Object para) throws SQLException;

	public void beginTransaction();

	public void commitTransaction();

	public void rollbackTransaction();
	
	public Connection getConnection();
	
	public void setNamespace(String namespace);
}