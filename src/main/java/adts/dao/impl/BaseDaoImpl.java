package adts.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import adts.dao.BaseDao;

/**
 * BaseDao 的通用实现
 * 通过注入不同的数据库 session 和 transactionManager 实现跨数据库操作
 * */
public class BaseDaoImpl implements BaseDao {

	/**
	 * SqlSession 和 DataSourceTransactionManager 使用 singleton 注入
	 * */
	private SqlSession session;
	private DataSourceTransactionManager transactionManager;
	
	/**
	 * 保存当前事务状态,注入 BaseDaoImpl 时请使用 prototype
	 * 如果注入 singleton 的 dao 会存在线程安全问题
	 * */
	private TransactionStatus status;
	
	private static Logger logger = Logger.getLogger(BaseDaoImpl.class);
	
	private static DefaultTransactionDefinition def = new DefaultTransactionDefinition();
	
	static{
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
	}
	
	@Override
	public Object selectOne(String id, Object para) throws SQLException {
		try{
			if(para != null && para instanceof Map && ((Map<String, Object>) para).containsKey("_MAPPER_NAMESPACE_")){
				id = ((Map<String, Object>) para).get("_MAPPER_NAMESPACE_").toString()+"."+id;
			}
			return session.selectOne(id, para);
		}catch(Exception e){
			throw new SQLException(e.getMessage());
		}
	}

	@Override
	public List<Map<String, Object>> select(String id, Object para)
			throws SQLException {
		try{
			if(para != null && para instanceof Map && ((Map<String, Object>) para).containsKey("_MAPPER_NAMESPACE_")){
				id = ((Map<String, Object>) para).get("_MAPPER_NAMESPACE_").toString()+"."+id;
			}
			return session.selectList(id, para);
		}catch(Exception e){
			throw new SQLException(e.getMessage());
		}
	}

	@Override
	public int insert(String id, Object para) throws SQLException {
		try{
			if(para != null && para instanceof Map && ((Map<String, Object>) para).containsKey("_MAPPER_NAMESPACE_")){
				id = ((Map<String, Object>) para).get("_MAPPER_NAMESPACE_").toString()+"."+id;
			}
			return session.insert(id, para);
		}catch(Exception e){
			throw new SQLException(e.getMessage());
		}
	}

	@Override
	public int update(String id, Object para) throws SQLException {
		try{
			if(para != null && para instanceof Map && ((Map<String, Object>) para).containsKey("_MAPPER_NAMESPACE_")){
				id = ((Map<String, Object>) para).get("_MAPPER_NAMESPACE_").toString()+"."+id;
			}
			return session.update(id, para);
		}catch(Exception e){
			throw new SQLException(e.getMessage());
		}
	}

	@Override
	public int delete(String id, Object para) throws SQLException {
		try{
			if(para != null && para instanceof Map && ((Map<String, Object>) para).containsKey("_MAPPER_NAMESPACE_")){
				id = ((Map<String, Object>) para).get("_MAPPER_NAMESPACE_").toString()+"."+id;
			}
			return session.delete(id, para);
		}catch(Exception e){
			throw new SQLException(e.getMessage());
		}
	}
	
	@Override
	public int selectIntVal(String id, Object para) throws SQLException {
		try{
			Object obj = selectOne(id, para);
			return (Integer)obj;
		}catch(Exception e){
			throw new SQLException(e.getMessage());
		}
	}

	@Override
	public void beginTransaction() {
		status = transactionManager.getTransaction(def);
	}

	@Override
	public void commitTransaction() {
//		UnsupportedOperationException: Manual commit is not allowed over a Spring managed SqlSession
//		session.commit();
		if(status != null){
			transactionManager.commit(status);
		}else{
			logger.warn("未开启事务,不做处理");
		}
	}

	@Override
	public void rollbackTransaction() {
		if(status != null){
			transactionManager.rollback(status);
		}else{
			logger.warn("未开启事务,不做处理");
		}
	}

	public SqlSession getSession() {
		return session;
	}

	public void setSession(SqlSession session) {
		this.session = session;
	}

	public DataSourceTransactionManager getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(
			DataSourceTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	@Override
	public Connection getConnection() {
		try {
			return session.getConfiguration().getEnvironment().getDataSource().getConnection();
		} catch (SQLException e) {
			logger.error("cannot get connection:"+e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void setNamespace(String namespace) {
		// TODO Auto-generated method stub
		
	}

}
