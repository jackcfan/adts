package adts.actions;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import adts.model.User;
import adts.util.Util;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class BasicAction extends ActionSupport{
	
	public HttpSession session = ServletActionContext.getRequest().getSession();
	
	HttpServletResponse response;
	
	ServletContext servletContext;
	
	protected User user = (User) session.getAttribute("ADTS_USER");
	
//	存储页面参数Hash，通过initParaMap初始化一些共用参数，例如用户名
	public Map<String,Object> paraMap;
	
	public Map<String,Object> result;

	public static final String END_DATE = "2999-12-31";
	
	public String updateVersion(){
		Util.updateVersion();
		result = new HashMap<String, Object>();
		result.put("result", 1);
		result.put("version", Util.getVersion());
		return SUCCESS;
	}

//	public Map<String, Object> getResult() {
//		return result;
//	}
//
//	public void setResult(Map<String, Object> result) {
//		this.result = result;
//	}

	public HttpServletResponse getResponse() {
		return ServletActionContext.getResponse();
	}

	public ServletContext getServletContext() {
		return (ServletContext)ActionContext.getContext().get(ServletActionContext.SERVLET_CONTEXT);
	}
	
	public Map<String, Object> getParaMap() {
		return paraMap;
	}

	public void initParaMap(){
		if(paraMap == null){
			paraMap = new HashMap<String, Object>();
		}
//		if(user != null){
//			paraMap.put("ADTS_USER_NAME", user.getUsername());
//			paraMap.put("ADTS_USER_ID", user.getId());
//		}
	}
	
	public void initParaMap(String mapper_namespace){
		if(paraMap == null){
			paraMap = new HashMap<String, Object>();
		}
		if(mapper_namespace != null){
			paraMap.put("_MAPPER_NAMESPACE_", mapper_namespace);
		}
//		if(user != null){
//			paraMap.put("EDW_USER_NAME", user.getUsername());
//			paraMap.put("EDW_USER_ID", user.getId());
//		}
	}
}
