package adts.actions;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import adts.dao.BaseDao;
import adts.model.User;
import adts.util.Util;

@SuppressWarnings("serial")
public class LoginAction extends BasicAction {
	private int id;
	private String username;
	private String password;
	private String realname;
	private String auto_login;
	private String pre_page;
	
	private BaseDao baseDao;
	
	private Map<String,Object> result;
	private static final String mapper_namespace = "adts_auth";
	
	public String login() throws Exception {
		initParaMap(mapper_namespace);
		result = new HashMap<String,Object>();
		result.put("success", false);
		if(user != null){
			result.put("success", true);
			return SUCCESS;
		}
		pre_page = (String) session.getAttribute("pre_page");
		session.removeAttribute("pre_page");
		paraMap = new HashMap<String,Object>();
		paraMap.put("username", username);
		paraMap.put("password", Util.getMD5(password));
//		user = (User) baseDao.selectOne("getUserByUsernamePwd", paraMap);
		
		Map<String,Object> user = (Map<String, Object>) baseDao.selectOne("getUserByUsernamePwd", paraMap);
		if(user == null){
			result.put("msg", "用户名或密码错误");
			return SUCCESS;
		}else if((Integer)user.get("state") == User.STATE_FORBIDDEN){
			result.put("msg", "用户被禁用");
			return SUCCESS;
		}else if((Integer)user.get("state") == User.STATE_PWD_EXPIRED){
			result.put("msg", "用户密码到期");
			return SUCCESS;
		}else if((Integer)user.get("state") == User.STATE_NORMAL){
			session.setAttribute("ADTS_USER", new User(user));
			paraMap.put("id", user.get("id"));
			if("on".equalsIgnoreCase(auto_login)){	//自动登录时设置客户端的cookie:account、token
				user.put("token", Util.getUUID());
				paraMap.put("token", user.get("token"));
				baseDao.update("updateUserByIdWithToken", paraMap);
				Cookie acc = new Cookie("username", username);
				acc.setMaxAge(2592000);//30days
				acc.setPath("/");
				
				Cookie token = new Cookie("token", user.get("token").toString());
				token.setMaxAge(2592000);//30days
				token.setPath("/");
				
				Cookie a_login = new Cookie("auto_login", "on");
				a_login.setMaxAge(2592000);//30days
				a_login.setPath("/");
				
				ServletActionContext.getResponse().addCookie(acc);
				ServletActionContext.getResponse().addCookie(token);
				ServletActionContext.getResponse().addCookie(a_login);
			}else{
				user.put("token", "");
				paraMap.put("token", "");
				baseDao.update("updateUserByIdWithToken", paraMap);
				Cookie acc = new Cookie("account", "");
				acc.setMaxAge(0);//delete
				acc.setPath("/");
				
				Cookie token = new Cookie("token", "");
				token.setMaxAge(0);//delete
				
				Cookie a_login = new Cookie("auto_login", "");
				a_login.setMaxAge(0);//delete
				a_login.setPath("/");
				
				ServletActionContext.getResponse().addCookie(acc);
				ServletActionContext.getResponse().addCookie(token);
				ServletActionContext.getResponse().addCookie(a_login);
			}
			result.put("success", true);
			return SUCCESS;
		}else{
			result.put("msg", "无效的用户状态");
			result.put("result", "0");
			result.put("success", false);
			return SUCCESS;
		}
	}
	
	public String logout() throws SQLException {
		initParaMap(mapper_namespace);
		HttpSession session = ServletActionContext.getRequest().getSession();
		session.invalidate();
		if(user != null){
			user.setToken(null);
			paraMap.put("id", user.getId());
			paraMap.put("token", null);
			baseDao.update("updateUserByIdWithToken", paraMap);
		}
		Cookie acc = new Cookie("account", "");
		acc.setPath("/");
		acc.setMaxAge(0);//delete
		Cookie token = new Cookie("token", "");
		token.setPath("/");
		token.setMaxAge(0);//delete
		Cookie a_login = new Cookie("auto_login", "");
		a_login.setMaxAge(0);//delete
		a_login.setPath("/");
		ServletActionContext.getResponse().addCookie(acc);
		ServletActionContext.getResponse().addCookie(token);
		ServletActionContext.getResponse().addCookie(a_login);
		return "logout";
	}

	public String changePwd() {
		result = new HashMap<String,Object>();
		if(user == null){
			result.put("result", 0);
			result.put("msg", "用户未登陆");
			return "success";
		}
		if(password == null || password.trim().length() == 0){
			result.put("result", 0);
			result.put("msg", "密码不能为空");
			return "success";
		}
		user.setPassword(password);
		try {
			baseDao.update("updateUserPwd", user);
		} catch (SQLException e) {
			result.put("result", 0);
			result.put("msg", "更改密码失败:"+e.getMessage());
			e.printStackTrace();
		}
		result.put("result", 1);
		return SUCCESS;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getAuto_login() {
		return auto_login;
	}

	public void setAuto_login(String auto_login) {
		this.auto_login = auto_login;
	}

	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}
	
	
}
