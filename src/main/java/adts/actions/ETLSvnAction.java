package adts.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tmatesoft.svn.core.SVNException;

import adts.dao.BaseDao;
import adts.util.SVNKitUtil;

public class ETLSvnAction extends BasicAction {
	private String env;
	private String etl_system;
	
	private BaseDao baseDao;
	private SVNKitUtil etl_svn;
	private List list;	//Ajax 返回
	private Map<String,Object> result;		//Ajax 返回
	
	public String doSummarizeDiff() throws SVNException{
		list = etl_svn.doSummarizeDiff("/tags/ETL/"+env+"/APP/"+etl_system,"/trunk/ETL/APP/"+etl_system,"file");
		return SUCCESS;
	}

	public String getEnvList() throws SVNException{
		list = etl_svn.getFileList("/tags/ETL");
		return SUCCESS;
	}
	
	public String getAppList() throws SVNException{
		list = etl_svn.getFileList("/trunk/ETL/APP");
		return SUCCESS;
	}
	
	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getEtl_system() {
		return etl_system;
	}

	public void setEtl_system(String etl_system) {
		this.etl_system = etl_system;
	}

	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public SVNKitUtil getEtl_svn() {
		return etl_svn;
	}

	public void setEtl_svn(SVNKitUtil etl_svn) {
		this.etl_svn = etl_svn;
	}

	public List<Map<String, Object>> getList() {
		return list;
	}

	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}
	
	
}
