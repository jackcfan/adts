package adts.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.tmatesoft.svn.core.SVNException;

import adts.dao.BaseDao;
import adts.util.SVNKitUtil;
import adts.util.Util;
import adts.util.XlsxAbstractUtil;
import adts.util.XlsxCommonUtil;
import adts.util.ZipCompressor;

@SuppressWarnings("serial")
public class VersionControlAction extends BasicAction {
	private static final String mapper_namespace = "adts_version";
	private BaseDao baseDao;
	private SVNKitUtil etl_svn;
	private List<Map<String,Object>> list;
	private Map<String,Object> map;
	private String version;

	private String url = "/trunk/ETL/init/update";
	Logger logger = Logger.getLogger(VersionControlAction.class);

	public String getEtlUpdateList(){
		try{
			list = etl_svn.getFileList(url);
			for(int i = list.size() - 1;i>=0;i--){
				Map<String,Object> map = list.get(i);
				if(!map.get("name").toString().startsWith("v") && !"dir".equals(map.get("kind")) ){
					list.remove(i);
				}
			}
		}catch(SVNException e){
			logger.error(e.getMessage());
		}
		return SUCCESS;
	}
	
	public String downloadPackage() throws Exception{
		String svn_xlsx    = "/trunk/ETL/init/update/prd_update_list.xlsx";
		String svn_ver_dir = "/trunk/ETL/init/update/" + version;
		String uuid        = Util.getUUID();
		String work_home   = System.getenv("HOME")+"/adts_tmp/"+uuid + "/PKG";
		
		String zip_file       = work_home + "/" + version + ".zip";
		File xlsx_file = null;
		XlsxCommonUtil xlsx_util = null;
		try{
			if(version == null || version.length() == 0){
				throw new Exception("version not input");
			}
			File work_home_ver = new File(work_home + "/ETL/init/update/" + version);
			work_home_ver.mkdirs();
			//导出该版本下默认需要导出的文件
			etl_svn.export(svn_ver_dir, work_home_ver);
			
			xlsx_file = new File(System.getenv("tmp")+"/adts/"+uuid + "/prd_update_list.xlsx");
			//导出上线列表excel
			etl_svn.export(svn_xlsx, xlsx_file);
			
			//生成上线脚本列表 scripts_update.conf
			xlsx_util = new XlsxCommonUtil(xlsx_file);
			List<Map<String,String>> rows = xlsx_util.readSheet("脚本部署", "ETL_Script_Update");
			File script_update = new File(work_home_ver+"/scripts_update.conf");
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(script_update),"GBK");
			writer.write("##################################################\n");
			writer.write("#所有的脚本路径从AUTO_HOME下开始\n");
			writer.write("#比如 APP/FLD/S_EUS_AQZHD\n");
			writer.write("#一行一条记录，可以是文件或目录，行首用#号表示注释\n");
			writer.write("##################################################\n\n");
			for(Map<String,String> map : rows){
				String ver = map.get("version");
				if(!ver.equalsIgnoreCase(version)){
					continue;
				}
				writer.write("#"+map.get("content")+"("+map.get("owner")+")\n");
				writer.write(map.get("file")+"\n\n");
				etl_svn.export("/trunk/ETL/"+map.get("file"), new File(work_home+"/ETL/"+map.get("file")));
			}
			writer.close();
			
			//生成DPF模型部署列表 update_dpf_ecif.conf
			boolean empty = true;    //文件为空标志
			rows = xlsx_util.readSheet("后端模型", "ETL_Model_Update");
			File model_update = new File(work_home_ver+"/update_dpf_ecif.conf");
			writer = new OutputStreamWriter(new FileOutputStream(model_update),"GBK");
			for(Map<String,String> map : rows){
				String ver = map.get("version");
				if(!ver.equalsIgnoreCase(version) ||"N".equalsIgnoreCase(map.get("auto_dep"))){
					continue;
				}
				empty = false;
				writer.write(map.get("file")+"\n");
			}
			writer.close();
			if(empty){
				model_update.delete();
			}
			
			//生成前端模型部署列表 update_db2_ecif.conf
			empty = true;    //文件为空标志
			rows = xlsx_util.readSheet("前端模型", "ETL_Model_Update");
			model_update = new File(work_home_ver+"/update_db2_ecif.conf");
			writer = new OutputStreamWriter(new FileOutputStream(model_update),"GBK");
			for(Map<String,String> map : rows){
				String ver = map.get("version");
				if(!ver.equalsIgnoreCase(version) ||"N".equalsIgnoreCase(map.get("auto_dep"))){
					continue;
				}
				empty = false;
				writer.write(map.get("file")+"\n");
			}
			writer.close();
			if(empty){
				model_update.delete();
			}
			
			//生成回单模型部署列表 update_db2_ivc.conf
			empty = true;    //文件为空标志
			rows = xlsx_util.readSheet("BRMS模型", "ETL_Model_Update");
			model_update = new File(work_home_ver+"/update_db2_ivc.conf");
			writer = new OutputStreamWriter(new FileOutputStream(model_update),"GBK");
			for(Map<String,String> map : rows){
				String ver = map.get("version");
				if(!ver.equalsIgnoreCase(version) ||"N".equalsIgnoreCase(map.get("auto_dep"))){
					continue;
				}
				empty = false;
				writer.write(map.get("file")+"\n");
			}
			writer.close();
			if(empty){
				model_update.delete();
			}
			
			//导出所有文件后打zip包
			if(ZipCompressor.zipDir(work_home,zip_file) != 1){
				throw new Exception("打包失败");
			}
			
			//下载
			response = ServletActionContext.getResponse();
			ServletOutputStream output = response.getOutputStream();
			FileInputStream fis = new FileInputStream(new File(zip_file));
			
			response.setContentType("application/octet-stream;charset=ISO8859-1");
			response.setHeader("Content-Disposition", "attachment;filename="+new String((version + ".zip").getBytes(),"ISO8859-1"));
			
			byte buff[] = new byte[1024 * 8];
			while(fis.read(buff) > 0){
				output.write(buff);
			}
			fis.close();
			output.close();
			/*避免报错  getOutputStream() has already been called for this response */
//			out.clear();
//			out = pageContext.pushBody();
		}catch(Exception e){
			e.printStackTrace();
			response = ServletActionContext.getResponse();
			response.setCharacterEncoding("utf-8");
			PrintWriter writer = response.getWriter();
			writer.write(e.getMessage());
		}finally{
			if(xlsx_util != null){
				xlsx_util.closeXlsx();
				xlsx_file.delete();
			}
			FileUtils.deleteDirectory(new File(System.getenv("tmp")+"/adts/"+uuid));
		}
		return null;
	}
	
	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public SVNKitUtil getEtl_svn() {
		return etl_svn;
	}

	public void setEtl_svn(SVNKitUtil etl_svn) {
		this.etl_svn = etl_svn;
	}

	public List<Map<String, Object>> getList() {
		return list;
	}

	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	
}
