package adts.actions;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.tmatesoft.svn.core.SVNException;
import org.xml.sax.SAXException;

import adts.dao.BaseDao;
import adts.util.DDLUtilInterface;
import adts.util.DDLgenUtil;
import adts.util.Db2DDLUtil;
import adts.util.DpfDDLUtil;
import adts.util.IniUtil;
import adts.util.SVNKitUtil;
import adts.util.Util;
import adts.util.XlsxAbstractUtil;
import adts.util.XlsxCommonUtil;

public class ModelAction extends BasicAction {
	private static final String mapper_namespace = "adts_model";
	private BaseDao baseDao;
	private SVNKitUtil doc_svn;
	private List<Map<String,Object>> list;	//Ajax 返回
	private Map<String,Object> result;		//Ajax 返回
	private String tag;	//区分不同的库,例如 DPF,DB2_ECIF,DB2_BRMS
	
//	private static String model_file  = IniUtil.getValByKey("DOC_SVN_CFG", "dpf_dict_file");
	
	Logger logger = Logger.getLogger(ModelAction.class);
	
	private Integer revision;
	private String schema_name;
	private String tab_name;
	private String env_name;
	private String table_list;
	
	private int start; 		 // 开始条数，从0开始
	private int page;          //页数
	private int limit; 	    // 每页条数
	
	private InputStream input_stream;
	
	public String getSvnHistory() throws SVNException, SQLException{
		initParaMap(mapper_namespace);
		//tag和数据字典文件配置关系 tag.toLowerCase()+"_dict_file"
		String model_file  = IniUtil.getValByKey("DOC_SVN_CFG", tag.toLowerCase()+"_dict_file");
		paraMap.put("tag", tag);
		list = (List)doc_svn.getSvnLog(model_file);
		List<Map<String, Object>> hisory = baseDao.select("get_model_his", paraMap);
		for(Map<String,Object> map : list){
			map.put("imported", "N");
			int revision = Integer.valueOf(map.get("revision").toString());
			for(Map<String,Object> his_map : hisory){
				int his_revision = Integer.valueOf(his_map.get("revision").toString());
				if(revision == his_revision){
					map.put("import_stat", his_map.get("import_stat"));
					if("Success".equals(his_map.get("import_stat"))){
						map.put("imported", "Y");
					}else{
						map.put("imported", "F");
					}
					map.put("import_date", his_map.get("import_date"));
					map.put("import_err", his_map.get("import_err"));
					break;
				}
			}
		}
		return SUCCESS;
	}
	
	public String getTableList(){
		initParaMap(mapper_namespace);
		paraMap.put("tag", tag);
		paraMap.put("start", (page-1)*limit);
		paraMap.put("limit", limit);
		paraMap.put("schema_name", schema_name);
		paraMap.put("tab_name", tab_name);
		paraMap.put("env_name", env_name);
		result = new HashMap<String,Object>();
		try {
			if(revision == null || revision < 0){
				revision = baseDao.selectIntVal("getMaxRevision", revision);
			}
			paraMap.put("revision", revision);
			int count = baseDao.selectIntVal("getTableListCount", paraMap);
			list = baseDao.select("getTableList", paraMap);
			result.put("count", count);
			result.put("rows", list);
		} catch (SQLException e) {
			result.put("count", 0);
			list = new ArrayList<Map<String,Object>>();
			result.put("rows", list);
			logger.error(e.getMessage());
		}
		return SUCCESS;
	}
	
	public String getRevisionList(){
		initParaMap(mapper_namespace);
		paraMap.put("tag", tag);
		try {
			list = baseDao.select("getRevisionList", paraMap);
		} catch (SQLException e) {
			list = new ArrayList<Map<String,Object>>();
		}
		return SUCCESS;
	}
	
	public String getEnvListByRevision(){
		initParaMap(mapper_namespace);
		paraMap.put("tag", tag);
		paraMap.put("revision", revision);
		try {
			list = baseDao.select("getEnvListByRevision", paraMap);
		} catch (SQLException e) {
			list = new ArrayList<Map<String,Object>>();
		}
		return SUCCESS;
	}
	
	public String getSchemaList(){
		initParaMap(mapper_namespace);
		paraMap.put("tag", tag);
		try {
			list = baseDao.select("getSchemaList", paraMap);
		} catch (SQLException e) {
			list = new ArrayList<Map<String,Object>>();
		}
		return SUCCESS;
	}
	
	public String getColumnList(){
		initParaMap(mapper_namespace);
		paraMap.put("schema_name", schema_name);
		paraMap.put("tab_name", tab_name);
		paraMap.put("tag", tag);
		try {
			list = baseDao.select("getColumnList", paraMap);
		} catch (SQLException e) {
			list = new ArrayList<Map<String,Object>>();
		}
		return SUCCESS;
	}
	
	public String getDDLList(){
		result = getDDLListInfo(table_list,baseDao,mapper_namespace);
		return SUCCESS;
	}
	
	public String getDDLListFile() throws IOException{
		result = getDDLListInfo(table_list,baseDao,mapper_namespace);
		HttpServletResponse response = getResponse();
		OutputStream out = response.getOutputStream();
		String attachmentName = "dpf_" + revision + "_" + env_name+".sql";
		String ddl = result.get("ddl").toString();
        out.write(ddl.getBytes());
        response.setContentType("application/octet-stream;charset=ISO8859-1");
        response.setHeader("Content-Disposition", "attachment;filename="+new String(attachmentName.getBytes(),"ISO8859-1"));
        out.close();
        
        ByteArrayInputStream is = new ByteArrayInputStream(ddl.getBytes());
        setInput_stream(is);
        
		return SUCCESS;
	}
	
	
	private Map<String,Object> getDDLListInfo(String table_list,BaseDao baseDao,String mapper_namespace){
		String[] tab = table_list.toUpperCase().split(",|\n");
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			DDLUtilInterface ddlUtil = null;
			if("DPF".equalsIgnoreCase(tag)){
				ddlUtil = new DpfDDLUtil(baseDao, revision, env_name,"DPF");
			}else if("DB2_ECIF".equalsIgnoreCase(tag)){
				ddlUtil = new Db2DDLUtil(baseDao, revision, env_name,"DB2_ECIF");
			}else{
				result.put("ddl", "未实现的tag");
				return result;
			}
			String ddl = ddlUtil.genDDL(tab);
			result.put("ddl", ddl);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("ddl", e.getMessage());
		}
		return result;
	}
	
	/**
	 * result 0 导入失败  1导入成功 2警告
	 * */
	public String importSvnModel() throws SVNException, SQLException{
		initParaMap(mapper_namespace);
		result = new HashMap<String,Object>();
		if(revision == null || revision < 0){
			result.put("result", 2);
			result.put("msg", "请输入修订号");
			return SUCCESS;
		}
		paraMap.put("revision", revision);
		paraMap.put("tag", tag);
		File export_file = new File(System.getenv("tmp")+"/"+Util.getUUID()+"_dpf_model.xlsx");
		//tag和数据字典文件配置关系 tag.toLowerCase()+"_dict_file"
		String model_file  = IniUtil.getValByKey("DOC_SVN_CFG", tag.toLowerCase()+"_dict_file");
		List<Map<String, String>> log = doc_svn.getSvnLog(model_file, revision, revision);
		ModelXlsxParser xlsx = null;
		try{
			Map<String,Object> his = (Map<String, Object>) baseDao.selectOne("get_model_his", paraMap);
			if(his != null && his.containsKey("import_stat")
					&& "Failed".equalsIgnoreCase(his.get("import_stat").toString())){
				//对于导入失败的记录不再重复导入
				logger.info("版本号 "+ revision +" 历史导入失败，不再导入");
				result.put("result", 0);
				result.put("msg", "版本号 "+ revision +" 历史导入失败，不再导入");
				return SUCCESS;
			}else if(his != null && his.containsKey("import_stat")
					&& "Success".equalsIgnoreCase(his.get("import_stat").toString())){
				//对于导入成功的记录不再重复导入
				logger.info("版本号 "+ revision +" 历史已经导入，不再重复导入");
				result.put("result", 2);
				result.put("msg", "版本号 "+ revision +" 历史已经导入，不再重复导入");
				return SUCCESS;
			}
			doc_svn.export(model_file, export_file, revision);
			baseDao.beginTransaction();
			baseDao.delete("delete_model_table", paraMap);
			baseDao.delete("delete_model_column", paraMap);
			baseDao.delete("delete_model_tabspace", paraMap);
			baseDao.delete("delete_model_keywords", paraMap);
			baseDao.delete("delete_model_user_group", paraMap);
			
/*			xlsx = new XlsxCommonUtil(export_file.getAbsolutePath());
			for(String sheet : xlsx.getSheetsName()){
				if(sheet.endsWith("表级")){
					processSheet(xlsx,sheet,tag+"_Model_Table","insert_model_table");
				}else if(sheet.endsWith("字段级")){
					processSheet(xlsx,sheet,tag+"_Model_Column","insert_model_column");
				}else if(sheet.endsWith("表空间列表")){
					processSheet(xlsx,sheet,"Model_Tabspace","insert_model_tabspace");
				}else if("关键字列表".equals(sheet)){
					processSheet(xlsx,sheet,"Model_Keywords","insert_model_keywords");
				}else if("用户组列表".equals(sheet)){
					processSheet(xlsx,sheet,"Model_User_Group","insert_model_user_group");
				}
			}*/
			
			xlsx = new ModelXlsxParser(export_file.getAbsolutePath(),baseDao,revision);
			for(String sheet : xlsx.getSheetsName()){
				sheet = sheet.trim();
				if(sheet.endsWith("表级")){
					logger.info("process " + sheet);
					xlsx.processSheet(sheet, tag+"_Model_Table");
				}else if(sheet.endsWith("字段级")){
					logger.info("process " + sheet);
					xlsx.processSheet(sheet, tag+"_Model_Column");
				}else if(sheet.endsWith("表空间列表")){
					logger.info("process " + sheet);
					xlsx.processSheet(sheet, "Model_Tabspace");
				}else if("关键字列表".equals(sheet)){
					logger.info("process " + sheet);
					xlsx.processSheet(sheet, "Model_Keywords");
				}else if("用户组列表".equals(sheet)){
					logger.info("process " + sheet);
					xlsx.processSheet(sheet, "Model_User_Group");
				}
			}
			
			paraMap.put("svn_author", log.get(0).get("author"));
			paraMap.put("commit_date", log.get(0).get("date"));
			paraMap.put("message", log.get(0).get("message"));
			paraMap.put("import_date", log.get(0).get("import_date"));
			paraMap.put("import_stat", "Success");
			logger.info("process model_his");
			baseDao.insert("insert_model_his", paraMap);
			
			Map<String,Integer> top = (Map<String, Integer>) baseDao.selectOne("get_model_his_top10_revision", paraMap);
			if(top != null && top.get("cnt") >= 10){
				logger.info("模型清理");
				int min_revision = top.get("min");
				paraMap.put("min_revision", min_revision);
				baseDao.delete("deleteTop10", paraMap);
			}
			
			baseDao.commitTransaction();
			logger.info("导入成功");
			result.put("result", 1);
		}catch(Exception e){
			logger.info("导入失败");
			baseDao.rollbackTransaction();
			paraMap.put("svn_author", log.get(0).get("author"));
			paraMap.put("commit_date", log.get(0).get("date"));
			paraMap.put("message", log.get(0).get("message"));
			paraMap.put("import_date", log.get(0).get("import_date"));
			paraMap.put("import_stat", "Failed");
			paraMap.put("import_err", e.getMessage());
			baseDao.insert("insert_model_his", paraMap);
			logger.error(e.getMessage());
			e.printStackTrace();
			result.put("result", 2);
			result.put("msg", e.getMessage());
		}finally{
			if(xlsx != null){
				xlsx.closeXlsx();
			}
			export_file.delete();
		}
		return SUCCESS;
	}
	
	private void processSheet(XlsxCommonUtil xlsx,String sheet,String xls_mapping_worksheet_name,String sql_id) throws Exception{
		logger.info("process " + sheet);
		List<Map<String,String>> rows = xlsx.readSheet(sheet, xls_mapping_worksheet_name);
		for(Map<String,String> map : rows){
			map.put("tag", tag);
			map.put("_MAPPER_NAMESPACE_", mapper_namespace);
			map.put("revision", String.valueOf(revision));
			try{
				baseDao.insert(sql_id, map);
			}catch(Exception e){
				throw new RuntimeException("导入失败:"+sheet+" 第 " + map.get("__ROW_NO__") +" 行\n" + e.getMessage());
			}
		}
	}
	
	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public SVNKitUtil getDoc_svn() {
		return doc_svn;
	}

	public void setDoc_svn(SVNKitUtil doc_svn) {
		this.doc_svn = doc_svn;
	}

	public List<Map<String, Object>> getList() {
		return list;
	}

	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}

	public Integer getRevision() {
		return revision;
	}

	public void setRevision(Integer revision) {
		this.revision = revision;
	}

	class ModelXlsxParser extends XlsxAbstractUtil{

		private BaseDao dao;
		private int revision;
		public ModelXlsxParser(String filename,BaseDao dao,int revision) throws Exception,
				IOException, OpenXML4JException, SAXException {
			super(filename);
			this.dao = dao;
			this.revision = revision;
		}
		public ModelXlsxParser(String filename) throws Exception,
				IOException, OpenXML4JException, SAXException {
			super(filename);
		}

		@Override
		protected void processRows(int cur_row, Map<String, String> row_map,
				String xml_mapping_worksheet_name,String worksheet_name) {
			row_map.put("_MAPPER_NAMESPACE_", mapper_namespace);
			row_map.put("revision", String.valueOf(revision));
			row_map.put("tag", tag);
			try {
				if((tag+"_Model_Table").equals(xml_mapping_worksheet_name)){
					dao.insert("insert_model_table", row_map);
				}else if((tag+"_Model_Column").equals(xml_mapping_worksheet_name)){
					dao.insert("insert_model_column", row_map);
				}else if("Model_Tabspace".equals(xml_mapping_worksheet_name)){
					dao.insert("insert_model_tabspace", row_map);
				}else if("Model_Keywords".equals(xml_mapping_worksheet_name)){
					dao.insert("insert_model_keywords", row_map);
				}else if("Model_User_Group".equals(xml_mapping_worksheet_name)){
					dao.insert("insert_model_user_group", row_map);
				}
			} catch (SQLException e) {
				logger.error("insert error in sheet " + xml_mapping_worksheet_name + " at row " + cur_row);
				logger.error(row_map);
				throw new RuntimeException("导入失败:"+worksheet_name+" 第 " + cur_row +" 行\n" + e.getMessage());
			}
			
		}
		
	}

	public String getSchema_name() {
		return schema_name;
	}

	public void setSchema_name(String schema_name) {
		this.schema_name = schema_name;
	}

	public String getTab_name() {
		return tab_name;
	}

	public void setTab_name(String tab_name) {
		this.tab_name = tab_name;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}

	public String getTable_list() {
		return table_list;
	}

	public void setTable_list(String table_list) {
		this.table_list = table_list;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getEnv_name() {
		return env_name;
	}

	public void setEnv_name(String env_name) {
		this.env_name = env_name;
	}

	public InputStream getInput_stream() {
		return input_stream;
	}

	public void setInput_stream(InputStream input_stream) {
		this.input_stream = input_stream;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
