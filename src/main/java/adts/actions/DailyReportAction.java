package adts.actions;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import adts.dao.BaseDao;
import adts.util.AuthMenuUtil;
import adts.util.IniUtil;
import adts.util.Util;

public class DailyReportAction extends BasicAction {
	private static final String mapper_namespace = "adts_manage";
	private BaseDao baseDao;
	
	private List<Map<String,Object>> list;	//Ajax 返回
	private Map<String,Object> result;		//Ajax 返回
	
	private Integer id;
	private Integer parent_id;
	private String begin_date;
	private String state;
	private String work_dtl;
	private String remark;
	private String priority;
	private String work_type;
	private String component;
	
	public String getDailyReport() throws SQLException{
		initParaMap(mapper_namespace);
		paraMap.put("user_id", user.getId());
		list = baseDao.select("getDailyReport", paraMap);
		return SUCCESS;
	}
	
	public String updateDailyReport(){
		initParaMap(mapper_namespace);
		result = new HashMap<String,Object>();
		paraMap.put("id", id);
		paraMap.put("user_id", user.getId());
		paraMap.put("begin_date", begin_date);
		paraMap.put("state", state);
		paraMap.put("work_dtl", work_dtl);
		paraMap.put("remark", remark);
		paraMap.put("priority", priority);
		paraMap.put("work_type", work_type);
		paraMap.put("component", component);
		try {
			baseDao.insert("updateDailyReport", paraMap);
			result.put("success", "1");
		} catch (SQLException e) {
			result.put("success", "0");
			result.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return  SUCCESS;
	}
	
	public String insertDailyReport(){
		initParaMap(mapper_namespace);
		result = new HashMap<String,Object>();
		paraMap.put("user_id", user.getId());
		paraMap.put("parent_id", parent_id);
		paraMap.put("begin_date", begin_date);
		paraMap.put("state", state);
		paraMap.put("work_dtl", work_dtl);
		paraMap.put("remark", remark);
		paraMap.put("priority", priority);
		paraMap.put("work_type", work_type);
		paraMap.put("component", component);
		try {
			baseDao.insert("insertDailyReport", paraMap);
			result.put("success", "1");
		} catch (SQLException e) {
			result.put("success", "0");
			result.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return  SUCCESS;
	}
	
	public String deleteDailyReport(){
		initParaMap(mapper_namespace);
		result = new HashMap<String,Object>();
		paraMap.put("user_id", user.getId());
		paraMap.put("id", id);
		try {
			baseDao.delete("deleteDailyReport", paraMap);
			result.put("success", "1");
		} catch (SQLException e) {
			result.put("success", "0");
			result.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return  SUCCESS;
	}
	
	public String getDailyReportCnt() throws SQLException{
		initParaMap(mapper_namespace);
		list = baseDao.select("getDailyReportCnt", paraMap);
		return SUCCESS;
	}
	
	public String getThisWeekReport() throws SQLException{
		initParaMap(mapper_namespace);
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DAY_OF_WEEK);
		String begin;
		String end;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		if(day == 1){
			end = sdf.format(cal.getTime());
			cal.add(Calendar.DATE, -6);
			begin = sdf.format(cal.getTime());
		}else{
			cal.add(Calendar.DATE, -(day-2));
			begin = sdf.format(cal.getTime());
			cal.add(Calendar.DATE, 6);
			end = sdf.format(cal.getTime());
		}
		paraMap.put("begin", begin);
		paraMap.put("end", end);
		list = baseDao.select("getReportByBeginDate", paraMap);
		return SUCCESS;
	}

	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public List<Map<String, Object>> getList() {
		return list;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParent_id() {
		return parent_id;
	}

	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}

	public String getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getWork_dtl() {
		return work_dtl;
	}

	public void setWork_dtl(String work_dtl) {
		this.work_dtl = work_dtl;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getWork_type() {
		return work_type;
	}

	public void setWork_type(String work_type) {
		this.work_type = work_type;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}

	
}
