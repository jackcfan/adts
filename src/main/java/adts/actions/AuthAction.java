package adts.actions;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import adts.dao.BaseDao;
import adts.util.AuthMenuUtil;
import adts.util.IniUtil;
import adts.util.Util;

public class AuthAction extends BasicAction {
	private static final String mapper_namespace = "adts_auth";
	private BaseDao baseDao;
	private Integer user_id;
	private Integer group_id;
	private String group_name;
	private String group_desc;
	private String username;
	private String realname;
	private String password;
	private Integer state;
	private String tel_no;
	private String email;
	
	private Integer id;
	private Integer parent_id;
	private String text;
	private String url;
	private String title;
	private String iconcls;
	private Integer order_no;
	private String expanded;
	
	private String id_list;
	
	
	
	private final static String app_name = "ADTS_HOME_MENU";
	private List<Map<String,Object>> list;	//Ajax 返回
	private Map<String,Object> result;		//Ajax 返回
	
	public String getJsonMenu() throws SQLException, IOException{
		initParaMap(mapper_namespace);
		paraMap.put("app_name", app_name);
		paraMap.put("user_id", user.getId());
		List<Map<String,Object>> list = baseDao.select("getMenuListByUserId", paraMap);
		String json = AuthMenuUtil.getJsonMenu(0, list);
		response = ServletActionContext.getResponse();
		response.setCharacterEncoding("utf-8");
		response.getWriter().write(json);
		return null;
	}

	public String getAllJsonMenu() throws SQLException, IOException{
		initParaMap(mapper_namespace);
		paraMap.put("app_name", app_name);
		List<Map<String,Object>> list = baseDao.select("getAllJsonMenu", paraMap);
		String json = AuthMenuUtil.getJsonMenu(-1, list);
		response = ServletActionContext.getResponse();
		response.setCharacterEncoding("utf-8");
		response.getWriter().write(json);
		return null;
	}
	
	public String getAllGroup() throws SQLException, IOException{
		initParaMap(mapper_namespace);
		paraMap.put("app_name", app_name);
		list = baseDao.select("getAllGroup", paraMap);
		return SUCCESS;
	}
	
	public String getAllUser() throws SQLException{
		initParaMap(mapper_namespace);
		list = baseDao.select("getAllUser", paraMap);
		return SUCCESS;
	}
	
	public String getJsonCheckedMenuByGroupId() throws SQLException, IOException{
		initParaMap(mapper_namespace);
		paraMap.put("app_name", app_name);
		paraMap.put("group_id", group_id);
		String json;
		if(group_id != null){
			List<Map<String,Object>> list = baseDao.select("getJsonCheckedMenuByGroupId", paraMap);
			json = AuthMenuUtil.getJsonMenuWithChecked(-1, list);
		}else{
			json = "[]";
		}
		response = ServletActionContext.getResponse();
		response.setCharacterEncoding("utf-8");
		response.getWriter().write(json);
		return null;
	}
	
	public String addUser(){
		initParaMap(mapper_namespace);
		paraMap.put("username", username);
		paraMap.put("realname", realname);
		paraMap.put("password", Util.getMD5(password));
		paraMap.put("state", state);
		paraMap.put("tel_no", tel_no);
		paraMap.put("email", email);
		result = new HashMap<String,Object>();
		try {
			baseDao.insert("addUser", paraMap);
			result.put("success", 1);
		} catch (SQLException e) {
			result.put("success", 0);
			result.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String deleteUser(){
		initParaMap(mapper_namespace);
		paraMap.put("id", id);
		result = new HashMap<String,Object>();
		try {
			baseDao.insert("deleteUser", paraMap);
			result.put("success", 1);
		} catch (SQLException e) {
			result.put("success", 0);
			result.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String addMenu(){
		initParaMap(mapper_namespace);
		paraMap.put("parent_id", parent_id);
		paraMap.put("text", text);
		paraMap.put("url", url);
		paraMap.put("title", title);
		paraMap.put("iconcls", iconcls);
		paraMap.put("expanded", expanded);
		result = new HashMap<String,Object>();
		try {
			baseDao.insert("addMenu", paraMap);
			result.put("success", 1);
		} catch (SQLException e) {
			result.put("success", 0);
			result.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String updateMenu(){
		initParaMap(mapper_namespace);
		paraMap.put("id", id);
//		paraMap.put("parent_id", parent_id);
		paraMap.put("text", text);
		paraMap.put("url", url);
		paraMap.put("title", title);
		paraMap.put("iconcls", iconcls);
		paraMap.put("expanded", expanded);
		result = new HashMap<String,Object>();
		try {
			baseDao.insert("updateMenu", paraMap);
			result.put("success", 1);
		} catch (SQLException e) {
			result.put("success", 0);
			result.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String deleteMenu(){
		initParaMap(mapper_namespace);
		paraMap.put("id", id);
		result = new HashMap<String,Object>();
		try {
			baseDao.insert("deleteMenu", paraMap);
			result.put("success", 1);
		} catch (SQLException e) {
			result.put("success", 0);
			result.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String addMenuGroup() throws SQLException{
		result = new HashMap<String,Object>();
		if(group_id == null){
			result.put("success", 0);
			result.put("msg", "未指定组ID");
			return SUCCESS;
		}
		
		initParaMap(mapper_namespace);
		paraMap.put("group_id", group_id);
		baseDao.insert("deleteMenuGroup", paraMap);
		if(id_list == null || id_list.length() == 0){
			result.put("success", 1);
			result.put("msg", "");
			return SUCCESS;
		}
		String[] ids = id_list.split("\\,");
		for(String id : ids){
			paraMap.put("resource_id",Integer.parseInt(id));
			baseDao.insert("addMenuGroup", paraMap);
		}
		result.put("success", 1);
		return SUCCESS;
	}
	
	public String addGroup() throws SQLException{
		initParaMap(mapper_namespace);
		paraMap.put("group_name", group_name);
		paraMap.put("group_desc", group_desc);
		result = new HashMap<String,Object>();
		baseDao.insert("addGroup", paraMap);
		result.put("success", 1);
		return SUCCESS;
	}
	
	public String updateGroup() throws SQLException{
		initParaMap(mapper_namespace);
		paraMap.put("id", id);
		paraMap.put("group_name", group_name);
		paraMap.put("group_desc", group_desc);
		result = new HashMap<String,Object>();
		baseDao.insert("updateGroup", paraMap);
		result.put("success", 1);
		return SUCCESS;
	}
	
	public String deleteGroup() throws SQLException{
		initParaMap(mapper_namespace);
		paraMap.put("id", id);
		result = new HashMap<String,Object>();
		baseDao.insert("deleteGroup", paraMap);
		result.put("success", 1);
		return SUCCESS;
	}
	
	/**
	 * 查看所有的用户和当前组的关系
	 * */
	public String getAllUserWithinGroup() throws SQLException{
		initParaMap(mapper_namespace);
		paraMap.put("id", id);
		list = baseDao.select("getAllUserWithinGroup", paraMap);
		return SUCCESS;
	}
	
	public String addUserToGroup() throws SQLException{
		initParaMap(mapper_namespace);
		paraMap.put("user_id", user_id);
		paraMap.put("group_id", group_id);
		result = new HashMap<String,Object>();
		baseDao.delete("delUserFromGroup", paraMap);
		baseDao.insert("addUserToGroup", paraMap);
		result.put("success", 1);
		return SUCCESS;
	}
	
	public String delUserFromGroup() throws SQLException{
		initParaMap(mapper_namespace);
		paraMap.put("user_id", user_id);
		paraMap.put("group_id", group_id);
		result = new HashMap<String,Object>();
		baseDao.delete("delUserFromGroup", paraMap);
		result.put("success", 1);
		return SUCCESS;
	}
	
	
	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public List<Map<String, Object>> getList() {
		return list;
	}

	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}

	public Integer getGroup_id() {
		return group_id;
	}

	public void setGroup_id(Integer group_id) {
		this.group_id = group_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getTel_no() {
		return tel_no;
	}

	public void setTel_no(String tel_no) {
		this.tel_no = tel_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParent_id() {
		return parent_id;
	}

	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIconcls() {
		return iconcls;
	}

	public void setIconcls(String iconcls) {
		this.iconcls = iconcls;
	}

	public Integer getOrder_no() {
		return order_no;
	}

	public void setOrder_no(Integer order_no) {
		this.order_no = order_no;
	}

	public String getExpanded() {
		return expanded;
	}

	public void setExpanded(String expanded) {
		this.expanded = expanded;
	}

	public String getId_list() {
		return id_list;
	}

	public void setId_list(String id_list) {
		this.id_list = id_list;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public String getGroup_desc() {
		return group_desc;
	}

	public void setGroup_desc(String group_desc) {
		this.group_desc = group_desc;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	
	
}
