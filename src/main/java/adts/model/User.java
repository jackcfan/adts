package adts.model;

import java.util.Map;

public class User {
	private int id;
	private String username;
	private String password;
	private String realname;
	private String token;
	private int state;
	
	public static final int STATE_NORMAL = 0;
	public static final int STATE_FORBIDDEN = 1;
	public static final int STATE_PWD_EXPIRED = 2;
	
	public User(Map<String,Object> para){
		super();
		if(para.containsKey("id")){
			id = (Integer)para.get("id");
		}
		if(para.containsKey("username")){
			username = para.get("username").toString();
		}
		if(para.containsKey("realname")){
			realname = para.get("realname").toString();	
		}
		if(para.containsKey("token")){
			token = para.get("token").toString();
		}
		if(para.containsKey("state")){
			state = (Integer)para.get("state");
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	
}
