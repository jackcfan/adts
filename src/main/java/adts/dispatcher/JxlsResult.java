package adts.dispatcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.StrutsStatics;
import org.apache.struts2.dispatcher.StrutsResultSupport;

import com.opensymphony.xwork2.ActionInvocation;

@SuppressWarnings("serial")
public class JxlsResult extends StrutsResultSupport {

	@Override
	protected void doExecute(String arg0, ActionInvocation arg1)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/*private static Logger logger = Logger.getLogger(JxlsResult.class);
	
	private HttpServletResponse response;
	
	private ServletContext servletContext;
	
	private String xlsTemplate;
	
	private String attachment;
	
	public void init(ActionInvocation invocation) {
		setResponse((HttpServletResponse) invocation.getInvocationContext().get(StrutsStatics.HTTP_RESPONSE));
		setServletContext((ServletContext) invocation.getInvocationContext().get(StrutsStatics.SERVLET_CONTEXT));
	}

	@Override
	protected void doExecute(String finalLocation, ActionInvocation invocation)
			throws Exception {
		init(invocation);
		HSSFWorkbook workBook = null;
		InputStream tptIs = null;
		OutputStream resOs = null;
		
		String prefix = String.valueOf(System.currentTimeMillis());					// csv文件名前缀
		String xlsFile = servletContext.getRealPath("/")+"temp\\" +prefix+".xls";	// 待压缩的csv文件名
		String zipFile = servletContext.getRealPath("/")+"temp\\"+prefix+".zip";	// 压缩后的文件名
		String outputFile = "";
		
		String attachmentName = null;		// 附件名 (不含后缀名)
		if(getStackValue(invocation,"attachmentName").contains(".")==true){
			attachmentName = getStackValue(invocation,"attachmentName").substring(0, getStackValue(invocation,"attachmentName").lastIndexOf("."));
		}else {
			attachmentName = getStackValue(invocation,"attachmentName");
		}
		
		try {
			tptIs = new FileInputStream(servletContext.getRealPath("/") + getXlsTemplate());
			resOs = response.getOutputStream();
			// 导出 excel 开始
			workBook = new XLSTransformer().transformXLS(tptIs, getBeans(invocation));
			FileOutputStream file = new FileOutputStream(xlsFile);
			workBook.write(new FileOutputStream(xlsFile));
			file.close();
			workBook = null;
			// 导出 excel 结束
			if("y".equalsIgnoreCase(getStackValue(invocation,"zip"))){					// 判断当前文件是否要压缩传输
				FileProcessUtil.compressToZipFile(xlsFile,attachmentName+".xls", zipFile, true);
				outputFile = zipFile;
				response.setContentType("application/zip;charset=utf-8");
				setResponseAttachmentName(attachmentName+".zip");
			}else{
				outputFile = xlsFile;
				response.setContentType("application/vnd.ms-excel;charset=utf-8");
				setResponseAttachmentName(attachmentName+".xls");
			}
			tptIs = null;
			tptIs = new FileInputStream(outputFile);
			resOs = response.getOutputStream();
			byte[] buff = new byte[1024*1024];	//使用字节缓冲数组
			int len = tptIs.read(buff);	 		//把读取到的字节缓存到 buff 数组中
			while(len != -1){
				resOs.write(buff,0,len);		//把 buff 写入到 文件输出流中
				len=tptIs.read(buff);
			}
			resOs.flush();
			resOs.close();
			tptIs.close();
		} catch (Exception e) {
			log.error(e);
		} finally {
			new File(outputFile).delete();			//删除生成的zip或 csv 文件
			new File(xlsFile).delete();			//删除生成的zip或 csv 文件
		}
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public String getXlsTemplate() {
		return xlsTemplate;
	}

	public void setXlsTemplate(String xlsTemplate) {
		this.xlsTemplate = xlsTemplate;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	*/
}
