package adts.filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter;

import adts.dao.BaseDao;
import adts.model.User;

public class LoginFilter extends StrutsPrepareAndExecuteFilter {
	public static final String LOGIN_PAGE = "auth/login.html";
	
	public static final String LOGIN_DO_PAGE = "auth/login.do";
	
	private BaseDao baseDao;
	
	public void doFilter(ServletRequest req, ServletResponse res,FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String uri = request.getRequestURI();
		User user = (User) request.getSession().getAttribute("ADTS_USER");
		String context_path = request.getContextPath();
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "public");
		// 未登陆或session失效
		if(user == null) { // 未登录
			if (uri.equals(context_path+"/"+LOGIN_PAGE) || uri.equals(context_path+"/"+LOGIN_DO_PAGE)) {
				chain.doFilter(request, response);
			} else {
				request.getSession().setAttribute("pre_page",uri.substring(uri.indexOf("/",2)));
				Cookie[] cookies = request.getCookies();
				String username = null;
				String token = null;
				String auto_login = null;
				if(cookies != null){
					//尝试从cookie中取得自动登录信息
					for(Cookie cookie : cookies){
						if("username".equals(cookie.getName())){
							username = cookie.getValue();
						}
						if("token".equals(cookie.getName())){
							token = cookie.getValue();
						}
						if("auto_login".equals(cookie.getName())){
							auto_login = cookie.getValue();
						}
					}
				}
				if("on".equalsIgnoreCase(auto_login)){
					Map<String,Object> para = new HashMap<String,Object>();
					para.put("username", username);
					para.put("token", token);
					try {
						Map<String,Object> user_map = (Map<String, Object>) baseDao.selectOne("getUserByToken", para);
						if(user_map != null){
							request.getSession().setAttribute("ADTS_USER",new User(user_map));
							chain.doFilter(request, response);
						}else{
							reLogin(request, response);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}else{
					reLogin(request, response);
				}
			}
		}else {
			chain.doFilter(request, response);
		}
	}
	
	public void reLogin(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<script language=\"JavaScript\">");
		//out.println("alert('您还没有登录或者会话已过期，请重新登录。');");
		out.println("window.location.href = '" + request.getContextPath() +"/"+LOGIN_PAGE+ "';");
		out.println("</script>");
		out.close();
	}

	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}
	
}
