package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.tmatesoft.svn.core.ISVNLogEntryHandler;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNLogClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

/**
 * @author liuhx
 *
 */
public class SvnExportTool {
	
	public static final Pattern  PROTOCOL = Pattern.compile("(\\w+://[\\w|\\d|.]+)");	
    private static volatile SvnExportTool svnExportTool ;
    
    private SvnExportTool(){
		SVNRepositoryFactoryImpl.setup();		
		FSRepositoryFactory.setup();  
	    DAVRepositoryFactory.setup();
    }
    
    public static SvnExportTool getInstance(){
    	if(svnExportTool==null){
    		svnExportTool = new SvnExportTool();
    	}
    	return svnExportTool;
    }
	/**
	 * 比较版本导出
	 * @param repositoryURL	
	 * @param dist	导出的目录文件夹
	 * @param userName	用户名
	 * @param password	密码
	 * @param startRevision	起始版本
	 * @param endRevision	结束版本
	 * @throws SVNException
	 * @throws IOException
	 */
	public void export(String repositoryURL,String dist,String userName,String password,long startRevision ,long endRevision,EnableExportHandler enableExport) throws SVNException, IOException{		
		 export(repositoryURL, dist, userName, password, startRevision , endRevision,false, enableExport) ;
	}
	/**
	 * 比较版本导出
	 * @param repositoryURL	
	 * @param dist	导出的目录文件夹
	 * @param userName	用户名
	 * @param password	密码
	 * @param startRevision	起始版本
	 * @param endRevision	结束版本
	 * @param lastVersion	是否导出最新版本
	 * @throws SVNException
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	public void export(String repositoryURL,String dist,String userName,String password,long startRevision ,long endRevision,boolean lastVersion,EnableExportHandler enableExport) throws SVNException, IOException{		
	    ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(userName, password);  

		ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
		//创建SVNClientManager的实例
		SVNClientManager clientManager = SVNClientManager.newInstance((DefaultSVNOptions) options, authManager);
		
		ExportLogEntryHandler handler = new ExportLogEntryHandler();
		
		Matcher matcher = PROTOCOL.matcher(repositoryURL);
		String svnHost = null;
		if(matcher.find()){
			svnHost = matcher.group(1);
		}else
			svnHost = repositoryURL;
		//获得历史记录
		getHistoryEntrys(clientManager,repositoryURL,startRevision ,endRevision,handler);
		
		List<SVNLogEntry> history =	compareRevision(handler.history);
		
		StringBuilder logger = new StringBuilder();
		for(SVNLogEntry entry:history){
			if(enableExport!=null){
				//判断能否导出，若不能导出继续
				if(!enableExport.enableExport(entry)){
					continue;
				}
			}
			logger.append(entry.getRevision()).append(" ").append(entry.getDate().toLocaleString()).append(" ").append(entry.getAuthor()).append(" ").append(entry.getMessage()).append("\r\n");
			Collection<SVNLogEntryPath> paths = entry.getChangedPaths().values();
			for (SVNLogEntryPath path : paths) {
				
				logger.append(path).append("\r\n");
				if(path.getType()=='D'){//删除
					System.out.println("D");
					File file = new File(dist,path.getPath());
					if(file.exists())
						file.delete();
					continue;
				}
				//添加或更新
				File file = new File(dist,path.getPath()).getParentFile();
				if(!file.exists()){
					file.mkdirs();
				}
				System.out.println(path.getPath());
				
				SVNRevision fileVersion = null;
				if(lastVersion){
					fileVersion = SVNRevision.HEAD;
				}else{
					fileVersion = SVNRevision.create(entry.getRevision());
				}
				//导出
				clientManager.getUpdateClient().doExport(SVNURL.parseURIEncoded(svnHost +path.getPath()),file, SVNRevision.HEAD,fileVersion,"native", true,SVNDepth.INFINITY);
			}
			logger.append("\r\n");
		}
		FileUtils.writeStringToFile(new File(dist,"logger.log"), logger.toString(), "UTF-8");
	}
	/**
	 * 比较版本
	 * @param list
	 * @return
	 */
	private  List<SVNLogEntry>  compareRevision(List<SVNLogEntry> list){
		return list;		
	}
	/**
	 * 获得版本区间的历史记录
	 * @param repositoryURL
	 * @param start
	 * @param end
	 * @param handler
	 * @throws SVNException
	 */
	private void getHistoryEntrys (SVNClientManager clientManager ,String repositoryURL,long start ,long end,ISVNLogEntryHandler handler) throws SVNException{
		SVNLogClient logClient = clientManager.getLogClient();
		SVNRevision startRevision = SVNRevision.create(start);
		SVNRevision endRevision = null;
		if(end==-1)
			endRevision = SVNRevision.HEAD;
		else
			endRevision = SVNRevision.create(end);
		
		logClient.doLog(SVNURL.parseURIEncoded(repositoryURL),new String[] {},SVNRevision.HEAD, startRevision, endRevision,false, true, 100,handler);
	}
	
	class ExportLogEntryHandler implements ISVNLogEntryHandler {
		private List<SVNLogEntry> history = new ArrayList<SVNLogEntry>();
		
		public void handleLogEntry(SVNLogEntry logEntry) throws SVNException {
				//存储历史提交记录
				history.add(logEntry);
		}
	};
	
	public interface EnableExportHandler{
		public boolean enableExport(SVNLogEntry logEntry);
	}
	
	public static void main(String[] args) throws SVNException, IOException{
		String svnHost = "https://197.1.4.29:8443/svn/ECIF_FILE_UPGRADE/01_需求模型/0104_模型设计/010400_逻辑模型/01040001_数据字典/0104000101_最终模型/DPF_数据字典.xlsx";
		SvnExportTool.getInstance().export(svnHost,"d:/adts","ecif-wugang","123456",1070,1071,false,new EnableExportHandler(){
			@Override
			public boolean enableExport(SVNLogEntry logEntry) {
				if(logEntry.getAuthor().equals("ecif-wugang")){
					return true;
				}
				return true;
			}
		});
	}
}
