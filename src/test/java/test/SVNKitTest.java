package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.internal.util.SVNURLUtil;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;

import adts.util.IniUtil;
import adts.util.SVNKitUtil;
import adts.util.Universalchardet;
import adts.util.Util;

public class SVNKitTest {
	
	@Test
	public void testExport() throws SVNException{
		SVNKitUtil svn = new SVNKitUtil("DOC_SVN_CFG");
		String url = "/06_运维/svnkit_test.txt";
		long revision = svn.export(url, new File("D:/adts/svnkit_test.txt"), 1072);
		System.out.println(revision);
	}
	
	@Test
	public void testGetFileList() throws SVNException{
		SVNKitUtil svn = new SVNKitUtil("ETL_SVN_CFG");
		String url = "/ETL/init/update";
		List<Map<String,Object>> result = svn.getFileList(url);
		for(int i = result.size() - 1;i>=0;i--){
			Map<String,Object> map = result.get(i);
			if(!map.get("name").toString().startsWith("v") && !"dir".equals(map.get("kind")) ){
				result.remove(i);
			}
		}
		for(Map<String,Object> map : result){
			System.out.println(map);
		}
	}
	
	@Test
	public void readUpdateConfFile() throws SVNException, IOException{
		String version        = "v4.9c";
		String scripts_update = "/ETL/init/update/" + version + "/scripts_update.conf";
		String work_dir       = System.getenv("tmp")+"/adts/"+Util.getUUID()+"/ETL";
		File work = new File(work_dir + "/init/update/" + version);
		work.mkdirs();
		
		SVNKitUtil svn = new SVNKitUtil("ETL_SVN_CFG");
		
		String des = work_dir + "/init/update/" + version+"/scripts_update.conf";
		File file = new File(des);
		svn.export(scripts_update, file, SVNRevision.HEAD, SVNRevision.HEAD, null, true, SVNDepth.INFINITY);
		String encoding = Universalchardet.getFileEncoding(file);
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),encoding));
		String line;
		System.out.println(des);
		while((line = reader.readLine()) != null){
			line = line.trim();
			if(line.startsWith("#") || line.length() == 0){
				System.out.println(line);
				continue;
			}
			String svn_file = "/ETL/" + line;
			svn.export(svn_file, new File(work_dir + "/" + line) , SVNRevision.HEAD, SVNRevision.HEAD, null, true, SVNDepth.INFINITY);
			System.out.println(line);
		}
		reader.close();
		System.out.println(work.delete());;
	}
	
	@Test
	public void testGetLog() throws SVNException{
		String file = "/01_需求模型/0104_模型设计/010400_逻辑模型/01040001_数据字典/0104000101_最终模型/DPF_数据字典.xlsx";
		SVNKitUtil svn = new SVNKitUtil("DOC_SVN_CFG");
//		file = "01_需求模型/0104_模型设计/010400_逻辑模型/01040001_数据字典/0104000101_最终模型/";
		List<Map<String,String>> list = svn.getSvnLog(file);
		for(Map<String,String> map : list){
			System.out.println(map);
		}
	}
	
	@Test
	public void testTrim(){
		String str = "   a\nbc\nde f   ";
		char cr = 10;
		char lf = 13;
		str = str.trim().replaceAll("(\r|\n)", ";");
		System.out.println(str);
		
	}
	
	@Test
	public void doSummarizeDiff() throws SVNException{
		SVNKitUtil svn = new SVNKitUtil("ETL_SVN_CFG");
		List<Map<String,String>> list = svn.doSummarizeDiff("/tags/ETL/UAT/APP/LLD","/trunk/ETL/APP/LLD","file");
		for(Map<String,String> map : list){
			System.out.println(map);
		}
	}
	
	@Test
	public void testCommit() throws SVNException{
		SVNKitUtil svn = new SVNKitUtil("ETL_SVN_CFG");
		svn.commit();
	}
}
