package test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import adts.dao.BaseDao;
import adts.util.DDLgenUtil;



public class SpringDaoTest {
	BeanFactory factory;
	BaseDao baseDao;
	
	@Before
	public void setUp() throws Exception {
		factory = new ClassPathXmlApplicationContext("applicationContext.xml");
		baseDao = (BaseDao) factory.getBean("baseDao");
	}
	
	@Test
	public void testDDLgenTable() throws SQLException{
		String schema_name = "ECIF_CDATA";
		String tab_name    = "C00_GL_EXCH_RATE0";
		int revision       = 1088;
		String env_name    = "PRD";
		
		DDLgenUtil ddl = new DDLgenUtil(baseDao,"adts_model");
		Map<String,String> map = ddl.genDB2DDL(schema_name, tab_name, revision, env_name);
		System.out.println(map.get("ddl"));
		System.out.println(map.get("grant"));
		System.out.println(map.get("index"));
		System.out.println(map.get("col_sens"));
	}
	@Test
	public void testDDLgen() throws SQLException, IOException{
		
	}
	
	@Test
	public void testTimeout(){
		try {
			baseDao.select("timeout_test", null);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("============="+e.getMessage());
		}
	}
	
}
