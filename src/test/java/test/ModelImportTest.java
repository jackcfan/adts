package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.internal.util.SVNURLUtil;
import org.tmatesoft.svn.core.wc.SVNRevision;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;

import adts.util.IniUtil;
import adts.util.POIExcelParser;
import adts.util.SVNKitUtil;
import adts.util.Universalchardet;
import adts.util.Util;

public class ModelImportTest {
	String xls = "c:\\ecif_model.xlsx";
	int revision = 1070;
	
	@Test
	public void readTable() throws Exception{
		List<Map<String,Object>> table = POIExcelParser.readXLS(xls, "表级", "Model_Table");
		System.out.println(table);
	}
	
	@Test
	public void readXls() throws FileNotFoundException, IOException{
		XSSFWorkbook book = new XSSFWorkbook(new FileInputStream(new File(xls)));
	}
}
