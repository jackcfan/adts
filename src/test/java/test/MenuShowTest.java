package test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import adts.util.AuthMenuUtil;

public class MenuShowTest {
	
	public class MenuNode{
		public long id;
		public String title;
		public boolean leaf;
		public List<MenuNode> children;
	}
	
	public static void main(String[] args) throws IOException {
//		StringBuffer sb = new StringBuffer("abc,");
//		if(sb.charAt(sb.length() - 1) == ','){
//			System.out.println("eq");
//		}
//		sb.deleteCharAt(sb.length() - 1);
//		System.out.println(sb);
//		System.exit(0);
		
		
		String resource = "test/resources/mybatis.xml";
		InputStream in = Resources.getResourceAsStream(resource);
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
		SqlSession session = factory.openSession();
		List<Map<String,Object>> list = session.selectList("getMenuList");
		
		System.out.println(AuthMenuUtil.getJsonMenu(0,list));
//		System.out.println(AuthMenuUtil.getJsonMenu(1,list));
//		System.out.println(AuthMenuUtil.getJsonMenu(5,list));
	}

}
