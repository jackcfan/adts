package test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.xml.sax.SAXException;

import adts.util.XlsxAbstractUtil;

public class NmaCheckListImport extends XlsxAbstractUtil {

	public String resource = "test/resources/mybatis.xml";
	public InputStream in = Resources.getResourceAsStream(resource);
	public SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
	public SqlSession session = factory.openSession();
	
	public NmaCheckListImport(String filename) throws Exception,
			IOException, OpenXML4JException, SAXException {
		super(filename);
	}

	@Override
	protected void processRows(int cur_row, Map<String, String> row_map,String xml_mapping_worksheet_name,String worksheet) {
		String chk_err_cmp_cnt = row_map.get("CHK_ERR_CMP_CNT");
		if(chk_err_cmp_cnt != null && chk_err_cmp_cnt.length() > 0){
			row_map.put("CHK_ERR_CMP", chk_err_cmp_cnt.replaceAll("\\d", ""));
			row_map.put("CHK_ERR_CNT", chk_err_cmp_cnt.replaceAll("[>=<]", ""));
		}else{
			row_map.put("CHK_ERR_CMP", "");
			row_map.put("CHK_ERR_CNT", "null");
		}
		System.out.println(row_map);
		session.delete("deleteCheckListCfg",row_map);
		session.insert("insertCheckListCfg",row_map);
		
		session.commit();
	}
	
	public static void main(String[] args) throws Exception {
		String file = "c:/民生管会checklist.xlsx";
		NmaCheckListImport xlsx = new NmaCheckListImport(file);
		xlsx.processSheet("检查配置模板", "Nma_Check_List");
		
		xlsx.session.close();
	}
	
}
