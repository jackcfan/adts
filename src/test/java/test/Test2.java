package test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Test2 {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DAY_OF_WEEK);
		String begin;
		String end;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		if(day == 1){
			end = sdf.format(cal.getTime());
			cal.add(Calendar.DATE, -6);
			begin = sdf.format(cal.getTime());
		}else{
			cal.add(Calendar.DATE, -(day-2));
			begin = sdf.format(cal.getTime());
			cal.add(Calendar.DATE, 6);
			end = sdf.format(cal.getTime());
		}
		System.out.println(begin+":"+end);
	}
}
