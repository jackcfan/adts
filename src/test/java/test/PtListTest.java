package test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import adts.util.IniUtil;

public class PtListTest {
	public static void main(String[] args) {
		List<String> list = genPtInfo();
		for(String pt : list){
			System.out.println(pt);
		}
	}
	
	private static List<String> genPtInfo(){
		List<String> list = new ArrayList<String>();
		//分区区间，从前 pt_before_month 月开始，共 pt_all_month 个分区
		int before = Integer.parseInt(IniUtil.getValByKey("MODEL_PT_INFO", "pt_before_month"));
		int all = Integer.parseInt(IniUtil.getValByKey("MODEL_PT_INFO", "pt_all_month"));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-01");
		
		Calendar ca = Calendar.getInstance();
		ca.setTime(new Date());
		ca.add(Calendar.MONTH, -before);
		list.add("PART PART0 STARTING(MINVALUE) IN $TABLE_SPACE $INDEX_IN_INDEX_SPACE");
		for(int i=0;i < all; i++){
			Date d = ca.getTime();
			String pt_info = "PART PART"+(i+1)+" STARTING('" + sdf.format(ca.getTime()) + "') ENDING('";
			ca.add(Calendar.MONTH, 1);
			pt_info += sdf.format(ca.getTime()) + "') EXCLUSIVE IN $TABLE_SPACE $INDEX_IN_INDEX_SPACE";
			list.add(pt_info);
		}
		list.add("PART PART14 ENDING(MAXVALUE) IN $TABLE_SPACE $INDEX_IN_INDEX_SPACE");
		return list;
	}
}
