package test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.xml.sax.SAXException;

import adts.util.XlsxAbstractUtil;

public class XlsxAbstractUtilTest extends XlsxAbstractUtil {

	public XlsxAbstractUtilTest(String filename) throws Exception,
			IOException, OpenXML4JException, SAXException {
		super(filename);
	}
	
	@Override
	protected void processRows(int cur_row, Map<String, String> row_map,String xml_mapping_worksheet_name,String worksheet) {
		System.out.println(row_map);
		System.out.println("##############################################\n");
	}
	
	public static void main(String[] args) throws Exception {
		String file = "D:\\workspace\\java\\adts\\src\\test\\java\\test\\XlsxAbstractUtilTest.xlsx";
		file = "D:\\tmp\\model\\DPF_数据字典.xlsx";
		XlsxAbstractUtilTest xlsx = new XlsxAbstractUtilTest(file);
		xlsx.processSheet("表空间列表",null);
	}

}
