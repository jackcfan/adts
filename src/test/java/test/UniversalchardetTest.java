package test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.mozilla.universalchardet.CharsetListener;
import org.mozilla.universalchardet.UniversalDetector;

import adts.util.Universalchardet;

public class UniversalchardetTest {
	public static void main(String[] args) throws IOException {
		String file = "D://README.txt";
		file = "D://scripts_update.conf";
		String encoding = Universalchardet.getFileEncoding(file);
		System.out.println(encoding);
		
		
		String read;
		InputStreamReader r = new InputStreamReader(new FileInputStream(file), encoding);
		BufferedReader in = new BufferedReader(r);
		while ((read = in.readLine()) != null) {
			System.out.println(read);
		}
	}
}
