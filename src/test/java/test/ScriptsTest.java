package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ScriptsTest {
	public static void main(String[] args) throws IOException, InterruptedException {
		String file = ScriptsTest.class.getResource("").getPath().replaceAll("^\\/", "") + "scripts_test.pl";
		
		String[] cmd = {"perl", file};
//		Process process = Runtime.getRuntime().exec(cmd);
		
		ProcessBuilder build = new ProcessBuilder(cmd);
		//设置redirectErrorStream(boolean redirectErrorStream)方法，让错误输出流与标准输出合并
		build.redirectErrorStream(true);
		
		Process process = build.start();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()), 1024); 
        
		String line = null; 
		while (bufferedReader != null && (line = bufferedReader.readLine()) != null) { 
			System.out.println(line); 
		}
		                     
		process.waitFor();
		int ret_code = process.exitValue();
		System.out.println(ret_code);
		process.destroy();
	}
}
