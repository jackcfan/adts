package test;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.tmatesoft.svn.core.SVNException;

import adts.util.SVNKitUtil;
import adts.util.Util;
import adts.util.XlsxCommonUtil;
import adts.util.ZipCompressor;

public class VersionControlTest {
	BeanFactory factory;
	String version = "v4.9n";
	private SVNKitUtil etl_svn;
	
	@Before
	public void setUp() throws Exception {
		factory = new ClassPathXmlApplicationContext("applicationContext.xml");
		etl_svn = (SVNKitUtil) factory.getBean("etl_svn");
	}
	
	@Test
	public void testMakePackage() throws Exception {
		String svn_xlsx    = "/trunk/ETL/init/update/prd_update_list.xlsx";
		String svn_ver_dir = "/trunk/ETL/init/update/" + version;
		String work_home   = System.getenv("tmp")+"/adts/"+Util.getUUID();
		
		work_home       = System.getenv("tmp")+"/adts/test";
		
		String zip_file       = work_home + "/" + version + ".zip";
		File xlsx_file = null;
		XlsxCommonUtil xlsx_util = null;
		OutputStreamWriter writer = null;
		try{
			File work_home_ver = new File(work_home + "/ETL/init/update/" + version);
			work_home_ver.mkdirs();
			//导出该版本下默认需要导出的文件
			etl_svn.export(svn_ver_dir, work_home_ver);
			
			xlsx_file = new File(work_home + "/prd_update_list.xlsx");
			//导出上线列表excel
			etl_svn.export(svn_xlsx, xlsx_file);
			
			//生成上线脚本列表 scripts_update.conf
			xlsx_util = new XlsxCommonUtil(xlsx_file);
			List<Map<String,String>> rows = xlsx_util.readSheet("脚本部署", "ETL_Script_Update");
			File script_update = new File(work_home_ver+"/scripts_update.conf");
			writer = new OutputStreamWriter(new FileOutputStream(script_update),"GBK");
			writer.write("##################################################\n");
			writer.write("#所有的脚本路径从AUTO_HOME下开始\n");
			writer.write("#比如 APP/FLD/S_EUS_AQZHD\n");
			writer.write("#一行一条记录，可以是文件或目录，行首用#号表示注释\n");
			writer.write("##################################################\n\n");
			for(Map<String,String> map : rows){
				String ver = map.get("version");
				if(!ver.equalsIgnoreCase(version)){
					continue;
				}
				writer.write("#"+map.get("content")+"("+map.get("owner")+")\n");
				writer.write(map.get("file")+"\n\n");
				etl_svn.export("/trunk/ETL/"+map.get("file"), new File(work_home+"/ETL/"+map.get("file")));
			}
			writer.close();
			
			//生成DPF模型部署列表 update_dpf_ecif.conf
			boolean empty = true;    //文件为空标志
			rows = xlsx_util.readSheet("后端模型", "ETL_Model_Update");
			File model_update = new File(work_home_ver+"/update_dpf_ecif.conf");
			writer = new OutputStreamWriter(new FileOutputStream(model_update),"GBK");
			for(Map<String,String> map : rows){
				String ver = map.get("version");
				if(!ver.equalsIgnoreCase(version)){
					continue;
				}
				empty = false;
				writer.write(map.get("file")+"\n");
			}
			writer.close();
			if(empty){
				model_update.delete();
			}
			
			//生成前端模型部署列表 update_db2_ecif.conf
			empty = true;    //文件为空标志
			rows = xlsx_util.readSheet("前端模型", "ETL_Model_Update");
			model_update = new File(work_home_ver+"/update_db2_ecif.conf");
			writer = new OutputStreamWriter(new FileOutputStream(model_update),"GBK");
			for(Map<String,String> map : rows){
				String ver = map.get("version");
				if(!ver.equalsIgnoreCase(version)){
					continue;
				}
				empty = false;
				writer.write(map.get("file")+"\n");
			}
			writer.close();
			if(empty){
				model_update.delete();
			}
			
			//生成回单模型部署列表 update_db2_ivc.conf
			empty = true;    //文件为空标志
			rows = xlsx_util.readSheet("BRMS模型", "ETL_Model_Update");
			model_update = new File(work_home_ver+"/update_db2_ivc.conf");
			writer = new OutputStreamWriter(new FileOutputStream(model_update),"GBK");
			for(Map<String,String> map : rows){
				String ver = map.get("version");
				if(!ver.equalsIgnoreCase(version)){
					continue;
				}
				empty = false;
				writer.write(map.get("file")+"\n");
			}
			writer.close();
			if(empty){
				model_update.delete();
			}
			
			//导出所有文件后打zip包
			if(ZipCompressor.zipDir(work_home+"/ETL",zip_file) != 1){
				throw new Exception("打包失败");
			}
			
			
		}catch(SVNException e){
			System.out.println(e.getMessage());
		}finally{
			if(xlsx_util != null){
				xlsx_util.closeXlsx();
				xlsx_file.delete();
			}
			if(writer != null){writer.close();}
		}
	}

}
